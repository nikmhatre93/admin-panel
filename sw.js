var cash_name = 'static-v2'
var cash_list = [
    '/',
    'index.html',
    'add.html',
    'ambassadorlist.html',
    'studentlist.html',
    'transaction.html',

    'js/jquery-3.1.1.min.js',
    'js/popper.min.js',
    'js/bootstrap.js',
    'https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js',
    'js/login.js',
    'js/plugins/metisMenu/jquery.metisMenu.js',
    'js/plugins/slimscroll/jquery.slimscroll.min.js',
    'js/inspinia.js',
    'js/plugins/pace/pace.min.js',
    'js/plugins/iCheck/icheck.min.js',
    'js/plugins/toastr/toastr.min.js',
    'js/logout.js',
    'js/add.js',
    'js/ambassador.js',
    'js/plugins/footable/footable.all.min.js',
    'js/transaction.js',
    'js/student.js',
    'https://unpkg.com/dexie@latest/dist/dexie.js',

    'css/bootstrap.min.css',
    'font-awesome/css/font-awesome.css',
    'css/animate.css',
    'css/style.css',
    'css/plugins/toastr/toastr.min.css',
    'css/plugins/iCheck/custom.css',
    'css/plugins/footable/footable.core.css',
    'js/excellentexport.min.js',
    'css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',

    'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
    'https://fonts.googleapis.com/css?family=Roboto:400,300,500,700',
    'https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UNirkOUuhp.woff2',
    'https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN7rgOUuhp.woff2',
    'https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVZ0b.woff2'
]

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

self.addEventListener('install', function (e) {
    e.waitUntil(
        caches.open(cash_name)
            .then(function (cache) {
                return cache.addAll(cash_list);
            })
    )
})


self.addEventListener('activate', function (event) {

    var cacheWhitelist = ['static-v2'];

    event.waitUntil(
        caches.keys().then(function (cacheNames) {
            return Promise.all(
                cacheNames.map(function (cacheName) {
                    if (cacheWhitelist.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    );
});


self.addEventListener('fetch', function (event) {

    event.respondWith(
        caches.match(event.request)
            .then(function (response) {

                if (response) {
                    return response;
                }

                var fetchRequest = event.request.clone();

                return fetch(fetchRequest).then(function (response) {
                    if (!response || response.status !== 200 || response.type !== 'basic') {
                        return response;
                    }

                    var responseToCache = response.clone();
                    caches.open(cash_name)
                        .then(function (cache) {
                            cache.put(event.request, responseToCache);
                        });

                    return response;
                })
            }
            )
    );
});

let syncStore = {}
self.addEventListener('message', event => {
    if (event.data.type === 'post') {
        let id = 'post'
        id += guid()
        syncStore[id] = { data: event.data, port: event.ports[0] }
        self.registration.sync.register(id)
    }
})

self.addEventListener('sync', function (event) {
    if (event.tag.substring(0, 4) == 'post') {
        return event.waitUntil(postCall(event.tag));
    }
});

function postCall(id) {
    let callData = syncStore[id].data
    let cllbck = syncStore[id].port
    const body = JSON.stringify({ ...callData.options.data })
    fetch(callData.url, {
        method: 'POST',
        body,
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${callData.options.token}`
        }
    }).then(res => res.json()).then(res => {
        cllbck.postMessage(res)
    })
}

