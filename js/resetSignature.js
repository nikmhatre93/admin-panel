
function resetAttendace()
{
    //console.log(updateData)
    $.ajax({
       //processData:false,->Dont do else it wont put data
        type: "PUT",
        url:`${base_url}/participants/${_id}`,
        data:{attended:true,pic:imageStrapiLink},
        headers: {
            Authorization: `Bearer ${authToken}`
        },
        success: function (data) {

            alert("Status changed to not attended for \""+name+"\" ("+TZ_id+") ");
            location.reload();
        },
        error: function (err) 
        {
            console.log("Error: "+JSON.stringify(err));
        }
    });
}