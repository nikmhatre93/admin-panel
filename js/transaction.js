
// db.version(1).stores({
//     colleges: 'id,name',
//     transactions: 'id,Amount,collected_by,paid_via,status,db',
//     participants: 'id,name,email,colleges,mobile,owner,db'
// });


// const base_url = 'https://upskilladmin.techzillaindia.com'
// const base_url = 'http://localhost:1337'

var collegeList = {}
var colleges = []
var owner = Cookies.get('_id')
var auth_role = Cookies.get('_role')
var uName = Cookies.get('_uName')

var allow = true

var normalize_participants = {}
var currentId = ''

var clear = true

var couponCodes = []
var couponCodeList = {}

var slots_count = 0

var setTimeoutId = undefined

var query = {
  numberOfRows: 10,
  currentIndex: 0,
  search_text: '',
  filter: '',
}

// Fill the exam drop down
function getExams() {
  $.ajax({
    url: `${base_url}/exams`,
    type: 'GET',
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (data) {
      fillDropDown(data, $('#Exam_uid'))
      fillDropDown(data, $('#deactivate_exam_uid'))
    },
    error: function (err) {

    }
  })
}

// function getVenues() {
//   $.ajax({
//     url: `${base_url}/examvenues`,
//     type: 'GET',
//     headers: {
//       Authorization: `Bearer ${Cookies.get('_t')}`
//     },
//     success: function (data) {
//       fillDropDown(data, $('#venues'))
//     },
//     error: function (err) {

//     }
//   })
// }


getExams()
// getVenues()

function fillDropDown(data, element) {
  // console.log(data)
  let op = ''
  data.forEach(function (item) {
    op += `<option value="${item._id}">${item.name}</option>`
  })
  element.append(op)
}


function getTransactionCount() {

  let rows = $('#number_of_rows').val()

  let old_IIT_exam = $('#Exam_uid').val();
  let filter_status_examVal = $('#select-status').val()
  let filter_status_text = $('#select-status').find(":selected").text()
  let filter_exam_selected_status = $('#select-status').val();

  if (old_IIT_exam == "5cf12da8fb1d4671819654c0") {

  }
  else if ((filter_status_examVal == '1st' || filter_status_examVal == '2nd') && old_IIT_exam != "5cf12da8fb1d4671819654c0") {
    filter_exam_selected_status = filter_status_text;
  }

  // console.log(filter_exam_selected_status)

  query = {
    ...query,
    numberOfRows: parseInt(rows),
    search_text: $('#search_text').val(),
    filter: filter_exam_selected_status,
    exam: $('#Exam_uid').val(),
  }

  $.ajax({
    url: `${base_url}/participant_filter_count`,
    type: 'POST',
    data: query,
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (data) {
      // Returns number of rows available for given @query
      $('#slots_count').html(data.count)

      // This function takes following arguments

      /*  
        table id, 
        count of rows, 
        @query, 
        number of columns in table,
        callback
      */

      // This function takes care of building pagination options and inserts it into tables footer
      // This function will internally call the given call back function
      // Use that call back to dynamically insert table row in table
      pagination('datatable', data.count, query, 9, fillTable)
    },
    error: function (err) {

    }
  })
}


$(document).ready(function () {
  getTransactionCount()
})

$('#number_of_rows').change(function () {
  getTransactionCount()
})

$('#select-status').change(function () {
  getTransactionCount()
})

$('#Exam_uid').change(function () {
  // get timeslot according to exam selected
  let examSelected = $('#Exam_uid').find(":selected").val();

  $('#select-status option[value="1st"]').text('')
  $('#select-status option[value="2nd"]').text('')

  $('#select-status option[value="1st"]').hide();
  $('#select-status option[value="2nd"]').hide();

  if (examSelected != "Select") {
    getCurrentTimeSlotDetails(examSelected)

  }
  getTransactionCount()
})

function getCurrentTimeSlotDetails(exam_id) {
  $.ajax({
    type: "GET",
    url: `${base_url}/exams/${exam_id}`,
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (data) {

      // examsVenueSlotObj = {
      //   "slot1": data.time_slot_1,
      //   "slot2": data.time_slot_2,
      //   "exams_id": data._id,
      // }

      if (data.time_slot_2 == "") {
        $('#select-status option[value="1st"]').text(data.time_slot_1);
        $('#select-status option[value="1st"]').show();

      }
      else {
        $('#select-status option[value="1st"]').text(data.time_slot_1);
        $('#select-status option[value="2nd"]').text(data.time_slot_2);

        $('#select-status option[value="1st"]').show();
        $('#select-status option[value="2nd"]').show();
      }


    }
  })
}

// $('#venues').change(function () {
//   getTransactionCount()
// })

$('#search_text').on('input', function () {

  if (setTimeoutId != undefined) {
    clearInterval(setTimeoutId)
  }

  setTimeoutId = setTimeout(function () {
    getTransactionCount()
  }, 500)

})

// Used to get count of Initiated, Confirmed and Completed transactions to show it in stats
function getStatusCount(val, cb) {
  $.ajax({
    type: "GET",
    url: `${base_url}/transactions/count?status=${val}`,
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (count) {
      cb(count)
    },
    error: function (err) {
      cb(0)
    }
  });
}

getStatusCount('Initiated', function (count) {
  $('#totalInitiated').text(count)
})

getStatusCount('Confirmed', function (count) {
  $('#totalConfirmed').text(count)
})

getStatusCount('Completed', function (count) {
  $('#totalComplete').text(count)
})


if (auth_role == 'authenticated') {
  $('#AmbassadorList').css('display', 'block')
}

$('#myUpdateModal').on('hidden.bs.modal', function () {
  currentId = ''
})

function openTrx(e) {

  currentId = e.target.id
  let _t = normalize_participants[currentId].transaction
  // debugger
  $('#remaining-amt').val(Array.isArray(_t) ? _t[0].Amount.rem : _t.Amount.rem)
  $('#myUpdateModal').modal('show');

}

// $('#remaining-amt').blur(function () {
//   if ($(this).val().length <= 0) {
//     $(this).parent().parent().addClass('has-error')
//   } else {
//     $(this).parent().parent().removeClass('has-error')
//   }

// })


// Mark transaction complete
function updateTrxData() {

  let _t = normalize_participants[currentId].transaction
  let trx = Array.isArray(_t) ? _t[0] : _t
  trx.Amount.rem = $('#remaining-amt').val()
  trx.collected_by.rem = uName
  trx.paid_via.rem = 'cash'
  trx.status = 'Completed'
  trx.checkTzId = true;

  let _participant_ = trx.participants

  // debugger
  // #transNoIssue trx.participants gets deleted at backend after participant got its TZ ID
  $.ajax({
    type: "PUT",
    url: `${base_url}/transactions/${trx._id}`,
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    data: trx,
    success: function (data) {


      $.ajax({
        type: "GET",
        url: `${base_url}/participants/${_participant_}`,
        headers: {
          Authorization: `Bearer ${Cookies.get('_t')}`
        },
        success: function (participantData) {
          // debugger
          let name = getFirstName(titleCase(participantData.name))

          let venue_date = participantData.examvenue.name.split('-')
          // TODO: Uncomment to activate mail and sms

          sendMailAfterTransaction(participantData.email, name, participantData.uid, venue_date[0], venue_date[1])
          sendSmsAfterTransaction(participantData.mobile, name, participantData.uid, venue_date[0], venue_date[1])

          toastr.options.onclick = toastr.clear()
          toastr.success('Successful', { timeOut: 2500 })
          toastr.options.progressBar = true;
          $('#myUpdateModal').modal('hide');

          createCouponCode(
            participantData._id,
            name,
            participantData.email,
            participantData.mobile
          )
        }

      })


    },
    error: function (err) {
      console.log(err)
    }
  });
}

//  create rows
function fillTable(q, cb) {

  smsCheckedList = []
  emailCheckedList = []
  deleteCheckedList = []

  $('#smsbtn').fadeOut()
  $('#mailbtn').fadeOut()
  $('#delbtn').fadeOut()

  let send_data = {
    ...q,
    currentIndex: parseInt(q.currentIndex) * q.numberOfRows
  }

  allow = true

  let tr = ``
  $.ajax({
    type: "POST",
    url: `${base_url}/participant_filter`,
    data: send_data,
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (data) {

      if (auth_role == 'authenticated') {
        data.forEach(item => {
          tr += generateRow(item)
        })
      } else {
        data.forEach(item => {
          if (owner == item.participants.owner) {
            tr += generateRow(item)
          }
        })
      }

      $('.tempTbl').remove()

      $('#table-res').append(getRawTable())


      $('.tempTr, .footable-row-detail').remove()
      $("#trans-table").append(tr)
      $('#datatable').footable();

      // Excel code ..

      // $("#datatable").tableExport({

      //   footers: false,
      //   formats: ["xls", "csv", "xlsx"],
      //   fileName: "id",
      //   bootstrap: true,
      //   position: "bottom",
      // });

      checkBoxEventBinding()
      cb()

    },
    error: function (err) {
      console.log(err)
    }
  });
}



function checkBoxEventBinding() {

  $("#th-checkbox").find('.footable-sort-indicator').css('display', 'none')

  $('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
  });


  $('.i-checks').on('ifChecked', function (event) {
    if (event.target.id == 'checkAll' && allow == true) {
      allow = false
      $('.i-checks').iCheck('check')
    }
    if ($('.i-checks:checkbox:checked').length > 0) {
      // $('#change-status-div').fadeIn()
      $('#smsbtn').fadeIn()
      $('#mailbtn').fadeIn()
      $('#delbtn').fadeIn()
    }
    else {
      // $('#change-status-div').fadeOut()
      $('#smsbtn').fadeOut()
      $('#mailbtn').fadeOut()
      $('#delbtn').fadeOut()
    }
  });


  $('.i-checks').on('ifUnchecked', function (event) {
    if (event.target.id == 'checkAll' && allow == false) {
      allow = true
      $('.i-checks').iCheck('uncheck')
    }
    if ($('.i-checks:checkbox:checked').length > 0) {
      // $('#change-status-div').fadeIn()
      $('#smsbtn').fadeIn()
      $('#mailbtn').fadeIn()
      $('#delbtn').fadeIn()
    } else {
      // $('#change-status-div').fadeOut()
      $('#smsbtn').fadeOut()
      $('#mailbtn').fadeOut()
      $('#delbtn').fadeOut()
    }
  });


  $('.updateTrx').on('click', openTrx)

  $('caption.btn-toolbar').css({ 'display': 'table-caption' })
  $('caption.btn-toolbar').find('button').css({ 'margin': '5px' })
}


// Multiple sms code
var smsCheckedList = []
$('#smsbtn').on('click', function () {
  smsCheckedList = []
  $('.i-checks:checkbox:checked').each(function () {
    if ($(this).attr('data-key') != undefined) {
      smsCheckedList.push(normalize_participants[$(this).attr('data-key')]['mobile'])
    }
  })
  $('#myModalSms').modal('show');

})


// Multiple email code
var emailCheckedList = []
$('#mailbtn').on('click', function () {
  emailCheckedList = []
  $('.i-checks:checkbox:checked').each(function () {
    if ($(this).attr('data-key') != undefined) {
      emailCheckedList.push(normalize_participants[$(this).attr('data-key')]['email'])
    }
  })

  $('#myModalMail').modal('show');
})



// Delete Student and transaction code
var deleteCheckedList = []

$('#delbtn').on('click', function () {
  deleteCheckedList = []
  $('.i-checks:checkbox:checked').each(function () {
    if ($(this).attr('data-key') != undefined) {
      deleteCheckedList.push($(this).attr('data-key'))
    }
  })

  $('#remove-total').text(`Delete ${deleteCheckedList.length} records.`)
  $('#myModalDelete').modal('show');
})


// Deactivate participants
$("#deactivateParticipants").click(function () {
  $('#deactivateModal').modal('show');
})


$("#btnDeactivate").click(function () {
  $.ajax({
    type: "POST",
    url: `${base_url}/mark_all_inactive`,
    data: { exam: $('#deactivate_exam_uid').val(), is_active: false },
    success: function (data) {
      console.log(data)
      $('#deactivateModal').modal('hide');
    },
    error: function (err) {
      console.log(err)
      alert('Something went wrong')
    }
  })
})

$("#btnActivate").click(function () {
  $.ajax({
    type: "POST",
    url: `${base_url}/mark_all_inactive`,
    data: { exam: $('#deactivate_exam_uid').val(), is_active: true },
    success: function (data) {
      $('#deactivateModal').modal('hide');
    },
    error: function (err) {
      console.log(err)
      alert('Something went wrong')
    }
  })
})

//  Update transaction code
// var updateCheckedList = []

// $('#change-status').change(function () {
//   updateCheckedList = []

//   $('.i-checks:checkbox:checked').each(function () {
//     if ($(this).attr('data-key') != undefined) {
//       updateCheckedList.push($(this).attr('data-key'))
//     }
// })

// Update all ---- un comment to activate (call)

// if ($('#change-status').val() != 'Select') {
//   updateAll($('#change-status').val(), updateCheckedList)
// }

// })


function getRawTable() {
  return `<table id="datatable" class="tempTbl table table-stripped"  data-page-size="${$('#number_of_rows').val()}">
              <thead>
                  <tr>
                      <th>TZ-Id</th>
                      <th>Name</th>
                      <th>Contact</th>
                      <th>Technical</th>
                      <th>Aptitude</th>
                      <th>Verbal</th>
                      <th>Total</th>
                      <th>Status</th>
                      <th id="th-checkbox">
                      <div><input id="checkAll" type="checkbox" class="i-checks" /></div>
                      </th>
                      
                      <th data-hide="all">Time Slot</th>
                      <th data-hide="all">Origin</th>
                      <th data-hide="all">Picture</th>
                      <th data-hide="all">DOB</th>
                      <th data-hide="all">College</th>
                      <th data-hide="all">Email</th>
                      <th data-hide="all">Coupon</th> 
                      <th data-hide="all">Amount</th>
                      <th data-hide="all">Referred By</th>
                      <th data-hide="all">Training Batch</th>
                  </tr>
                </thead>

                <tbody id="trans-table">

                </tbody>
              
              </table>`
}

function generateRow(i) {

  // i | refers to single participant

  normalize_participants[i._id] = i

  let state = ``

  let trx = Array.isArray(i.transaction) ? i.transaction[0] : i.transaction

  if (trx == undefined) {
    //  set default
    trx = {
      Amount: { initial: "", rem: "", },
      applied_code: null,
      collected_by: { initial: "", rem: "" },
      paid_via: { initial: "", rem: "" },
      participants: "",
      status: "",
      updatedAt: "",
      _id: "",
    }
  }

  let marks = {
    tech: 0,
    verb: 0,
    apti: 0,
    total: 0
  }

  if (i.res.length > 0) {
    let mark_list = i.res[0]
    marks.tech = mark_list.tech
    marks.verb = mark_list.verb
    marks.apti = mark_list.apti
    marks.total = mark_list.total
  }

  // console.log(mark)

  if (trx.status == 'Initiated') {
    state += `<label class="label label-warning">${trx.status}</label>`
  } else if (trx.status == 'Confirmed') {
    state += `<label class="label label-info">${trx.status}</label>`
  } else if (trx.status == "Completed") {
    state += `<label class="label label-success">${trx.status}</label>`
  } else {
    state += `<label class="label label-danger">Pending</label>`
  }

  var couponCodes = []

  i.couponCode.forEach(function (ccode) {
    couponCodes.push(ccode.unique_code)
  })

  var returnRow = `<tr class="tempTr">
                    <td>${i.uid}</td>
                    <td>${i.name}</td>
                    <td>${i.mobile}</td>
                    <th>${marks.tech}</th>
                    <th>${marks.verb}</th>
                    <th>${marks.apti}</th>
                    <th>${marks.total}</th>                            
                    <td>${state}</td>

                    <td> 
                    <div class="row"> 
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                      <input data-key=${i._id} type="checkbox" class="i-checks" />
                    </div>
                  
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                      <button class="btn updateTrx" id=${i._id}>Update</button> 
                    </div>
                  </div>
                    </td> 

                    <td>${i.time_slots == '' ? '' : i.time_slots == '1st' ? "11:00 AM" : i.time_slots == '2nd' ? "01:30 PM" : i.time_slots}</td>
                    <td>${i.city}, ${i.state}</td>
                    <td>${i.pic == '' ? 'No' : 'Yes'}</td>
                    <td>${i.dob}</td>
                    <td>${i.college.name}</td>
                    <td>${i.email}</td>                   
                    <td>${couponCodes}</td>
                    <td>Initial ${trx.Amount.initial} <br/> Rem ${trx.Amount.rem}</td>
                    <td>${trx.applied_code == null ? '' : trx.applied_code}</td>
                    <td>${i.training_batch ? i.training_batch : ''}</td>
                </tr>`

  return returnRow
}


function get_marks(ids, cb) {
  $.ajax({
    type: "GET",
    url: `${base_url}/res?${ids}`,
    // headers: {
    //   Authorization: `Bearer ${Cookies.get('_t')}`
    // },
    success: function (data) {
      let marks = {}
      data.forEach(item => {
        marks[item.participants._id] = item
      })

      cb(marks)
    },
    error: function (err) {
      console.log(err)
    }
  });
}


// Update all --- uncomment to activate following block of code

// function updateAll(val, checkedKeys) {
//   $.ajax({
//     type: "PUT",
//     url: `${base_url}/transactionsUpdate`,
//     data: { status: val, keys: checkedKeys },
//     headers: {
//       Authorization: `Bearer ${Cookies.get('_t')}`
//     },
//     success: function (data) {
//       fillTable('?_sort=_id:DESC&_limit=10000000')
//     },
//     error: function (err) {
//       console.log(err)
//     }
//   });
// };

