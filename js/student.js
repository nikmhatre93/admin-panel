// const base_url = 'https://upskilladmin.techzillaindia.com'
// const base_url = 'http://localhost:1337'

const urlParams = new URLSearchParams(window.location.search)
const sid = urlParams.get('sid')

const owner = Cookies.get('_id')
const auth_role = Cookies.get('_role')

var stdData = {}
var allow = true

if (auth_role == 'authenticated') {
    $('#AmbassadorList').css('display', 'block')
}

var collegeList = {}
var colleges = []

var couponCodes = []
var couponCodeList = {}

var trasnsactionList = []
var trasnsactionalStudentList = []

var Initiated = 0
var Confirmed = 0
var Completed = 0
var Pending = 0
var total = 0


var row_generated = 0

var customPagination = {
    current: 0,
    limit: 100,
    skip: 0,
    total_pages: 0
}

var recordsPerCall = 100000000


// $.ajax({
//     type: "GET",
//     url: `${base_url}/participants/count`,
//     headers: {
//         Authorization: `Bearer ${Cookies.get('_t')}`
//     },
//     success: function (data) {
//         debugger
//         customPagination.total_pages = Math.ceil(parseInt(data) / recordsPerCall)
//         generatePaginationButtons()

//     },
//     error: function (err) {
//         console.log(err)
//     }
// });


// function generatePaginationButtons() {
//     var cls = ''
//     $('.tmpPaginateBtn').remove()

//     var sp = ``
//     for (var num = 0; num < customPagination.total_pages; num++) {
//         cls = num == customPagination.current ? 'btn-primary' : ''
//         sp += `<button class="tmpPaginateBtn btn ${cls}" data-count="${num * recordsPerCall}" style="margin:2px;">${num + 1}</button>`
//     }
//     $('#customPagination').append(sp)

//     $('.tmpPaginateBtn').click(function () {

//         console.log($(this).data('count'))
//         customPagination.skip = $(this).data('count')
//         customPagination.current = parseInt($(this).data('count')) / recordsPerCall

//         generatePaginationButtons()
//         gernerateTable()
//     })
// }


$('#select_status').change(function () {
    gernerateTable()
})


// get college list
// $.ajax({
//     type: "GET",
//     url: `${base_url}/colleges?_limit=100000000`,
//     headers: {
//         Authorization: `Bearer ${Cookies.get('_t')}`
//     },
//     success: function (data) {
//         colleges = data
//         getTransaction()
//     },
//     error: function (err) {
//         console.log(err)
//     }
// });


// get trasnsactions

function getTransaction() {
    $.ajax({
        type: "GET",
        url: `${base_url}/transactions?_limit=100000000`,
        headers: {
            Authorization: `Bearer ${Cookies.get('_t')}`
        },
        success: function (data) {
            trasnsactionList = data
            gernerateTable()
        },
        error: function (err) {
            console.log(err)
        }
    });
}


function getCouponCode() {
    $.ajax({
        type: "GET",
        url: `${base_url}/couponcodes?_limit=10000000`,
        headers: {
            Authorization: `Bearer ${Cookies.get('_t')}`
        },
        success: function (data) {
            couponCodes = data

        },
        error: function (err) {
            console.log(err)
        }
    });
}
getCouponCode()


// 

function fillTable(uri, type) {
    row_generated = 0
    stdData = {}
    Initiated = 0
    Confirmed = 0
    Completed = 0
    Pending = 0
    total = 0
    allow = true
    $.ajax({
        type: "GET",
        url: uri,
        headers: {
            Authorization: `Bearer ${Cookies.get('_t')}`
        },
        success: function (data) {

            colleges.forEach(element => {
                collegeList[element._id] = element.name
            });


            trasnsactionList.forEach(element => {
                if (element.participants != undefined || element.participants != null) {
                    trasnsactionalStudentList.push(element.participants._id)
                }

            })

            couponCodes.forEach(function (item) {
                if (item.owner != undefined || item.owner != null) {
                    couponCodeList[item.owner._id] = item.unique_code
                }

            })

            $(`.tempTr${type}, .footable-row-detail`).remove()
            let tempTr = appendTable(data, type)
            $("#trans-table").append(tempTr)


            $("#datatable").tableExport({

                footers: false,
                formats: ["xls", "csv", "xlsx"],
                fileName: "id",
                bootstrap: true,
                position: "bottom",
            });

            $('.footable').footable();


            $("#th-checkbox").find('.footable-sort-indicator').css('display', 'none')

            if (auth_role == 'authenticated') {
                $("#th-checkbox").fadeIn()
                $(".td-checkbox").fadeIn()
            }

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });


            $('.i-checks').on('ifChecked', function (event) {
                if (event.target.id == 'checkAll' && allow == true) {
                    allow = false
                    $('.i-checks').iCheck('check')
                }
                if ($('.i-checks:checkbox:checked').length > 0) {
                    $('#smsbtn').fadeIn()
                    $('#mailbtn').fadeIn()
                    $('#delbtn').fadeIn()
                }
                else {
                    $('#smsbtn').fadeOut()
                    $('#mailbtn').fadeOut()
                    $('#delbtn').fadeOut()
                }
            });


            $('.i-checks').on('ifUnchecked', function (event) {
                if (event.target.id == 'checkAll' && allow == false) {
                    allow = true
                    $('.i-checks').iCheck('uncheck')
                }

                if ($('.i-checks:checkbox:checked').length > 0) {
                    $('#smsbtn').fadeIn()
                    $('#mailbtn').fadeIn()
                    $('#delbtn').fadeIn()
                } else {
                    $('#smsbtn').fadeOut()
                    $('#mailbtn').fadeOut()
                    $('#delbtn').fadeOut()
                }
            });


            $('#_totalStudents').text(`Students ${total}`)
            $('#_totalPending').text(`Pending ${Pending}`)
            $('#_totalInitiated').text(`Initiated ${Initiated}`)
            $('#_totalComfirmed').text(`Confirmed ${Confirmed}`)
            $('#_totalCompleted').text(`Completed ${Completed}`)
            $('#_status').show()


            $('caption.btn-toolbar').css({ 'display': 'table-caption' })
            $('caption.btn-toolbar').find('button').css({ 'margin': '5px' })

        },
        error: function (err) {
            console.log(err)
        }
    });
}

function appendTable(data, t) {
    let tr = ``

    data.forEach(item => {
        stdData[item.id] = item
        tr += generateRow(item, t)
    })

    return tr
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

var monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']


let tbl = `<table id="datatable" class="footable table" data-page-size="100" data-filter=#filter>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            
                            <th>DOB</th>
                            <th>College</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>City</th>
                            <th>Created At</th>
                            <th>FOS</th>
                            <th>Working</th>
                            <th>Code</th>
                            
                            <th>Status</th>

                            <th style="display:none" id="th-checkbox"><input id="checkAll" type="checkbox" class="i-checks" /></div></th>
                            
                            <th data-hide="all">Paid Via</th>
                            <th data-hide="all">Amount</th>
                            <th data-hide="all">Collected By</th>
                        </tr>
                    </thead>
                    <tbody id="trans-table">

                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="13">
                            <ul class="pagination float-right"></ul>
                        </td>
                    </tr>
                </tfoot>
                </table>`



// <th>Pic</th>
//             <th>Time Slot</th>


function generateRow(i, type) {

    let checkStatus = ''

    try {

        let exits = false
        let newDate = new Date(i.createdAt)

        total += 1

        let amount = `<div>
    <span>Initial:</span>
    <br/>
    <span>Rem:</span>
    </div>`

        let paid_via = `<div>
    <span>Initial:</span>
    <br/>
    <span>Rem:</span>
    </div>`

        let collected_via = `<div>
    <span>Initial:</span>
    <br/>
    <span>Rem:</span>
    </div>`

        let status = ''

        exits = trasnsactionalStudentList.includes(i._id)

        if (exits) {
            trasnsactionList.forEach(item => {
                if (item.participants._id == i._id) {

                    amount = `<div>
                <span>Initial: ${item.Amount.initial}</span>
                <br/>
                <span>Rem: ${item.Amount.rem}</span>
                </div>`

                    paid_via = `<div>
                <span>Initial: ${item.paid_via.initial}</span>
                <br/>
                <span>Rem: ${item.paid_via.rem}</span>
                </div>`

                    collected_via = `<div>
                <span>Initial: ${item.collected_by.initial}</span>
                <br/>
                <span>Rem: ${item.collected_by.rem}</span>
                </div>`


                    if (item.status == 'Initiated') {
                        Initiated += 1
                        status = `<label class="label label-warning">${item.status}</label>`
                    } else if (item.status == 'Confirmed') {
                        Confirmed += 1
                        status = `<label class="label label-info">${item.status}</label>`
                    } else {
                        Completed += 1
                        status = `<label class="label label-success">${item.status}</label>`
                    }

                    checkStatus = item.status

                }

            })
        } else {
            Pending += 1
            checkStatus = 'Pending'
            status = '<label class="label label-danger">Pending</label>'
        }

        let cls = 'i-checks'



        let returnRow = `<tr class="tempTr${type}">
        <td class="tableexport-string target">${i.uid}</td>
        <td>${i.name}</td>
        <td>${i.dob}</td>
        <td>${collegeList[i.colleges._id]}</td>
        <td>${i.email}</td>
        <td>${i.mobile}</td>
        <td>${i.city}</td>
        <td>${newDate.getDate()}-${monthList[newDate.getMonth()]} | ${formatAMPM(newDate)}</td>
        <td>${i.degree} ${i.branch}</td>
        <td>${i.working}</td>
        <td>${couponCodeList[i._id]}</td>
        
        <td>${status}</td>
        <td  style="display:none" class="td-checkbox"> <input data-key=${i._id} type="checkbox" class=${cls} /></div></td>
        
        <td>${paid_via}</td>
        <td>${amount}</td>
        <td>${collected_via}</td>
    </tr>`

        // <td>${i.pic == '' ? 'No' : 'Yes'}</td>
        //     <td>${i.time_slots == '' ? '' : i.time_slots == '1st' ? "11:00 AM" : "01:30 PM"}</td>
        if ($('#select_status').val() == "Show All") {
            row_generated += 1

        } else if ($('#select_status').val() == "Pending") {
            if (checkStatus != 'Pending') {
                returnRow = ''
            } else {
                row_generated += 1
            }

        } else if ($('#select_status').val() == "Initiated") {
            if (checkStatus != 'Initiated') {
                returnRow = ''
            } else {
                row_generated += 1
            }


        }
        else if ($('#select_status').val() == "Confirmed") {
            if (checkStatus != 'Confirmed') {
                returnRow = ''
            } else {
                row_generated += 1
            }

        } else {
            if (checkStatus != 'Completed') {
                returnRow = ''
            } else {
                row_generated += 1
            }

        }

        return returnRow

    } catch (error) {

    }
}


function gernerateTable() {
    $('.footable').remove()
    $('#table-res').append(tbl)

    if ('authenticated' == auth_role) {
        if (sid != null) {
            fillTable(`${base_url}/participantsAll?owner=${sid}&_limit=${recordsPerCall}&_start=${customPagination.skip}`, 'std')
        } else {
            fillTable(`${base_url}/participants?_sort=createdAt:DESC&_limit=${recordsPerCall}&_start=${customPagination.skip}`, 'std')
        }

    } else {

        fillTable(`${base_url}/participantsAll?_limit=${recordsPerCall}&_start=${customPagination.skip}&owner=${owner}`, 'std')
    }
}

// gernerateTable()

let smsCheckedList = []
$('#smsbtn').on('click', function () {
    smsCheckedList = []
    $('.i-checks:checkbox:checked').each(function () {
        if ($(this).attr('data-key') != undefined) {
            smsCheckedList.push(stdData[$(this).attr('data-key')]['mobile'])
        }
    })
    $('#myModalSms').modal('show');

})


let emailCheckedList = []
$('#mailbtn').on('click', function () {
    emailCheckedList = []
    $('.i-checks:checkbox:checked').each(function () {
        if ($(this).attr('data-key') != undefined) {
            emailCheckedList.push(stdData[$(this).attr('data-key')]['email'])
        }
    })

    $('#myModalMail').modal('show');
})

let deleteCheckedList = []

$('#delbtn').on('click', function () {
    deleteCheckedList = []
    $('.i-checks:checkbox:checked').each(function () {
        if ($(this).attr('data-key') != undefined) {
            deleteCheckedList.push($(this).attr('data-key'))
        }
    })

    $('#remove-total').text(`Delete ${deleteCheckedList.length} records.`)
    $('#myModalDelete').modal('show');
})
