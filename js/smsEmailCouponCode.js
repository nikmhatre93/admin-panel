function sendMails() {
  let mailMessage = $("#mailMessage")
  let sub = $("#mailSubject")

  $.ajax({
    type: "POST",
    url: `${base_url}/customSendMail`,
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    data: {
      "to": emailCheckedList,
      "from": "upskill@techzillaindia.com",
      "subject": sub.val(),
      "html": mailMessage.val()
    },
    success: function (data) {
      $('#myModalMail').modal('hide');
      mailMessage.val('')
      sub.val('')
      toastr.success('Email has been sent', { timeOut: 2500 })
      emailCheckedList = []
    },
    error: function (err) {
      toastr.error('Failed', { timeOut: 2500 })
    }
  });
}


function sendSms() {
  let msg = $('#messageToSend')
  let smsList = smsCheckedList.join(',')
  let txt = msg.val()

  $.ajax({
    type: "POST",
    url: `${base_url}/sendSms`,
    data: { mobile: smsList, msg: txt },
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (data) {
      $('#myModalSms').modal('hide');
      msg.val('')

      toastr.success('Sms has been sent', { timeOut: 2500 })
      smsCheckedList = []
    },
    error: function (err) {

    }
  });
}

function removeRecords() {
  $.ajax({
    type: "POST",
    url: `${base_url}/deleteParticipantsAndRelations`,
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    data: {
      ids: deleteCheckedList
    },
    success: function (data) {
      $('#myModalDelete').modal('hide');
      $('#remove-total').text(``)
      toastr.success(`${deleteCheckedList.length} record deleted`, { timeOut: 2500 })
      deleteCheckedList = []
      getTransactionCount()
    },
    error: function (err) {
      toastr.error('Failed', { timeOut: 2500 })
    }
  });
}




function sendMailAfterTransaction(email, name, tzid, venue, date) {

  let mailMessage = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
      
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Neopolitan Confirm Email</title>
        <!-- Designed by https://github.com/kaytcat -->
        <!-- Robot header image designed by Freepik.com -->
      
        <style type="text/css">
          @import url(http://fonts.googleapis.com/css?family=Droid+Sans);
      
          /* Take care of image borders and formatting */
      
          p{
              font-size:18px !important
          }
  
          img {
            max-width: 600px;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
          }
      
          a {
            text-decoration: none;
            border: 0;
            outline: none;
            color: #bbbbbb;
          }
      
          a img {
            border: none;
          }
      
          /* General styling */
      
          td,
          h1,
          h2,
          h3 {
            font-family: Helvetica, Arial, sans-serif;
            font-weight: 400;
          }
      
          td {
            text-align: center;
          }
      
          body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100%;
            height: 100%;
            color: #37302d;
            background: #ffffff;
            font-size: 16px;
          }
      
          table {
            border-collapse: collapse !important;
          }
      
          .headline {
            color: #ffffff;
            font-size: 36px;
          }
      
          .force-full-width {
            width: 100% !important;
          }
      
          .force-width-80 {
            width: 80% !important;
          }
        </style>
      
        <style type="text/css" media="screen">
          @media screen {
      
            /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
            td,
            h1,
            h2,
            h3 {
              font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
            }
          }
        </style>
      
        <style type="text/css" media="only screen and (max-width: 480px)">
          /* Mobile styles */
          @media only screen and (max-width: 480px) {
      
            table[class="w320"] {
              width: 320px !important;
            }
      
            td[class="mobile-block"] {
              width: 100% !important;
              display: block !important;
            }
      
      
          }
        </style>
      </head>
      
      <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
        bgcolor="#ffffff">
        <table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
          <tr>
            <td align="center" valign="top" bgcolor="#ffffff" width="100%">
              <center>
                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
                  <tr>
                    <td align="center" valign="top">
      
                      <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
                        style="margin:0 auto;">
                        <tr>
                          <td style="font-size: 15px; text-align:center;">
                            <br>
                            <img src="https://techzillaindia.com/template_images/upskill.png" height="50"
                              width="160" alt="">
                            <br>
                            <br>
                          </td>
                        </tr>
                      </table>
      
                      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#4dbfbf">
                        <tr>
                          <td>
                            <br>
                            <img src="https://techzillaindia.com/template_images/robomail.gif" width="224" height="240" alt="robot picture">
                          </td>
                        </tr>
                        <tr>
                          <td class="headline">
                          Congratulations!
                          </td>
                        </tr>
                        <tr>
                          <td>
      
                            <center>
                              <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="70%">
                                <tr>
                                  <td style="color:#083d3d;text-align: left;padding:5px;">
                                    <br>
      
                                    <p>Hey <span style="color:black"><b>${name}</b></span>,</p>
      
                                    <p>Your seat for the National Upskill Entrance Test (NUET) is confirmed with seat
                                    no <span style="color:black"><b>${tzid}</b></span>.</p>
                                    which will be held on <span style="color:black"><b>${date}</b></span> 
                                    at <span style="color:black"><b>${venue}</b></span>.
                                    
                                    <p>Gear up for NUET !</p>
                                    <p>For any further information
                                      <a href="https://www.techzillaindia.com/upskill" style="color: black;text-decoration: underline">Click
                                        here</a></p>
                                   
                                    
                                    <p>
                                        Admit card details will be sent 15 days prior to the test date.
                                        Download detailed Syllabus  
                                        <a style="color:#53f6c6;" href="http://bit.ly/nuetsyb">
                                        here
                                        </a>
                                      </p>
                                  </td>
                                </tr>
                              </table>
                            </center>
      
                          </td>
                        </tr>
                      </table>
      
      
                      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141"
                        style="margin: 0 auto">
                        <tr>
                          <td style="color:#bbbbbb; font-size:15px">
                            <br>
                            <span style="text-align: center;">
                            For any further assistance email us at <b style="color:black">upskill@techzillaindia.com</b>.
                            </span>
                          </td>
                        </tr>
      
                        <tr>
      
                          <td style="color:#bbbbbb; font-size:12px;">
                          <br> 
                            <span>National UpSkill program by Techzilla India Infotech</span>
      
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#bbbbbb; font-size:12px;">
                          <br>
                            © 2019 All Rights Reserved
                            <br>
                            <br>
                            <br>
                          </td>
                        </tr>
                      </table>
  
                    </td>
                  </tr>
                </table>
              </center>
            </td>
          </tr>
        </table>
      </body>
      
      </html>`
  let sub = "Successfully Enrolled"

  $.ajax({
    type: "POST",
    url: `${base_url}/email`,
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    data: {
      "to": email,
      "from": "upskill@techzillaindia.com",
      "subject": sub,
      "html": mailMessage
    },
    success: function (data) {
    },
    error: function (err) {

    }
  });

}


function sendSmsAfterTransaction(mobile, name, tzid, venue, date) {

  let txt1 = `Hey ${name},\nYour seat for the National upskill entrance Test is confirmed with seat no ${tzid}. For more details on National Upskill programme visit http://bit.ly/TZUPSKILL`
  let txt2 = `We have successfully received your payment for NUET. All the important updates will be sent to your number and email. Download Syllabus @ http://bit.ly/nuetsyb. Cheers and all the best!`

  smsAjax(mobile, txt1)
  smsAjax(mobile, txt2)

}


function smsAjax(mobile, txt) {
  $.ajax({
    type: "POST",
    url: `${base_url}/sendSms`,
    data: { mobile: "91" + mobile, msg: txt },
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (data) {

    },
    error: function (err) {

    }
  });
}


function titleCase(str) {
  var splitStr = str.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(' ');
}


function getFirstName(str) {
  var name = str.split(" ");
  return name[0]
}

function makeid() {
  var text = "TZ";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


function createCouponCode(owner, name, email, mobile) {

  $.ajax({
    type: "POST",
    url: `${base_url}/couponcodes`,
    data: {
      unique_code: makeid(),
      owner
    },
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (data) {
      referredTemplate = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Neopolitan Confirm Email</title>
  <!-- Designed by https://github.com/kaytcat -->
  <!-- Robot header image designed by Freepik.com -->
  
  <style type="text/css">
  @import url(http://fonts.googleapis.com/css?family=Droid+Sans);
  
  /* Take care of image borders and formatting */
  
  p {
    font-size: 18px !important
  }
  
  img {
    max-width: 600px;
    outline: none;
    text-decoration: none;
    -ms-interpolation-mode: bicubic;
  }
  
  a {
    text-decoration: none;
    border: 0;
    outline: none;
    color: #bbbbbb;
  }
  
  a img {
    border: none;
  }
  
  /* General styling */
  
  td,
  h1,
  h2,
  h3 {
    font-family: Helvetica, Arial, sans-serif;
    font-weight: 400;
  }
  
  td {
    text-align: center;
  }
  
  body {
    -webkit-font-smoothing: antialiased;
    -webkit-text-size-adjust: none;
    width: 100%;
    height: 100%;
    color: #37302d;
    background: #ffffff;
    font-size: 16px;
  }
  
  table {
    border-collapse: collapse !important;
  }
  
  .headline {
    color: #ffffff;
    font-size: 36px;
  }
  
  .force-full-width {
    width: 100% !important;
  }
  
  .force-width-80 {
    width: 80% !important;
  }
  </style>
  
  <style type="text/css" media="screen">
  @media screen {
  
    /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
    td,
    h1,
    h2,
    h3 {
      font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
    }
  }
  </style>
  
  <style type="text/css" media="only screen and (max-width: 480px)">
  /* Mobile styles */
  @media only screen and (max-width: 480px) {
  
    table[class="w320"] {
      width: 320px !important;
    }
  
    td[class="mobile-block"] {
      width: 100% !important;
      display: block !important;
    }
  
  
  }
  </style>
  </head>
  
  <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
  bgcolor="#ffffff">
  <table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
  <tr>
    <td align="center" valign="top" bgcolor="#ffffff" width="100%">
      <center>
        <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
          <tr>
            <td align="center" valign="top">
  
              <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
                style="margin:0 auto;">
                <tr>
                  <td style="font-size: 15px; text-align:center;">
                    <br>
                    <img src="https://techzillaindia.com/upskill/wp-content/uploads/2019/01/WHITE.png" height="50"
                      width="160" alt="">
                    <br>
                    <br>
                  </td>
                </tr>
              </table>
  
              <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#4dbfbf">
                <tr>
                  <td>
                    <br>
                    <img src="https://techzillaindia.com/template_images/robowave_03.gif" width="224" height="240" alt="robot picture">
                  </td>
                </tr>
                <tr>
                  <td class="headline">
                    Congratulations!
                  </td>
                </tr>
                <tr>
                  <td>
  
                    <center>
                      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="60%">
                        <tr>
                          <td style="color:#083d3d;text-align: left;">
                            <br>
  
                            <p>Hey <span style="color:black"><b>${name}</b></span>,</p>
  
                            Greetings from Techzilla India!
                            You can earn Rs. 100 cash reward on each successful enrollment of your friend and your friend gets 100 instant discount too.
                            <br/>
                            <br/>
                            Share the Referral code: <b>${data.unique_code}</b>,  
                            with your friends and ask them to register for NUET: <a href="https://techzillaindia.com/upskill/registration">https://techzillaindia.com/upskill/registration</a>
                            <br/>
                            <br/>
                            On every successful registration of your friend you will get an email & sms of his enrollment. 
                            <br/>
                            <br/>
                            Once you refer 3 friends, you can request a cashout via WhatsApp message on this 7304322623. 
                            <br/>
                            <br/>
                            We shall verify & issue your cashout in 2 working days. 
                            <br/>
                            <br/>
                            Minimum 3 referral required for cashout.
                            <br/>
                            <br/>
                            Payment method- Bank Transfer only.
                            <br/>
                            Details needed for cashout
                            <br/>
                            Payee Name
                            <br/>
                            Account Number
                            <br/>
                            IFSC CODE
                            <br/>
                            Branch Name
                            
                            <br>
                            <br>


                            <h3>You can use this message to forward in your groups.</h3>
                                                
                            <p>
                            *Dream job* with a guaranteed package of around *7LPA* but don't have enough skills ?
                              We have got your back 📇
                              <br>

                              *Techzilla India* & *E Cell, IIT Bombay* present <br>
                              *National Upskill Entrance Test* <br>
                              A search for India's next gen technology leaders <br>
                              <br>
                              What is *National Upskill Programme?*
                              <br>
                              💻100 Days no cost Intensive training programme.
                              <br>
                              💻Assured placement of INR 7LPA Avg salary. 
                              <br>
                              💻 Hands on experience on technologies used by Big giants like Facebook, Microsoft,Flipkart,Airbnb etc.
                              <br>
                              💻Training by industry experts & Live projects and certification at no cost.
                              <br>
                              *No CGPA Criteria* : Students studying in Final year or graduates from science stream(BTech,BE,ME,MCA,BCA,Bsc,MSc,diploma )
                              <br>
                              <br>
                              *Our process*
                              1.NUET(Entrance Test) <br>
                              2.Screening<br>
                              3.Training<br>
                              4.Get Placed. <br>
                              <br>
                              For more details on the training module & about the programme visit <span style="color:blue">https://techzillaindia.com/upskill</span>
                              <br>
                              
                              Limited seats and first come first serve basis.<br>

                              Register now at <span style="color:blue">https://techzillaindia.com/upskill/registration</span> <br>
                              Use my code ${data.unique_code} to get INR 100/- instant discount on the application fee.<br>
                              <br>

                              </p>

                          </td>
                        </tr>
                      </table>
                    </center>
  
                  </td>
                </tr>
              </table>
  
  
              <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141"
                style="margin: 0 auto">
                <tr>
                  <td style="color:#bbbbbb; font-size:15px">
                    <br>
                    <span style="text-align: center;">
                    For any further assistance email us at <b style="color:black">upskill@techzillaindia.com</b>.
                    </span>
  
                  </td>
                </tr>
  
                <tr>
  
                  <td style="color:#bbbbbb; font-size:12px;">
                    <br>
                    <br>
                    <span>National UpSkill program by Techzilla India Infotech</span>
  
                  </td>
                </tr>
                <tr>
                  <td style="color:#bbbbbb; font-size:12px;">
                    <br>
                    © 2019 All Rights Reserved
                    <br>
                    <br>
                    <br>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </center>
    </td>
  </tr>
  </table>
  </body>
  
  </html>`


      $.ajax({
        type: "POST",
        url: `${base_url}/email`,
        headers: {
          Authorization: `Bearer ${Cookies.get('_t')}`
        },
        data: {
          "to": email,
          "from": "upskill@techzillaindia.com",
          "subject": 'Get Rs.100 cash reward on referring your friends!',
          "html": referredTemplate
        },
        success: function (data) {
        },
        error: function (err) {
          // toastr.error('Failed', { timeOut: 2500 })
        }
      });

      // sendMailAfterTransaction(email, tkn, referredTemplate, '')
      smsAjax(mobile,
        `Refer your friends using code: ${data.unique_code} and get Rs.100 cash reward on each successful enrollment. Please check mail for more details.`)
    },
    error: function (err) {
    }
  });
}

