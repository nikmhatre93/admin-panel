// var db = new Dexie("upSkill");
// db.version(1).stores({
//     colleges: 'id,name',
//     transactions: 'id,Amount,collected_by,paid_via,status,db',
//     participants: 'id,name,email,colleges,mobile,owner,db'
// });

let auth_role = Cookies.get('_role') // logged in use role in db (Authenticated, public(ambassador) )
let owner = Cookies.get('_id') // logged in user mongo id
let inputData = {}
let temp;

const amt = 999


// degree / branch list
var bsc_msc = [
  "Chemistry",
  "Computer Games",
  "Computer Science",
  "Electronic",
  "Information Technology",
  "Mathematics",
  "Physics",
  "Other"
]

var be_me_btech_mtech = [
  "Automation and Robotics",
  "Automobile",
  "Chemical",
  "Computers",
  "Computer Science",
  "Electrical",
  "Electrical and Communications",
  "Electronics",
  "Electronics and Communication",
  "Electronics and Computer",
  "Electronics and Telecommunication",
  "Information Science",
  "Information Technology",
  "Information and Communication",
  "Instrumentation",
  "Mechanical",
  "Mechanical & Industrial",
  "Production & Industrial Engineering",
  "Software",
  "Other"
]

// const base_url = 'http://localhost:1337'
// const base_url = 'https://upskilladmin.techzillaindia.com'

// if ambassador then only student else both student and ambassador.
// ambassador is not in use any more will comment it for now. 

if ('authenticated' == auth_role) {
  $('#AmbassadorList').css('display', 'block')
  // $('#role').append(`<option value='Other'>Student</option><option value='Ambassador'>Ambassador</option>`)
  $('#role').append(`<option value='Other'>Student</option>`)
} else {
  $('#role').append(`<option value='Other'>Student</option>`)
  $('#colleges').prop('disabled', true);
}


$('#degree').change(function () {

  let val = $(this).val()
  if (val != '') {
    $('#branchDiv').fadeIn()

    if (val == 'BCA' || val == 'MCA') {
      $('#branch').removeAttr('required');
      $('#branch').hide()
      $('#branch').prev().hide()
    } else {
      $('#branch').attr("required", true)
      generateBranchList(val)
      $('#branch').show()
      $('#branch').prev().show()
    }
  }
  else {
    $('#branchDiv').fadeOut()
  }
})

$('#venue').change(function () {
  if ($(this).val() == '') {
    $(this).parent().addClass('has-error')
  } else {
    $(this).parent().removeClass('has-error')
  }
})


$('#gender').change(function () {
  if ($(this).val() == '') {
    $(this).parent().addClass('has-error')
  } else {
    $(this).parent().removeClass('has-error')
  }
})

$('#degree').change(function () {
  if ($(this).val() == '') {
    $(this).parent().addClass('has-error')
  } else {
    $(this).parent().removeClass('has-error')
  }
})

$('#branch').change(function () {
  if ($(this).val() == '') {
    $(this).parent().addClass('has-error')
  } else {
    $(this).parent().removeClass('has-error')
  }
})

$('#currentlyWorking').change(function () {
  if ($(this).val() == '') {
    $(this).parent().addClass('has-error')
  } else {
    $(this).parent().removeClass('has-error')
  }
})

$('#state').blur(function () {
  if ($(this).val() == '') {
    $(this).parent().addClass('has-error')
  } else {
    $(this).parent().removeClass('has-error')
  }
})

$('#city').blur(function () {
  if ($(this).val() == '') {
    $(this).parent().addClass('has-error')
  } else {
    $(this).parent().removeClass('has-error')
  }
})

$('#pincode').blur(function () {
  if ($(this).val() == '') {
    $(this).parent().addClass('has-error')
  } else {
    $(this).parent().removeClass('has-error')
  }
})

$('#dob').blur(function () {
  if ($(this).val() == '') {
    $(this).parent().addClass('has-error')
  } else {
    $(this).parent().removeClass('has-error')
  }
})

// Get all venues
function venue(tkn) {
  $.ajax({
    type: "GET",
    url: `${base_url}/examvenues`,
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data2) {
      $('._op2').remove()
      ops2 = ``
      data2.forEach(element => {
        // console.log(data2)
        ops2 += `<option class="_op2" 
        value=${element._id} data-exam="${element.exam._id}">
        ${element.name}</option>`
      });
      $('#venue').append(ops2)
      // console.log('venue')
    },
    error: function (err) { }
  });
}

venue(Cookies.get('_t'))

// Branch options
function generateBranchList(val) {

  $('.branchClass').remove()
  let op = ``
  if (val == 'BE' || val == 'ME' || val == 'BTech' || val == 'MTech') {

    be_me_btech_mtech.forEach(function (item) {
      op += `<option class="branchClass" value=${item}>${item}</option>`
    })
  } else if (val == 'BSc' || val == 'MSc') {

    bsc_msc.forEach(function (item) {
      op += `<option class="branchClass" value=${item}>${item}</option>`
    })
  }
  $('#branch').append(op)

}

let errors = {
  name: true,
  mobile: true,
  email: true,
  amount: true,
  amount2: true,
  password: true,
  amountFocus: false,
  amount2Focus: false,
}


// Get colleges
$.ajax({
  type: "GET",
  url: `${base_url}/colleges`,
  headers: {
    Authorization: `Bearer ${Cookies.get('_t')}`
  },
  success: function (data) {
    ops = ``
    data.forEach(element => {
      ops += `<option value=${element._id}>${element.name}</option>`
    });
    $('#colleges').append(ops)
    $('#colleges').val(Cookies.get('_clg'))
  },
  error: function (err) {
    console.log(err)
  }
});


$('#role').change(function () {

  if ($(this).val() != '0') {
    $(this).parent().removeClass('has-error')

  } else {
    $(this).parent().addClass('has-error')
  }
  if ($(this).val() == 'Other') {
    $('#passDiv').css('display', "none ")
    $('#amtDiv').fadeIn()
    $('#amtDiv2').fadeIn()
  } else {
    $('#amtDiv').css('display', "none ")
    $('#amtDiv2').css('display', "none ")
    $('#passDiv').fadeIn()
  }

});


$('#colleges').change(function () {

  if ($(this).val() != '') {
    $(this).parent().removeClass('has-error')
  } else {
    $(this).parent().addClass('has-error')
  }

});

$('#full-name').blur(function () {

  if ($(this).val().length <= 0) {
    $(this).next().fadeIn()
    $(this).parent().addClass('has-error')
  } else {
    $(this).next().css('display', "none ")
    $(this).parent().removeClass('has-error')
    errors.name = false
  }
});

$('#ambPassword').blur(function () {

  if ($(this).val().length <= 0) {
    // $(this).next().fadeIn()
    $(this).parent().addClass('has-error')
  } else {
    // $(this).next().css('display', "none ")
    $(this).parent().removeClass('has-error')
    errors.password = false
  }
});

$('#email').blur(function () {
  var email_instance = $(this)
  if (email_instance.val().length <= 0) {
    email_instance.next().text('This field cannot be empty.')
    email_instance.next().fadeIn()
    email_instance.parent().addClass('has-error')
  } else {
    if (isEmail(email_instance.val()) == true) {
      let url = ''
      if ($('#role').val() == 'Other') {
        url = `${base_url}/participantsFind`
        doesExist({ email: email_instance.val() }, url, function (d) {

          if (d == undefined) {
            email_instance.next().css('display', "none ")
            email_instance.parent().removeClass('has-error')
            errors.email = false

          } else {
            email_instance.parent().addClass('has-error')
            email_instance.next().text('Email entered already exists.')
            email_instance.next().fadeIn()
          }
        })
      } else {
        // Not in use for now
        // for ambassador code here 
        url = `${base_url}/userFind`
        doesExist({ email: email_instance.val() }, url, function (d) {

          if (d == undefined) {
            email_instance.next().css('display', "none ")
            email_instance.parent().removeClass('has-error')
            errors.email = false

          } else {
            email_instance.parent().addClass('has-error')
            email_instance.next().text('Email entered already exists.')
            email_instance.next().fadeIn()
          }
        })

      }

    } else {
      email_instance.next().text('Email entered is not valid.')
      email_instance.next().fadeIn()
      email_instance.parent().addClass('has-error')
    }
  }
});


$('#phone').blur(function () {
  var phone_instance = $(this)
  if (phone_instance.val().length < 10 || phone_instance.val().length > 10) {
    phone_instance.next().text('Number entered is not valid.')
    phone_instance.next().fadeIn()
    phone_instance.parent().addClass('has-error')
  } else {
    let url = ''
    if ($('#role').val() == 'Other') {
      url = `${base_url}/participantsFind`
      doesExist({ mobile: phone_instance.val() }, url, function (d) {

        if (d == undefined) {
          phone_instance.next().css('display', "none ")
          phone_instance.parent().removeClass('has-error')
          errors.mobile = false

        } else {
          phone_instance.parent().addClass('has-error')
          phone_instance.next().text('Number entered already exists.')
          phone_instance.next().fadeIn()
        }
      })
    } else {
      // Not in use for now
      // for ambassador code here
      url = `${base_url}/userFind`
      doesExist({ mobile: phone_instance.val() }, url, function (d) {

        if (d == undefined) {
          phone_instance.next().css('display', "none ")
          phone_instance.parent().removeClass('has-error')
          errors.mobile = false

        } else {
          phone_instance.parent().addClass('has-error')
          phone_instance.next().text('Number entered already exists.')
          phone_instance.next().fadeIn()
        }
      })
    }

  }
});


$('#amount').focus(function () {
  errors.amountFocus = true
})

$('#amount2').focus(function () {
  errors.amount2Focus = true
})

$('#amount').blur(function () {

  if ($(this).val().length <= 0) {
    $(this).next().text('This field cannot be empty.')
    $(this).next().fadeIn()
    $(this).parent().addClass('has-error')
  } else {
    let amt2 = $('#amount2')
    if (amt2.val() != $(this).val() && errors.amountFocus && errors.amount2Focus) {
      $(this).next().text('Amount Should be same.')
      $(this).next().fadeIn()
      $(this).parent().addClass('has-error')
      amt2.parent().addClass('has-error')
      amt2.next().text('Amount Should be same.')
      amt2.next().fadeIn()
    } else {
      $(this).next().css('display', "none ")
      $(this).parent().removeClass('has-error')
      amt2.parent().removeClass('has-error')
      amt2.next().css('display', "none ")
      errors.amount = false
    }

  }
});


$('#amount2').blur(function () {

  if ($(this).val().length <= 0) {
    $(this).next().text('This field cannot be empty.')
    $(this).next().fadeIn()
    $(this).parent().addClass('has-error')
  } else {
    let amt = $('#amount')
    if (amt.val() != $(this).val() && errors.amountFocus && errors.amount2Focus) {
      $(this).next().text('Amount Should be same.')
      $(this).next().fadeIn()
      $(this).parent().addClass('has-error')

      amt.parent().addClass('has-error')
      amt.next().text('Amount Should be same.')
      amt.next().fadeIn()
    } else {
      $(this).next().css('display', "none ")
      $(this).parent().removeClass('has-error')
      amt.parent().removeClass('has-error')
      amt.next().css('display', "none ")
      errors.amount2 = false
    }

  }
});


function isEmail(email) {

  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}


// function saveTrx() {
//     toastr.clear()
//     toastr.options.onclick = toastr.clear()

//     inputData = {
//         paid_via: {
//             initial: 'cash',
//             rem: ''
//         },
//         Amount: {
//             initial: $('#amount').val(),
//             rem: 0
//         },
//         collected_by: {
//             initial: Cookies.get('_uName'),
//             rem: ''
//         },
//         status: 'Initiated',
//         participants: temp._id
//     }

//     $.ajax({
//         type: "POST",
//         url: '${base_url}/transactions',
//         data: inputData,
//         headers: {
//             Authorization: `Bearer ${Cookies.get('_t')}`
//         },
//         success: function (data) {
//             temp = undefined

//             toastr.options.onclick = toastr.clear()
//             toastr.success('Successful', { timeOut: 2500 })
//             toastr.options.progressBar = true;
//             sendMails($('#email').val())
//             sendSms($('#phone').val())

//             $('#full-name').val('')
//             $('#email').val('')
//             $('#phone').val('')
//             $('#amount').val('')
//             $('#amount2').val('')

//         },
//         error: function (err) {
//             toastr.options.timeOut = 0;
//             toastr.options.extendedTimeOut = 0;
//             toastr.options.onclick = function () { saveTrx() }
//             toastr.error('Failed', 'Click to retry')
//         }
//     });
// }

$('#save-changes').on('click', saveData)

//  Create participant and transaction
function saveData() {

  toastr.clear()
  toastr.options.onclick = toastr.clear()

  let bool = false

  if ($('#role').val() == 'Other') {
    bool = !errors.amount2 &&
      !errors.amount &&
      !errors.name &&
      !errors.mobile &&
      !errors.email &&
      $('#colleges').val() != '' &&
      $('#amount').val() == $('#amount2').val()
      && $('#state').val() != ''
      && $('#city').val() != ''
      && $('#pincode').val() != ''
      && $('#gender').val() != ''
      && $('#degree').val() != ''
      && $('#currentlyWorking').val() != ''
      && $('#dob').val() != ''

    // console.log($('#colleges').val(), ',,,')
  } else {

    bool = !errors.password &&
      !errors.name &&
      !errors.mobile &&
      !errors.email &&
      $('#colleges').val() != ''
  }



  // Check if all the conditions are satisfied
  // then only make db call else show error
  if (bool == true) {
    let full_name = titleCase($('#full-name').val())
    if ($('#role').val() == 'Other') {
      inputData = {
        par: {
          name: $('#full-name').val(),
          email: $('#email').val(),
          mobile: $('#phone').val(),
          state: $('#state').val(),
          city: $('#city').val(),
          pincode: $('#pincode').val(),
          colleges: $('#colleges').val(),
          owner: owner,
          degree: $('#degree').val(),
          branch: $('#branch').val(),
          working: $('#currentlyWorking').val(),
          examvenue: $('#venue').val(),
          exam: $('#venue').find(":selected").data("exam"),
          gender: $('#gender').val(),
          dob: $("#dob").val()

        },
        trx: {
          paid_via: {
            initial: 'cash',
            rem: ''
          },
          Amount: {
            initial: $('#amount').val(),
            rem: amt - parseInt($('#amount').val())
          },
          collected_by: {
            initial: Cookies.get('_uName'),
            rem: ''
          },
          status: 'Initiated',
          participants: ''
        },
        email: {
          "to": $('#email').val(),
          "from": "upskill@techzillaindia.com",
          "subject": 'Registration initiated',
          "text": getEmailTemplate(full_name)
          // "html": `Hi ${full_name},<br/>Greetings from Techzilla India Infotech.We have received a token amount for the national upskill entrance exam which will be held on 17th march ,2019 at IIT Bombay,powai.<br/>To complete your registration please visit the following URL http://bit.ly/NUETTZ . For more details regarding the programme visit https://www.techzillaindia.com/upskill .<br/>For any further assistance feel free to reach us at 022-33721615.<br/>Thanks and regards.`
        },
        sms: `Hi ${getFirstName(full_name)}\nGreetings from Techzilla India Infotech\nWe have received your initial payment for NUET. To complete your registration and confirm your seat, visit http://bit.ly/NUETTZ .`
      }

      // checking pending #transNoIssue
      $.ajax({
        type: "POST",
        url: `${base_url}/participantsWithTransaction`,
        data: inputData,
        headers: {
          Authorization: `Bearer ${Cookies.get('_t')}`
        },
        success: function (data) {

          toastr.options.onclick = toastr.clear()
          toastr.success('Successful', { timeOut: 2500 })
          toastr.options.progressBar = true;

          // Moved to backend
          // sendMails($('#email').val())
          // sendSms($('#phone').val())


          $('#branch').fadeOut()
          $('#full-name').val('')
          $('#email').val('')
          $('#phone').val('')
          $('#amount').val('')
          $('#amount2').val('')
          $('#state').val('')
          $('#city').val('')
          $('#pincode').val('')
          $('#gender').val('')
          $('#currentlyWorking').val('')
          $('#venue').val('')
          $('#degree').val('')
          $('#branch').val('')
          $("#dob").val('')


        },
        error: function (err) {
          toastr.options.timeOut = 0;
          toastr.options.extendedTimeOut = 0;
          toastr.options.onclick = function () { saveData() }
          toastr.error('Failed', 'Click to retry')

        }
      });

      // db.participants.put({
      //     id: 'temp',
      //     colleges: $('#colleges').val(),
      //     email: $('#email').val(),
      //     mobile: $('#phone').val(),
      //     name: $('#full-name').val(),
      //     owner: '',
      //     db: false
      // })


      // send_message_add_participant(inputData, Cookies.get('_t'), function (data) {
      // db.participants.delete({ id: 'temp' })

      // db.participants.put({
      //     id: data.id,
      //     colleges: data.colleges,
      //     email: data.email,
      //     mobile: data.mobile,
      //     name: data.name,
      //     owner: data.owner,
      //     db: true
      // })


      //     toastr.options.onclick = toastr.clear()
      //     toastr.success('Successful', { timeOut: 2500 })
      //     toastr.options.progressBar = true;
      //     sendMails($('#email').val())
      //     sendSms($('#phone').val())

      //     $('#full-name').val('')
      //     $('#email').val('')
      //     $('#phone').val('')
      //     $('#amount').val('')
      //     $('#amount2').val('')
      // })


    } else {
      // Not in use for now
      //Create user  on if role 'Ambassador' selected

      inputData = {
        username: $('#email').val(),
        email: $('#email').val(),
        provider: 'Local',
        password: $('#ambPassword').val(),
        confirmed: true,
        blocked: false,
        role: '5c3f0961b8d94e09fc356c99',
        colleges: $('#colleges').val(),
        owner: owner,
        name: $('#full-name').val(),
        mobile: $('#phone').val(),
      }

      $.ajax({
        type: "POST",
        url: `${base_url}/users`,
        data: inputData,
        headers: {
          Authorization: `Bearer ${Cookies.get('_t')}`
        },
        success: function (data) {

          toastr.success('Successful')
          toastr.options.progressBar = true;

          $('#full-name').val('')
          $('#email').val('')
          $('#phone').val('')
          $('#amount').val('')
          $('#amount2').val('')
          $('#ambPassword').val('')
        },
        error: function (err) {
          toastr.options.timeOut = 0;
          toastr.options.extendedTimeOut = 0;
          toastr.options.onclick = function () { saveData() }
          toastr.error('Failed', 'Click to retry')
        }
      });
    }

  } else {
    if (errors.name) {
      $('#full-name').parent().addClass('has-error')
    } else {
      $('#full-name').parent().removeClass('has-error')
    }

    if (errors.email) {
      $('#email').parent().addClass('has-error')
    } else {
      $('#email').parent().removeClass('has-error')
    }

    if (errors.mobile) {
      $('#phone').parent().addClass('has-error')
    } else {
      $('#phone').parent().removeClass('has-error')
    }


    if (errors.password) {
      $('#ambPassword').parent().removeClass('has-error')
    } else {
      $('#ambPassword').parent().addClass('has-error')
    }

    if ($('#role').val() == 'Other') {
      let am = $('#amount')
      let am2 = $('#amount2')
      if (errors.amount || errors.amount2) {
        am.parent().addClass('has-error')
        am2.parent().addClass('has-error')
      } else {
        am.parent().removeClass('has-error')
        am2.parent().removeClass('has-error')
      }
    }


    if ($('#colleges').val() == '') {
      $('#colleges').parent().addClass('has-error')
    } else {
      $('#colleges').parent().removeClass('has-error')
    }

    if ($('#degree').val() == '') {
      $('#degree').parent().addClass('has-error')
    } else {
      $('#degree').parent().removeClass('has-error')
    }

    if ($('#branch').val() == '') {
      $('#branch').parent().addClass('has-error')
    } else {
      $('#branch').parent().removeClass('has-error')
    }

    if ($('#currentlyWorking').val() == '') {
      $('#currentlyWorking').parent().addClass('has-error')
    } else {
      $('#currentlyWorking').parent().removeClass('has-error')
    }


    if ($('#gender').val() == '') {
      $('#gender').parent().addClass('has-error')
    } else {
      $('#gender').parent().removeClass('has-error')
    }

    if ($('#venue').val() == '') {
      $('#venue').parent().addClass('has-error')
    } else {
      $('#venue').parent().removeClass('has-error')
    }


    if ($('#state').val() == '') {
      $('#state').parent().addClass('has-error')
    } else {
      $('#state').parent().removeClass('has-error')
    }


    if ($('#city').val() == '') {
      $('#city').parent().addClass('has-error')
    } else {
      $('#city').parent().removeClass('has-error')
    }


    if ($('#pincode').val() == '') {
      $('#pincode').parent().addClass('has-error')
    } else {
      $('#pincode').parent().removeClass('has-error')
    }


    if ($("#dob").val() == '') {
      $('#dob').parent().addClass('has-error')
    } else {
      $('#dob').parent().removeClass('has-error')
    }

  }
}

// Check participant or ambassador 
// exits or not for given email or mobile
function doesExist(val, url, fun) {
  $.ajax({
    type: "POST",
    url: url,
    data: val,
    headers: {
      Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (data) {
      fun(data)
    },
    error: function (err) {
      fun(undefined)
    }
  });
}


// ------------------------- MOVED TO BACKEND (sendMails, sendSms) -------------------------

/* function sendMails(email) {
    $.ajax({
        type: "POST",
        url: '${base_url}/email',
        headers: {
            Authorization: `Bearer ${Cookies.get('_t')}`
        },
        data: {
            "to": email,
            "from": "upskill@techzillaindia.com",
            "subject": 'Registration',
            "text": 'Welcome..'
        },
        success: function (data) {
            toastr.success('Email has been sent', { timeOut: 2500 })
        },
        error: function (err) {
            toastr.error('Failed', { timeOut: 2500 })
        }
    });
}

function sendSms(mobile) {

    let user = 'techzillaindia'
    let pass = '123456'
    let senId = 'TESTTO'
    let nbs = mobile
    let txt = 'Welcome.. !!!'
    let uri = `http://bhashsms.com/api/sendmsg.php?user=${user}&pass=${pass}&sender=${senId}&phone=${nbs}&text=${txt}&priority=ndnd&stype=normal`

    $.ajax({
        type: "GET",
        url: uri,
        success: function (data) {
            toastr.success('SMS has been sent', { timeOut: 2500 })
        },
        error: function (err) {
            toastr.error('Failed', { timeOut: 2500 })
            console.log(err)
        }
    });
} */


// Not in use -----------
// function send_message_add_participant(inputData, token, calbck) {

//   var msg_chan = new MessageChannel();

//   // Handler for recieving message reply from service worker
//   msg_chan.port1.onmessage = function (event) {
//     if (event.data.error) {
//       event.data.error
//     } else {
//       calbck(event.data)
//     }
//   };

//   // Send message to service worker along with port for reply
//   if (navigator.serviceWorker.controller != null) {

//     navigator.serviceWorker.controller.postMessage({
//       type: 'post',
//       url: `${base_url}/participantsWithTransaction`, options: {
//         data: inputData,
//         token
//       }
//     }, [msg_chan.port2]);
//   } else {
//     if (navigator.onLine) {
//       $.ajax({
//         type: "POST",
//         url: `${base_url}/participantsWithTransaction`,
//         data: inputData,
//         headers: {
//           Authorization: `Bearer ${token}`
//         },
//         success: function (data) {
//           // Transaction on if role 'Other' selected
//           temp = data
//           saveTrx()
//         },
//         error: function (err) {
//           toastr.options.timeOut = 0;
//           toastr.options.extendedTimeOut = 0;
//           toastr.options.onclick = function () { saveData() }
//           toastr.error('Failed', 'Click to retry')

//         }
//       });
//     } else {
//       toastr.error('Failed', 'Offline')
//     }
//   }
// }


function titleCase(str) {
  var splitStr = str.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(' ');
}


function getFirstName(str) {
  var name = str.split(" ");
  return name[0]
}


// Email template for offline pay
function getEmailTemplate(full_name) {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title></title>
    <style type="text/css">
      @import url(http://fonts.googleapis.com/css?family=Droid+Sans);
  
      /* Take care of image borders and formatting */
      p{
          font-size:18px !important
      }

      img {
        max-width: 600px;
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
      }
  
      a {
        text-decoration: none;
        border: 0;
        outline: none;
        color: black;
      }
  
      a img {
        border: none;
      }
  
      /* General styling */
  
      td,
      h1,
      h2,
      h3 {
        font-family: Helvetica, Arial, sans-serif;
        font-weight: 400;
      }
  
      td {
        text-align: center;
      }
  
      .buttomCls {
        background-color: #178f8f;
        border-radius: 4px;
        color: #ffffff;
        display: inline-block;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 16px;
        font-weight: bold;
        line-height: 40px;
        text-align: center;
        text-decoration: none;
        width: 150px;
        -webkit-text-size-adjust: none;
        margin-top: 15px;
      }
  
      body {
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none;
        width: 100%;
        height: 100%;
        color: #37302d;
        background: #ffffff;
        font-size: 16px;
      }
  
      table {
        border-collapse: collapse !important;
      }
  
      .headline {
        color: #ffffff;
        font-size: 28px;
      }
  
      .force-full-width {
        width: 100% !important;
      }
  
      .step-width {
        width: 90px;
        height: 91px;
      }
    </style>
  
    <style type="text/css" media="screen">
      @media screen {
  
        /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
        td,
        h1,
        h2,
        h3 {
          font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
        }
      }
    </style>
  
    <style type="text/css" media="only screen and (max-width: 480px)">
      /* Mobile styles */
      @media only screen and (max-width: 480px) {
  
        table[class="w320"] {
          width: 320px !important;
        }
  
        img[class="step-width"] {
          width: 80px !important;
          height: 81px !important;
        }
  
  
      }
    </style>
  </head>
  
  <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
    bgcolor="#ffffff">
    <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
      <tr>
        <td align="center" valign="top" bgcolor="#ffffff" width="100%">
          <center>
            <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
              <tr>
                <td align="center" valign="top">
  
                  <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
                    style="margin:0 auto;">
                    <tr>
                      <td style="font-size: 15px; text-align:center;">
                        <br>
                        <img src="https://techzillaindia.com/template_images/upskill.png" height="50"
                          width="160" alt="">
                        <br>
                        <br>
                      </td>
                    </tr>
                  </table>
  
                  <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="100%" bgcolor="#4dbfbf">
                    <tr>
                      <td class="headline">
                        <br>
                        Initial payment received
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <br>
                        <center>
                          <table style="margin:0 auto;width:40%" cellspacing="0" cellpadding="0" class="force-width-80">
                            <tr>
  
  
  
                              <td>
                                <!-- ======== STEP ONE ========= -->
                                <!-- ==== Please use this url: https://techzillaindia.com/template_images/1_dark.png in the src below in order to set the progress to one.
                      
                                                  Then replace step two with this url: https://techzillaindia.com/template_images/2_light.png
                                                  Then replace step three with this url: https://techzillaindia.com/template_images/3_light.gif ==== -->
  
                                <img class="step-width" src="https://techzillaindia.com/template_images/1_dark.png" alt="step one">
                              </td>
  
  
  
                              <!-- ======== STEP TWO ========= -->
                              <!-- ==== Please use this url: https://techzillaindia.com/template_images/2_dark.gif in the src below in order to set the progress to two.
                      
                                                  Then replace step three with this url: https://techzillaindia.com/template_images/3_light.gif
                      
                                                  Then replace step one with this url: https://techzillaindia.com/template_images/1_light.png ==== -->
                              <td>
                                <img class="step-width" src="https://techzillaindia.com/template_images/2_light.png" alt="step two">
                              </td>
  
                              <td>
                              <!-- ======== STEP THREE ========= -->
                              <!-- ==== Please use this url: https://techzillaindia.com/template_images/3_dark.gif in the src below in order to set the progress to three.
                      
                                                  Then replace step one with this url: https://techzillaindia.com/template_images/1_light.png
                      
                                                  Then replace step two with this url: https://techzillaindia.com/template_images/2_light.png ==== -->
                             
                                <img class="step-width" src="https://techzillaindia.com/template_images/3_light.gif" alt="step three">
                              </td>
                            </tr>
  
  
                            <tr>
                              <td style="vertical-align:top; color:#187272; font-weight:bold;">
                                Initiated
                              </td>
                              <td style="vertical-align:top; color:#187272; font-weight:bold;">
                                Confirmed
                              </td>
                              <td style="vertical-align:top; color:#187272; font-weight:bold;">
                                Done!
                              </td>
                            </tr>
                          </table>
                        </center>
                      </td>
                    </tr>
                    <tr>
                      <td>
  
                        <center>
                          <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="70%">
                            <tr>
                              <td style="color:#083d3d;text-align: left;padding:5px;">
                                <br>
                                <br>
                                <p>Hi <span style="color:black"><b>${getFirstName(full_name)}</b></span>,</p>
                                <p>Greetings from Techzilla India Infotech.</p>
                                <p>We have received a token amount for the National Upskill Entrance Test (NUET) which
                                  will
                                  be held on [date] at [venue].</p>
                                <p> For more details regarding the programme visit our <a href="https://www.techzillaindia.com/upskill"
                                    style="text-decoration: underline;color: black;">website</a>.</p>
                                <div style="height: 5px"></div>
                                <p style="font-size: 22px !important;text-align: center;">To complete your registration <br><a class="buttomCls" style="color: white;"
                                    href="http://bit.ly/NUETTZ">Visit
                                    here</a></p>
  
                                <!-- <br /> -->
  
                                <!-- <br> -->
                                <br>
                              </td>
                            </tr>
                          </table>
                        </center>
  
                      </td>
                    </tr>
  
                  </table>
  
                  <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141"
                    style="margin: 0 auto">
                    <tr>
                      <td style="color:#bbbbbb; font-size:15px">
  
                        <br>
                        <span style="text-align: center;">For any further assistance contact us at <b>022-33721615</b>.</span>
                      </td>
                    </tr>
  
                    <tr>
  
                      <td style="color:#bbbbbb; font-size:12px;">
                      <br>
                        <span>National UpSkill program by Techzilla India Infotech</span>
  
                      </td>
                    </tr>
                    <tr>
                      <td style="color:#bbbbbb; font-size:12px;">
                      <br>
                        © 2019 All Rights Reserved
                        <br>
                        <br>
                        <br>
                      </td>
                    </tr>
                  </table>
  
                </td>
              </tr>
            </table>
          </center>
        </td>
      </tr>
    </table>
  </body>
  
  </html>`
}