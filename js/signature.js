// close inspect on chrome
// $(document).keydown(function(e){
//   console.log(e.which);
//     if(e.which === 123) //prevent f12
//     {
//        return false;
//     }
//     if(e.which === 17||16||73) //prevent ctrl+shift+i
//     {
//        return false;
//     }
// });
// $(document).bind("contextmenu",function(e) //prevent right click
// {
//  e.preventDefault();
// });
// import login  from './loginT';
// $('#2nd_slot_2slotExam').removeClass('inactive_time_slot');
// $('#1st_slot_2slotExam').removeClass('inactive_time_slot');
const base_url = 'http://192.168.31.234:1337'
// const base_url = 'https://upskilladmin.techzillaindia.com'
let TZ_id;
let _id = null;
let name = "";
let paymentStatus = null;
let attendedStatus = false;
let getParticipantsUrl;
let TZ_id_doc;
let alert_doc;
let container_doc;
let success_doc;
let formDiv_doc;
let backButton_doc;
let displayIcons_doc;
let imageStrapiLink;
let valid_tz_container;
let aTag_doc = document.getElementById("aTag");
let saveButton_doc = document.getElementById("saveButton");
let formSubmitBttn = document.getElementById("formSubmitBttn");
let savingDisplay = document.getElementById("savingDisplay");
let showLoadingSavingIcon = document.getElementById("showLoadingSavingIcon");
let displayLoadingORSaving = document.getElementById("displayLoadingORSaving");
let authToken = Cookies.get('_t');

//let authToken="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Yzg1ZjNjOTQ1MTVmNzBlYmQzMGI2N2MiLCJpZCI6IjVjODVmM2M5NDUxNWY3MGViZDMwYjY3YyIsImlhdCI6MTU1MjI4ODgzNCwiZXhwIjoxNTU0ODgwODM0fQ.AQwgNgQV06uH7IRW2hWp8WtZZnCZHm6w5sTBoZB6h8c";

formDiv_doc = document.getElementById("formDiv");
TZ_id_doc = document.getElementById('TZ_id');
displayIcons_doc = document.getElementById('displayIcons');
alert_doc = document.getElementById("showError");
success_doc = document.getElementById("showSuccess");
container_doc = document.getElementById("container");
valid_tz_container = document.getElementById("valid_tz_container");
let loadingElement_doc = document.getElementsByClassName("fa-3x");
let loading = document.querySelector('.fa-3x');


function getEnteredTZ_id() {
  formSubmitBttn.disabled = true;
  displayLoadingORSaving.innerHTML = "Loading...";
  loading.style.display = "block";
  formSubmitBttn.value = loading.innerText;

  TZ_id = "TZ-" + TZ_id_doc.value;
  getParticipantsUrl = `${base_url}/participantsAll?uid=${TZ_id}`;

  formDiv_doc.style.display = "Block";
  alert_doc.style.display = "none";
  valid_tz_container.style.display = "none";
  // displayIcons_doc.style.display="none";
  // success_doc.style.display="none";
  // container_doc.style.display="none";
  // savingDisplay.style.display="none";
  getRequiredParticipant(getParticipantsUrl);
}

function login(decession) {
  //let url = 'http://localhost:1337/auth/local'
  const url = 'http://192.168.31.234:1337/auth/local'
  let data = {
    identifier: "takshilk",
    password: "takshilk"
  }
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    success: function (data) {
      Cookies.set('_t', data.jwt, { path: '/' });
      authToken = Cookies.get('_t');
      console.log(authToken);
      if (decession) {
        getRequiredParticipant(getParticipantsUrl);
      }
      else {
        getParticipantId(getParticipantsUrl);
      }

    },
    error: function (err) {
      console.log(err);
      alert_doc.style.display = "Block";
      alert_doc.innerHTML = "Server down or some other technical issue, please try again later";

    }
  });
}

function handleBackButtonClick() {
  svg.selectAll("path").remove();
  loading.style.display = "none";
  formSubmitBttn.value = "Submit";
  formSubmitBttn.disabled = false;
  formDiv_doc.style.display = "Block";
  alert_doc.style.display = "none";
  valid_tz_container.style.display = "none";
  // displayIcons_doc.style.display="none";
  // success_doc.style.display="none";
  // container_doc.style.display="none";
}

function getRequiredParticipant(uri1) {
  let uri = uri1;
  //console.log(uri);unfreezebutton
  $.ajax({
    type: "GET",
    url: uri,
    headers: {
      Authorization: `Bearer ${authToken}`
    },
    success: function (data) {
      //console.log(data.length);
      if (data.length == 0) {
        //alert('Entered TZ-id does not exist');
        alert_doc.style.display = "Block";
        loading.style.display = "none";
        formSubmitBttn.value = "Submit";
        formSubmitBttn.disabled = false;
        alert_doc.innerHTML = "Entered Id: \"" + TZ_id + "\" does not exist"
      }
      else {
        name = data[0].name;
        _id = data[0]._id;
        attendedStatus = data[0].attended;
        if (attendedStatus) {
          // alert("\""+name+"\", Id: \""+TZ_id+"\" has already been set to attended status!");
          // location.reload();
          alert_doc.style.display = "Block";
          loading.style.display = "none";
          formSubmitBttn.value = "Submit";
          formSubmitBttn.disabled = false;
          alert_doc.innerHTML = name + " (" + TZ_id + ") has already set to status attended";

        }
        else {
          //getTransactionStatus(`${base_url}/transactions?participants=${_id}`);//_limit=100000000
          formDiv_doc.style.display = "none";
          success_doc.style.display = "Block";
          valid_tz_container.style.display = "Block";
          //only used when error shown->back->submit
          success_doc.innerHTML = "Attendance for : " + name + " (" + TZ_id + ")";
        }
      }
    },
    error: function (err) {
      console.log(err)
      console.log(err.responseJSON);
      if (err.responseJSON.statusCode == 403 || err.responseJSON.statusCode == 401) {
        login(true);
      }
      else {
        alert_doc.style.display = "Block";
        loading.style.display = "none";
        formSubmitBttn.value = "Submit";
        formSubmitBttn.disabled = false;
        alert_doc.innerHTML = "Server down or some other technical issue, please try to submit again";

      }
    }
  });
}

function getTransactionStatus(uri1) { ///////not used !
  //participants.uid=${uid}
  $.ajax({
    type: "GET",
    url: uri1,
    headers: {
      Authorization: `Bearer ${authToken}`
    },
    success: function (data) {
      //console.log(data);
      paymentStatus = data[0].status;
      //console.log(paymentStatus);
      if (paymentStatus == "Completed") {
        formDiv_doc.style.display = "none";
        success_doc.style.display = "Block";
        valid_tz_container.style.display = "Block";
        //only used when error shown->back->submit
        success_doc.innerHTML = "Attendance for : " + name + " (" + TZ_id + ")";
        // success_doc.style.display="Block";
        // displayIcons_doc.style.display="Block";
        // container_doc.style.display="Block";
      }
      else {
        //alert("Person's payment is not completed !")
        //let alert_doc=document.getElementById("showError");
        alert_doc.style.display = "Block";
        loading.style.display = "none";
        formSubmitBttn.value = "Submit";
        formSubmitBttn.disabled = false;
        alert_doc.innerHTML = "\"" + name + "\" (" + TZ_id + ") has not completed thier payment!";
      }

    },
    error: function (err) {
      console.log(err)
      alert_doc.style.display = "Block";
      loading.style.display = "none";
      formSubmitBttn.value = "Submit";
      formSubmitBttn.disabled = false;
      alert_doc.innerHTML = "Could't get the payment status for " + name + "\" (" + TZ_id + ") refresh the page & try again please";

    }
  });
}
function storeImgStrapi(bodyFormData, uri) {

  $.ajax({
    type: "POST",
    processData: false,
    contentType: false,
    url: uri,
    data: bodyFormData,
    headers: {
      Authorization: `Bearer ${authToken}`
    },
    success: function (data) {
      console.log(data[0].url);
      imageStrapiLink = data[0].url;
      let relative_path = imageStrapiLink.indexOf("uploads");
      imageStrapiLink = imageStrapiLink.substring(relative_path, imageStrapiLink.length);//comment this for absolute path
      console.log('Congrats file uploaded to strapi ' + relative_path + "\n");//data[0].url
      upadteAttendace();
    },
    error: function (error) {
      loading.style.display = "none";
      savingDisplay.style.display = "none";
      success_doc.style.display = "none";
      alert_doc.style.display = "Block";
      aTag_doc.style.pointerEvents = "auto";
      saveButton_doc.style.color = "#00f";
      alert_doc.innerHTML = "Couldn't upload sigature to strapi for \"" + name + "\" (" + TZ_id + ") refresh & try again after some time";
      console.log('An error occurred:', JSON.stringify(error));
    }
  });
}
function upadteAttendace() {
  //console.log(updateData)
  //imageStrapiLink=imageStrapiLink+".png";
  $.ajax({
    //processData:false,->Dont do else it wont put data
    type: "PUT",
    url: `${base_url}/participants/${_id}`,
    data: { attended: true, signature: imageStrapiLink },
    headers: {
      Authorization: `Bearer ${authToken}`
    },
    success: function (data) {
      //console.log(data);
      //  success_doc.display.value="Block";
      //success_doc.innerHTML="Id: \""+TZ_id+"\", Name: \""+name+"\'s\" status is changed to attended";
      loading.style.display = "none";
      savingDisplay.style.display = "none";
      alert("Status changed to attended for \"" + name + "\" (" + TZ_id + ") ");
      location.reload();
    },
    error: function (err) {
      loading.style.display = "none";
      savingDisplay.style.display = "none";
      success_doc.style.display = "none";
      alert_doc.style.display = "Block";
      aTag_doc.style.pointerEvents = "auto";
      saveButton_doc.style.color = "#00f";
      alert_doc.innerHTML = "Couldn't update attendance status for \"" + name + "\" (" + TZ_id + ") refresh & try again after some time";
      console.log("Error: " + JSON.stringify(err));

    }
  });
}

//////////////////////////////////////// D3.js starts ///////////////////////////////////////////

var line = d3.line()
  .curve(d3.curveBasis);
//console.log(line);
let start = null;
var canvas = document.createElement("canvas");
var svg = d3.select("svg")
  .call(d3.drag()
    .container(function abc() {
      return this;
    })
    .subject(function () { var p = [d3.event.x, d3.event.y]; return [p, p]; })
    //.on("click",handleClicked)
    .on("start", dragstarted));

svg.append("rect")
  .attr('width', "100%")
  .attr('height', "100%")
  .attr('id', "rectID")
  .attr('fill', "#eee");
//console.log(svg)
function handleClick() {
  // saveButton_doc.style.color="#00f";
  start ? null : alert("Please sign before changing attendance status");
  //aTag_doc.removeAttr('href');
}
function dragstarted() {
  //console.log(d3.event.x,d3.event.y);
  start = true;
  var d = d3.event.subject,
    active = svg.append("path").datum(d),
    x0 = d3.event.x,
    y0 = d3.event.y;
  d.push([x0, y0]);
  active.attr("d", line);
  // d3.event.on("click", function() {
  //   var x1 = d3.event.x;
  //   var y1 = d3.event.y;
  //   d.push([x0 = x1, y0 = y1]);
  //   active.attr("d", line);
  // });

  d3.event.on("drag", function () {
    // console.log(x0,y0);
    // console.log(d3.event.x,d3.event.y);
    var x1 = d3.event.x,
      y1 = d3.event.y,
      dx = x1 - x0,
      dy = y1 - y0;

    if (dx * dx + dy * dy > 10) //when to fix
    {
      d.push([x0 = x1, y0 = y1]);
      //console.log("IF: "+d);
      //console.log(dx * dx + dy * dy);
    }
    else {
      d[d.length - 1] = [x1, y1];
      //console.log("else: "+d);
    }
    active.attr("d", line);
    //console.log(line)
  });

  d3.select('#clearPad').on('click', function () {
    svg.selectAll("path").remove();//to remove in svg everything
    //active.attr("d",null);//acts as single undo operation
    d = null;
  });

  d3.select('#undoPad').on('click', function () {
    let remove_child = document.getElementsByTagName("path").length + 1;
    let svgn = svg.node();
    //console.log(remove_child);
    //console.log(svg.select("style")._groups[0]==0)//important check-true means no style
    if (!(svg.select("style")._groups[0] == 0)) //Means if style is present
    {
      remove_child = remove_child + 1;
    }
    svg.select("path:nth-child(" + remove_child + ")").remove();

  });

  d3.select('#saveButton').on('click', function () {
    console.log("Clicked");
    let path_doc = document.getElementsByTagName("path")
    //console.log(path_doc.length)
    if (d == null || path_doc.length == 0) {
      //saveButton_doc.style.color="#00f";
      alert_doc.style.display = "none";
      alert('Please sign before changing attendance status');
      // aTag_doc.style.pointerEvents="auto";
      // aTag_doc.style.cursor="pointer";
    }
    else {
      success_doc.style.display = "Block";
      success_doc.innerHTML = "Attendance for : " + name + " (" + TZ_id + ")";
      saveButton_doc.style.color = "#0f0";
      alert_doc.style.display = "none";
      displayLoadingORSaving.innerHTML = "Saving...";
      loading.style.display = "block";
      savingDisplay.innerHTML = loading.innerHTML;
      savingDisplay.style.display = "inline";
      var svgString = getSVGString(svg.node());
      var element = document.getElementById('svgID');
      aTag_doc.style.pointerEvents = "none";
      // passes Blob and filesize String to the callback
      let width = element.clientWidth;
      let height = element.clientHeight;
      svgString2Image(svgString, width, height, 'jpg', save);
    }


    function save(dataBlob, filesize, imagesrc, file1) {
      //console.log(file1);
      let bodyFormData = new FormData();
      uri = `${base_url}/upload`;
      bodyFormData.append('files', file1);
      //console.log(bodyFormData);
      storeImgStrapi(bodyFormData, uri)
      //saveAs( dataBlob, 'Signature', './js'); // FileSaver.js function
    }
  });

}

// Set-up the export button
// Below are the functions that handle actual exporting:
// getSVGString ( svgNode ) and 
//svgString2Image( svgString, width, height, format, callback )
function getSVGString(svgNode) {

  appendCSS(document.querySelector('.path'), svgNode); //important to add css
  var serializer = new XMLSerializer();
  var svgString = serializer.serializeToString(svgNode);//to string
  return svgString;

  function appendCSS(cssText, element) {
    var refNode = element.hasChildNodes() ? element.children[0] : null;
    element.insertBefore(cssText, refNode);
  }
}

function svgString2Image(svgString, width, height, format, callback) {

  var imgsrc = 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svgString))); // Convert SVG string to data URL
  //console.log(imgsrc)

  //console.log(width)
  //define canvas container height width
  canvas.width = width;
  canvas.height = height;
  var context = canvas.getContext("2d");

  var image = new Image();
  //console.log(typeof(imgsrc))
  image.onload = function () {
    //d-->destination, same goes for s but are optional
    context.drawImage(image, 0, 0, width, height);//(imageToBeDrawen,dx,dy,dwidth,dHeigght)
    // console.log(context);  
    canvas.toBlob(function (blob) {

      //console.log(blob);
      //var filesize = Math.round( blob.length/1024 ) + ' KB';
      if (callback) {
        var file1 = new File([blob], _id + ".png", {
          type: 'image/png',
          lastModified: Date.now()
        });
        console.log(blob);
        console.log(file1);
        callback(blob, 220, imgsrc, file1);
      }
    });
  };

  image.src = imgsrc;
}


///////////////////////////////// reset attendance logic //////////////////////////////////

function resetAttendace() {
  $.ajax({
    //processData:false,->Dont do else it wont put data
    type: "PUT",
    url: `${base_url}/participants/${_id}`,
    data: { attended: false, pic: "" },
    headers: {
      Authorization: `Bearer ${authToken}`
    },
    success: function (data) {

      alert("Status changed to not attended for \"" + name + "\" (" + TZ_id + ") ");
      location.reload();
    },
    error: function (err) {
      console.log("Error: " + JSON.stringify(err));
      alert_doc.style.display = "Block";
      alert_doc.innerHTML = "Could't reset the attendance for for \"" + name + "\" (" + TZ_id + ") ,refresh & please try again later";
      formSubmitBttn.disabled = false;
      formSubmitBttn.value = "Submit";
    }
  });
}
function getParticipantId(uri1) {
  let uri = uri1;
  //console.log(uri);
  $.ajax({
    type: "GET",
    url: uri,
    headers: {
      Authorization: `Bearer ${authToken}`
    },
    success: function (data) {
      //console.log(data.length);
      if (data.length == 0) {
        alert_doc.style.display = "Block";
        alert_doc.innerHTML = "Entered Id: \"" + TZ_id + "\" does not exist";
        formSubmitBttn.disabled = false;
        formSubmitBttn.value = "Submit";
      }
      else {
        name = data[0].name;
        _id = data[0]._id;
        attendedStatus = data[0].attended;
        if (attendedStatus) {
          resetAttendace();
        }
        else {
          alert_doc.style.display = "Block";
          alert_doc.innerHTML = "Status for \"" + name + "\" (" + TZ_id + ") is already not attended";
          //alert();
          formSubmitBttn.disabled = false;
          formSubmitBttn.value = "Submit";
        }

      }
    },
    error: function (err) {
      console.log(err);
      if (err.responseJSON.statusCode == 403 || err.responseJSON.statusCode == 401) {
        login(false);
      }
      else {
        alert_doc.style.display = "Block";
        alert_doc.innerHTML = "server issue, refresh & try again after some time";
        formSubmitBttn.disabled = false;
        formSubmitBttn.value = "Submit";

      }

    }
  });
}
function handleResetSubmit() {
  formSubmitBttn.disabled = "true";
  formSubmitBttn.value = "Processing...";
  alert_doc.style.display = "none";
  TZ_id = "TZ-" + TZ_id_doc.value;
  getParticipantsUrl = `${base_url}/participantsAll?uid=${TZ_id}`;
  password = document.getElementById("TZ_password").value;
  if (password == "reset") {
    getParticipantId(getParticipantsUrl);
  }
  else {
    alert_doc.style.display = "Block";
    alert_doc.innerHTML = "Wrong password";
    // alert("Wrong password");
    formSubmitBttn.disabled = false;
    formSubmitBttn.value = "Submit";
  }
}
//Escape()->Encode a string, unescape->Decode,
//encodeURIComponent---Encodes url
//btoa-> encoding for base64 images
//atob->decoding for base64 