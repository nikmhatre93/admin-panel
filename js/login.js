$('#login_as').change(function () {
    if ($(this).val() == 'Admin') {
        $('#colleges').fadeIn()
    } else {
        $('#colleges').css('display', 'none')
    }
})

// $.ajax({
//     type: "GET",
//     url: 'http://http://upskilladmin.techzillaindia.com/colleges',

//     success: function (data) {
//         ops = ``
//         data.forEach(element => {
//             ops += `<option value=${element._id}>${element.name}</option>`
//         });
//         $('#colleges').append(ops)
//     },
//     error: function (err) {
//         console.log(err)
//     }
// });

$('#login').on('click', function () {

    // login - authentication
    // TODO:
    // let url = 'https://upskilladmin.techzillaindia.com/auth/local'
    // let url = 'http://localhost:1337/auth/local'
    let url = `${base_url}/auth/local`
    let data = {
        identifier: $('#email').val(),
        password: $('#password').val()
    }
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (data) {

            Cookies.set('_t', data.jwt, { path: '/' });
            Cookies.set('_email', data.user.email, { path: '/' });
            Cookies.set('_uName', data.user.username, { path: '/' });
            Cookies.set('_role', data.user.role.type, { path: '/' });
            Cookies.set('_id', data.user._id, { path: '/' });
            Cookies.set('_clg', data.user.colleges, { path: '/' });

            location.href = 'transaction.html'

        },
        error: function (err) {
            console.log(err)
        }
    });
})
