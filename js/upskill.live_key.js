
/* Constants */
var live_key = "rzp_live_9bAzh5u3F8Tfpa"

/* degree / branch list */
var bsc_msc = [
  "Chemistry",
  "Computer Games",
  "Computer Science",
  "Electronic",
  "Information Technology",
  "Mathematics",
  "Physics",
  "Other"
]

var be_me_btech_mtech = [
  "Automation and Robotics",
  "Automobile",
  "Chemical",
  "Computers",
  "Computer Science",
  "Electrical",
  "Electrical and Communications",
  "Electronics",
  "Electronics and Communication",
  "Electronics and Computer",
  "Electronics and Telecommunication",
  "Information Science",
  "Information Technology",
  "Information and Communication",
  "Instrumentation",
  "Mechanical",
  "Mechanical & Industrial",
  "Production & Industrial Engineering",
  "Software",
  "Other"
]

const base_url = `https://upskilladmin.techzillaindia.com`
// const base_url = `http://localhost:1337`
var _step = sessionStorage.getItem('_step')
const mailId = 'upskill@techzillaindia.com'
var CCode = ''
var couponApplied = false
var callCount = 0
var timer = $('#timer')
var allow = true
const amt = 1499
var currentUser = {}
var trx = {}
var discount_amount = 0
var referredUser = { owner: { name: '' } }
var setIntervalId;
var remainingTime;
var time
var isSameEmailId = false;



/* Prevent scroll for number field */
$('form').on('focus', 'input[type=number]', function (e) {
  $(this).on('mousewheel.disableScroll', function (e) {
    e.preventDefault()
  })
})

$('form').on('blur', 'input[type=number]', function (e) {
  $(this).off('mousewheel.disableScroll')
})

/* Handle refresh / redirect to last step before refresh */
function handleRefresh() {
  if (_step != undefined) {
    try {
      currentUser = JSON.parse(sessionStorage.getItem("currentUser"))
      $('#fullName').val(currentUser.name)
      $('#email-Id').val(currentUser.email)
      $('#phoneNumber').val(currentUser.mobile)
      $('#state').val(currentUser.state)
      $('#city').val(currentUser.city)
      $('#pincode').val(currentUser.pincode)
      $('#colleges').val(currentUser.colleges._id)
      $('#gender').val(currentUser.gender)
      $('#date-dob').val(currentUser.dob)
      $('#venue').val(currentUser.examvenue._id)
      $('#training_batch').val(currentUser.training_batch)
      if ($('#venue').val() != '') {
        $('#venue').change()
      }
    } catch (error) {

    }

    // sessionStorage.setItem("currentUser", JSON.stringify(currentUser));

    if (_step == 'otp_sent') {
      $('#detailsDiv').css('display', 'none') // 2nd div
      $('#payTypeDiv').css('display', 'none') // 3rd div
      $('#confirm').css('display', 'none') // 4th input
      $('#proceed_with_payment').css('display', 'none') // 5th input
      $('#verifyOtpForm').hide()
      afterOtpSend()
    } else if (_step == 'otp_verified') {

      $('#otpDiv').css('display', 'none') // 1st div
      $('#verifyOtpForm').show()
      $('#payTypeDiv').css('display', 'none') // 3rd div
      $('#confirm').css('display', 'none') // 4th input
      $('#proceed_with_payment').css('display', 'none') // 5th input
      afterOtpVerified()
    }
    else {
      $('#verifyOtpForm').hide()
      $('#otpDiv').css('display', 'none') // 1st div
      $('#detailsDiv').css('display', 'none') // 2nd div
      $('#confirm').css('display', 'none') // 4th input
      $('#proceed_with_payment').css('display', 'none') // 5th input
      afterCheckOut()
    }
  } else {
    $('#otp').css('display', 'none')
    $('#verifyOtp').css('display', 'none')
    $('#verifyOtpForm').hide()
    $('#detailsDiv').css('display', 'none')
    $('#payTypeDiv').css('display', 'none')
    $('#confirm').css('display', 'none')
    $('#proceed_with_payment').css('display', 'none')
  }
}

handleRefresh()

/* Remaining time before "get otp on call" */
remainingTime = (function () {
  time = 30

  return function () {
    time -= 1
    if (time == 0) {
      clearInterval(setIntervalId)
    }
    return time
  }
})()


/* Get OTP */
$('#getOpt').on('click', function (e) {

  if ($("#getOtpForm").valid()) {
    e.preventDefault()

    // On token success calls -
    //  getOtpAuthRewrittenFunction(), getColleges(tokenReceived);  venue(tokenReceived);
    getTokenFromDb();
  }

})

function getOtpAuthRewrittenFunction() {

  setIntervalId = setInterval(function () {
    timer.text(`( ${remainingTime()} )`)

  }, 1000)

  $('#phoneNumber').next().fadeIn()
  $('#verifyOtpForm').show()

  // TODO:
  /* Prod: comment 1st line and un-comment 2nd line */
  // afterOtpSend()// 1st
  // getOtp($('#phoneNumber').val(), $('#emailId').val()) // 2nd
  getOtp($('#phoneNumber').val()) // 2nd

}


/* Verify OTP */
$('#verifyOtp').on('click', function (e) {

  if ($("#verifyOtpForm").valid()) {
    e.preventDefault()

    // TODO:
    /* Prod: comment 1st line and un-comment 2nd line */
    // afterOtpVerified() // 1st    
    verifyOtp($('#phoneNumber').val(), $('#otp').val()) // 2nd

    getColleges(sessionStorage.getItem("_tkn"))
    venue(sessionStorage.getItem("_tkn"))
  }
})


/* Get OTP on call */
$('#resendOtp').on('click', function (e) {
  e.preventDefault()
  callCount += 1
  $('#info-msg').html(`you'll get a call on <span style="font-weight:600;">+91 ${$('#phoneNumber').val()}</span>`)
  resendOtp($('#phoneNumber').val())
  if (callCount == 2) {
    $(this).css("pointer-events", 'none');
    $(this).css("color", 'grey');
  }

})


var dobError = false

$('#date-dob').on('input', function (e) {
  if (dobError) {
    let values = $(this).val().split('/')
    checkDate(values)
  }
});

$('#date-dob').blur(function () {

  let values = $(this).val().split('/')
  checkDate(values)
});


var pincodeError = false

$('#pincode').on('input', function (e) {
  if (pincodeError) {
    checkPincode($(this).val())
  }
});

$('#pincode').blur(function () {
  checkPincode($(this).val())
});


function checkPincode(values) {
  if (values.length == 6) {
    pincodeError = false
    allow = true
    $('#pincodeError').fadeOut()
  } else {
    pincodeError = true
    allow = false
    $('#pincodeError').fadeIn()
  }
}


function checkDate(values, d) {
  if (values.length == 3 && values[0].length == 2 && values[1].length == 2 && values[2].length == 4) {
    dobError = false
    allow = true
    $('#dobError').fadeOut()
  } else {
    dobError = true
    allow = false
    $('#dobError').fadeIn()
  }
}

$('#email-Id').focus(function () {
  $('#email-Id').next().hide()
  allow = true
  isSameEmailId = false;
  let user = JSON.parse(sessionStorage.getItem('currentUser'))
  if (user == undefined || user == null) {
    $('#imp-text').fadeIn()
  }
})

$('#email-Id').keydown(function () {
  let user = JSON.parse(sessionStorage.getItem('currentUser'))
  if (user && user.email != $(this).val()) {
    $('#imp-text').fadeIn()
  }
})

/* Revert back to form */
$('#editUserDetails').on('click', function (e) {
  e.preventDefault()
  $('#checkout').fadeIn()
  $('#checkout').next().hide()
  afterOtpVerified()
})

var checkout = true
/* Procced to checkout */
$('#checkout').on('click', function (e) {

  if ($('#details-Form').valid()) {
    e.preventDefault()

    if (allow && checkout && !(isSameEmailId)) {

      $(this).hide()
      $(this).next().fadeIn()

      checkout = false

      sessionStorage.setItem('_step', 'proceed_to_checkout');
      currentUser = sessionStorage.getItem("currentUser");

      currentUser = {
        name: $('#fullName').val(),
        email: ($('#email-Id').val()).toLowerCase(),
        mobile: $('#phoneNumber').val(),
        state: $('#state').val(),
        city: $('#city').val(),
        pincode: $('#pincode').val(),
        colleges: $('#colleges').val(),
        gender: $('#gender').val(),
        dob: $('#date-dob').val(),
        degree: $('#degree').val(),
        branch: $('#Courses').val(),
        working: $('#currentlyWorking').val(),
        examvenue: $('#venue').val(),
        training_batch: $('#training_batch').val(),
        exam: $('#venue').find(":selected").data("exam"),
      };


      sessionStorage.setItem("currentUser", JSON.stringify(currentUser));
      let u_id = sessionStorage.getItem("currentUserId")
      if (u_id != null || u_id != undefined) {
        /* Update Participant if exist */
        updateParticipant(currentUser, u_id)
      } else {
        /* Add Participant */
        createParticipant(currentUser)
      }

    } else {
      alert('email already exists')
      $('#email-Id').focus()
    }

  }
})


/* make online payment */
$('#online').on('click', function (e) {
  e.preventDefault()
  $(this).removeClass('customTab')
  $('#confirm').css('display', 'none')

  let payment = 0
  let trx = sessionStorage.getItem('trx')

  if (trx == null) {
    payment = amt
  } else {
    trx = JSON.parse(trx)
    if (trx.status != 'Confirmed') {
      payment = amt - parseInt(trx.Amount.initial)
    } else {
      payment = amt
    }
  }


  // var dis = 100
  // if (CCode == disCoupn) {
  //   dis = disAmt
  // }

  payment = couponApplied ? payment - discount_amount : payment
  let data = JSON.parse(sessionStorage.getItem("currentUser"))
  var options = {
    "key": live_key,
    // "key": 'rzp_test_51mYy9J1rLqtLk',
    "amount": payment * 100,
    "name": "Up Skill",
    "prefill": {
      "contact": data.mobile,
      "email": data.email
    },
    "notes": {
      code: CCode
    },
    "handler": function (response) {

      let paid_amt = payment * 100
      let data = JSON.parse(sessionStorage.getItem("currentUser"))
      let msg = generatePaymentMsg(data, response.razorpay_payment_id, paid_amt)

      let sub = `payment ${data.name} | ${data.uid} | ${data.tempClg.name}`
      if (couponApplied) {
        sub += ' (Referred)'
      }
      let tkn = sessionStorage.getItem('_tkn')

      // TODO: Uncomment to activate mails
      sendMails(mailId, tkn, msg, sub)

      /* Create Transaction if payment is successful */
      trasnsaction('online')

    }
  };



  let rzp1 = new Razorpay(options);
  rzp1.open();
  e.preventDefault();

})

/* not in use */
$('#confirm').on('click', function (e) {
  e.preventDefault()
  trasnsaction('cash')
})

/* get access token */
// get auth token from db new method
function getTokenFromDb() {
  $.ajax({
    type: "GET",
    url: `${base_url}/tokens`,
    success: function (data) {
      // console.log(data)

      if (data.length == 0) {

        initializeTokenData();
      }
      else {
        let AuthToken = data[0].AuthToken;
        sessionStorage.setItem('token_id', data[0]._id);

        if (AuthToken) { // if token present check if valid or not

          sessionStorage.setItem('_tkn', AuthToken);
          sessionStorage.setItem('_usr', data[0].userDetails);

          let decodedToken = parseJwt(AuthToken);
          var current_time = new Date().getTime() / 1000;

          if (current_time > decodedToken.exp) { // token expired get a new one
            getToken();
          }
          else { // token valid
            getOtpAuthRewrittenFunction();
            getColleges(AuthToken)
            venue(AuthToken)
          }

        }
        else {  // token not present in db so request

          getToken();

        }
      }
    }
  });
}

function initializeTokenData() {
  let tokenInitialData = {
    "AuthToken": "",
    "userDetails": {}
  }
  $.ajax({
    type: "POST",
    url: `${base_url}/tokens`,
    data: tokenInitialData,
    success: function (data) {
      sessionStorage.setItem('token_id', data._id);
      getToken();
    }
  });
}

/* get access token */
function getToken() {
  $.ajax({
    type: "POST",
    url: `${base_url}/auth/local`,
    data: { identifier: "public", password: "tech@123" },

    success: function (data) {
      let userDetails = JSON.stringify({ name: data.user.name, id: data.user._id });
      sessionStorage.setItem('_usr', JSON.stringify({ name: data.user.name, id: data.user._id }))
      sessionStorage.setItem('_tkn', data.jwt);
      saveTokenToDb(data.jwt, userDetails);
      // getColleges(data.jwt)
      // venue(data.jwt)
    },
    error: function (err) { }
  })
}

function saveTokenToDb(tokenReceived, userDetails) {

  let tkn = sessionStorage.getItem('_tkn') // might use this
  let tokenId = sessionStorage.getItem('token_id')
  let inputData = {
    "AuthToken": tokenReceived,
    "userDetails": userDetails
  }

  $.ajax({
    type: "PUT",
    url: `${base_url}/tokens/${tokenId}`,
    data: inputData,
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
      // console.log(data);
      getOtpAuthRewrittenFunction();
      getColleges(tokenReceived);
      venue(tokenReceived);
    }
  });

}

function parseJwt(token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));

  return JSON.parse(jsonPayload);
};


function createParticipant(inputData) {

  let tkn = sessionStorage.getItem('_tkn')
  let _usr = JSON.parse(sessionStorage.getItem('_usr'))
  inputData.owner = _usr.id
  $.ajax({
    type: "POST",
    url: `${base_url}/participants`,
    data: inputData,
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
      checkout = true
      // TODO: uncomment to activate mails

      let msg = generateRegMsg(data)
      let sub = `New Registration ${data.name} | ${data.uid} | ${data.colleges.name}`

      sendMails(mailId, tkn, msg, sub)

      let tempData = data
      tempData.tempClg = tempData.colleges
      tempData.colleges = data.colleges._id
      $('#checkout').fadeIn()
      $('#checkout').next().hide()
      sessionStorage.setItem("currentUser", JSON.stringify(tempData));
      afterCheckOut()
    },
    error: function (err) {
      $('#checkout').fadeIn()
      $('#checkout').next().hide()
      checkout = true
      console.log(err)
      alert('Something went wrong, Please try again later.')
    }
  })
}


function updateParticipant(inputData, u_id) {

  let tkn = sessionStorage.getItem('_tkn')
  $.ajax({
    type: "PUT",
    url: `${base_url}/participants/${u_id}`,
    data: inputData,
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
      // let msg = generateRegMsg(data)
      // let sub = `Confirmed Registration ${data.name} | ${data.uid} | ${data.colleges.name}`
      checkout = true
      let tempData = data
      tempData.tempClg = tempData.colleges
      tempData.colleges = data.colleges._id
      $('#checkout').fadeIn()
      $('#checkout').next().hide()
      sessionStorage.setItem("currentUser", JSON.stringify(tempData));
      afterCheckOut()
    },
    error: function (err) {
      $('#checkout').fadeIn()
      $('#checkout').next().hide()
      checkout = true
      alert('Something went wrong, Please try again later.')
    }
  })
}


function trasnsaction(paymentType) {

  let user_for_referral = JSON.parse(sessionStorage.getItem("currentUser"));

  // TODO:
  let referredTemplate = friendReferredTemplate(referredUser, user_for_referral)

  let tkn = sessionStorage.getItem('_tkn')
  let _usr = JSON.parse(sessionStorage.getItem('_usr'))
  let oldTrx = sessionStorage.getItem('trx')
  var inputData = {
    par: {},
    trx: {
      paid_via: { initial: '', rem: '' },
      Amount: { initial: '', rem: 0 },
      collected_by: { initial: '', rem: '' },
      status: 'Confirmed',
      participants: '',
      applied_code: CCode
    },
    email: {
      to: '',
      from: mailId,
      subject: '',
      text: ''
    },
    sms: ''
  }

  // email msg
  let full_name = ''
  let msg1 = ''
  let msg2 = `We have successfully received your payment for NUET. All the important updates will be sent to your number and email. Download Syllabus @ http://bit.ly/nuetsyb. Cheers and all the best!`

  if (oldTrx != null) {
    /* --------------------------  if transaction in db -------------------------- */
    inputData.par = JSON.parse(sessionStorage.getItem("currentUser"));
    inputData.trx = JSON.parse(oldTrx)
    inputData.trx.collected_by.rem = _usr.name
    inputData.trx.paid_via.rem = paymentType
    inputData.trx.status = paymentType == 'cash' ? 'Confirmed' : 'Completed'
    // var am = 100

    if (couponApplied) {
      // if (CCode == disCoupn) {
      //   am = disAmt
      // }
      inputData.trx.Amount.rem = amt - parseInt(inputData.trx.Amount.initial) - discount_amount
    } else {
      inputData.trx.Amount.rem = amt - parseInt(inputData.trx.Amount.initial)
    }

    let u_id = sessionStorage.getItem("currentUserId")

    full_name = titleCase(inputData.par.name)
    msg1 = `Hey ${getFirstName(full_name)},\nYour seat for the NUET is confirmed with seat no ${inputData.par.uid}. For more details on National Upskill programme visit http://bit.ly/TZUPSKILL`

    $.ajax({
      type: "PUT",
      url: `${base_url}/transactions/${inputData.trx._id}`,
      data: inputData.trx,
      headers: {
        Authorization: `Bearer ${tkn}`
      },
      success: function (data) {
        // 
        if (data.participants) {
          msg1 = `Hey ${getFirstName(full_name)},\nYour seat for the NUET is confirmed with seat no ${data.participants.uid}. For more details on National Upskill programme visit http://bit.ly/TZUPSKILL`
        }

        // inputData = data
        let emailText = ``
        let sub = ``
        let _data = { ...data.participants }
        delete _data['examvenue']
        delete _data['exam']
        if (paymentType == 'online') {
          let n = titleCase(getFirstName(data.participants.name))
          createCouponCode(data.participants._id, n, data.participants.email, inputData.par.mobile)

          if (couponApplied) {
            // TODO: Uncomment to activate mails and sms
            sendSms(referredUser.owner.mobile, tkn,
              `Congratulations! your friend ${n} has enrolled with us. Please check mail for more details.`)
            sendMails(referredUser.owner.email, tkn, referredTemplate, 'Congratulations! You have a new referral.')
          }

          // TODO: Uncomment to activate sms
          sendSms(inputData.par.mobile, tkn, msg1)
          sendSms(inputData.par.mobile, tkn, msg2)

          emailText = enrolledSuccessfulTemplate2(full_name, { ...inputData, ..._data })
          sub = 'Successfully Enrolled'
        } else {
          emailText = seatConfirmedTemplate(full_name, { ...inputData, ..._data })
          sub = `Seat Confirmed`
        }

        // TODO: Uncomment to activate mails
        sendMails(inputData.par.email, tkn, emailText, sub)

        afterTransaction()

      },
      error: function (err) {

        console.log('err ==>', err)
      }
    });

  } else {
    /* --------------------------  if transaction is not in db --------------------------  */
    let _currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    inputData.trx.paid_via.initial = paymentType
    inputData.trx.participants = _currentUser._id
    inputData.par = _currentUser
    inputData.par.owner = _usr.id

    inputData.trx.Amount.rem = amt
    inputData.trx.Amount.initial = 0

    if (paymentType != 'cash') {
      inputData.trx.Amount.initial = amt
      inputData.trx.Amount.rem = 0
    }

    // var amm = 100
    if (couponApplied) {
      // if (CCode == disCoupn) {
      //   amm = disAmt
      // }
      inputData.trx.Amount.initial = amt - discount_amount
    }


    inputData.trx.collected_by.initial = _usr.name
    inputData.email.to = _currentUser.email
    inputData.email.subject = `Seat Confirmed`
    full_name = titleCase(inputData.par.name)

    inputData.sms = `Hey ${getFirstName(full_name)},\nYour seat for the NUET is confirmed with seat no ${_currentUser.uid}. For more details on National Upskill programme visit http://bit.ly/TZUPSKILL`

    inputData.trx.status = paymentType == 'cash' ? 'Confirmed' : 'Completed'


    $.ajax({
      type: "POST",
      url: `${base_url}/transactions`,
      data: inputData.trx,
      headers: {
        Authorization: `Bearer ${tkn}`
      },
      success: function (data) {

        if (data.found == undefined) {

          // inputData = data
          if (data.participants) {
            inputData.sms = `Hey ${getFirstName(full_name)},\nYour seat for the NUET is confirmed with seat no ${data.participants.uid}. For more details on National Upskill programme visit http://bit.ly/TZUPSKILL`
          }

          // TODO: Uncomment to activate sms
          sendSms(inputData.par.mobile, tkn, inputData.sms)
          sendSms(inputData.par.mobile, tkn, msg2)

          let n = titleCase(getFirstName(data.participants.name))

          createCouponCode(data.participants._id, n, data.participants.email, inputData.par.mobile)

          if (couponApplied) {
            // TODO: Uncomment to activate mails and sms
            sendSms(referredUser.owner.mobile, tkn,
              `Congratulations! your friend ${n} has enrolled with us. Please check mail for more details.`)
            sendMails(referredUser.owner.email, tkn, referredTemplate, 'Congratulations! You have a new referral.')
          }

          if (paymentType != 'cash') {
            let _data = { ...data.participants }
            delete _data['examvenue']
            delete _data['exam']
            inputData.email.text = enrolledSuccessfulTemplate(full_name, { ..._currentUser, ..._data })

            inputData.email.subject = 'Successfully Enrolled'

            // TODO: Uncomment to activate mails
            sendMails(inputData.par.email, tkn, inputData.email.text, inputData.email.subject)

          } else {

            inputData.email.text = registrationSuccessfulTemplate(full_name, amt)

            inputData.email.subject = 'Registration Successful!'

            // TODO: Uncomment to activate mails
            sendMails(inputData.par.email, tkn, inputData.email.text, inputData.email.subject)

          }

        }

        afterTransaction()


      },
      error: function (err) {

        console.log('err ==>', err)
      }
    });
  }

}


function afterTransaction() {
  $('#otpDiv').hide()
  $('#detailsDiv').hide()
  $('#payTypeDiv').hide()
  $('#info-msg').text('')
  $('#header-msg').html(`Your registration is successful.  <br /> Please check email and sms for more! <br /><br /> 
  <a href="https://techzillaindia.com/upskill/hallticket/" style="text-decoration: underline;color: #26715d;padding-bottom: 20px;">
  Click here to generate your Admit Card</a>
  <br/>
  <p>
  You can download the syllabus <a style='color:#53f6c6;' target="_blank" href='http://bit.ly/nuetsyb'>here</a></p>`)
  var currentUserForHT = JSON.parse(sessionStorage.getItem("currentUser"))
  sessionStorage.clear()
  sessionStorage.setItem('currentUserForHT', JSON.stringify(currentUserForHT))
}

function getTransaction(userId) {

  let tkn = sessionStorage.getItem('_tkn')
  $.ajax({
    type: "GET",
    url: `${base_url}/transactionsForUser/${userId}`,
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
      let d = data

      if (d.status == "Completed") {
        $('#header-msg').text("You're already there!")
        $('#info-msg').text('')
        $('#showData').append(`<p>Name: ${d.participants.name} <br /> Transaction status: ${d.status}</p>`)
        $('#showData').fadeIn()
        sessionStorage.clear();
      } else {
        delete d['participants']

        trx = JSON.stringify(d)
        sessionStorage.setItem("trx", trx);

        $('#detailsDiv').fadeIn()

      }
    },
    error: function (err) { $('#detailsDiv').fadeIn() }
  });
}


function getColleges(tkn) {

  $.ajax({
    type: "GET",
    url: `${base_url}/colleges`,
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
      $('._op').remove()
      ops = ``
      data.forEach(element => {
        ops += `<option class="_op" value=${element._id}>${element.name}</option>`
      });

      $('#colleges').append(ops)
      // console.log('getColleges')
      handleRefresh()
    },
    error: function (err) { }
  });
}

function venue(tkn) {
  $.ajax({
    type: "GET",
    url: `${base_url}/examvenues`,
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data2) {
      $('._op2').remove()
      ops2 = ``
      data2.forEach(element => {
        // console.log(data2)
        if (element._id != "5cf66b4c0634cb6cacd3a523") {
          ops2 += `<option class="_op2" 
          value=${element._id} data-exam="${element.exam._id}">
          ${element.name}</option>`
        }
        else {
          ops2 += `<option class="_op2" 
          value=${element._id} data-exam="${element.exam._id}" disabled>
          ${element.name}- Registrations Closed</option>`
        }
      });
      $('#venue').append(ops2)
      // console.log('venue')
    },
    error: function (err) { }
  });
}

function getDetailsIfExists(type, callback) {

  let tkn = sessionStorage.getItem("_tkn")
  $.ajax({
    type: "POST",
    url: `${base_url}/participantsFind`,
    data: { "mobile": $('#phoneNumber').val() },
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
      callback(data)
    },
    error: function (err) {
      if (type == 'afteropt') {
        $('#detailsDiv').fadeIn()
      } else {
        callback(undefined)
      }
    }
  });
}

function getDetailsIfExistsCallback(data) {
  // debugger
  currentUser = {
    name: data.name,
    email: data.email,
    mobile: data.mobile,
    state: data.state,
    city: data.city,
    pincode: data.pincode,
    colleges: data.colleges,
    gender: data.gender,
    dob: data.dob,
    examvenue: data.examvenue,
    training_batch: data.training_batch
  };
  // console.log(data)
  sessionStorage.setItem("currentUserId", data._id)
  sessionStorage.setItem("currentUser", JSON.stringify(currentUser));
  getTransaction(data._id)

  $('#fullName').val(data.name)
  $('#email-Id').val(data.email)
  $('#state').val(data.state)
  $('#city').val(data.city)
  $('#pincode').val(data.pincode)
  $('#colleges').val(data.colleges._id)
  let gen = data.gender == 'Select' ? '' : data.gender
  $('#gender').val(gen)
  $('#date-dob').val(data.dob)
  $('#venue').val(data.examvenue._id)
  if (data.training_batch) {
    $('#training_batch').val(data.training_batch)
  }

  $('#degree').val(data.degree)
  $('#currentlyWorking').val(data.working)
  if ($('#venue').val() != '') {
    $('#venue').change()
  }
}

$('#email-Id').blur(function () {
  let user = JSON.parse(sessionStorage.getItem('currentUser'))
  $('#imp-text').hide()
  if (user == null || user == undefined) {
    findUserByEmail($(this).val())
  } else {
    if (user.email != $(this).val()) {
      findUserByEmail($(this).val())
    }
  }
})


function getOtp(number) {

  // $('#email-Id').val(email);

  getDetailsIfExists('beforeotp', function (user_data) {

    let email = user_data == undefined ? undefined : user_data.email

    let tkn = sessionStorage.getItem('_tkn')

    let url = `${base_url}/participantsOtp`
    $.ajax({
      type: "POST",
      url: url,
      data: {
        number,
        email
      },
      headers: {
        Authorization: `Bearer ${tkn}`
      },
      "async": true,
      "crossDomain": true,
      success: function (data) {
        sessionStorage.setItem('_step', 'otp_sent');
        currentUser['mobile'] = number
        sessionStorage.setItem("currentUser", JSON.stringify(currentUser));
        afterOtpSend()
      },
      error: function (err) {

        console.log(err)
      }
    });
  })

}

function verifyOtp(number, otp) {
  let tkn = sessionStorage.getItem('_tkn')

  let url = `${base_url}/participantsVerifyOtp`

  $.ajax({
    type: "POST",
    url: url,
    async: true,
    crossDomain: true,
    data: { number, otp },
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
      let d = data
      if (d.type != 'error') {
        sessionStorage.setItem('_step', 'otp_verified');
        afterOtpVerified()
      }
      else {
        $("#otp").next().fadeIn()
      }
    },
    error: function (err) {
      console.log(err)
    }
  });
}


function resendOtp(number) {
  let tkn = sessionStorage.getItem('_tkn')

  let url = `${base_url}/participantsGetCall`

  $.ajax({
    type: "POST",
    url: url,
    async: true,
    crossDomain: true,
    data: { number },
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
      // console.log(data)
    },
    error: function (err) {
      // console.log(err)
    }
  });
}

function afterOtpSend() {

  let resendOtp = $('#resendOtp')
  $('#otp').fadeIn()
  $('#verifyOtpForm').show()
  $('#getOtpForm').css('display', 'none')
  $('#verifyOtp').fadeIn()
  $('#detailsDiv').hide()
  $('#payTypeDiv').hide()
  setTimeout(function () {
    timer.text('')
    resendOtp.css("pointer-events", 'auto');
    resendOtp.css("color", '#1d7d5d');
  }, 30000)
  resendOtp.css("pointer-events", 'none');
  resendOtp.fadeIn()
  $('#info-msg').css('font-weight', 'unset')
  $('#info-msg').html(`OTP has been sent to <span style="font-weight:600;">+91 ${$('#phoneNumber').val()}</span>`)
  // $('#info-msg').html(`OTP has been sent to <span style="font-weight:600;">+91 ${$('#phoneNumber').val()} & ${$('#emailId').val()}</span>`)
  $('#getOpt').css('display', 'none')
}

function afterOtpVerified() {

  $('#verifyOtpForm').hide()
  $('#otpDiv').hide()
  $('#payTypeDiv').hide()
  $('#Enroll-NUET').hide()
  $('#custom-html-1').removeClass('vc_col-lg-5')
  $('#custom-html-1').addClass('vc_col-lg-9')
  $('#header-msg').text("You're almost there!")
  $('#info-msg').text('Please fill in the below information.')
  $('#otpDiv').css('display', 'none')
  getDetailsIfExists('afteropt', getDetailsIfExistsCallback)
}


function afterCheckOut() {

  try {
    $('html, body').animate({
      scrollTop: $("#headerDiv").offset().top
    }, 600);
  } catch (error) { }

  $('#verifyOtpForm').hide()
  $('#otpDiv').hide()
  $('#detailsDiv').hide()
  $('#Enroll-NUET').hide()
  $('#detailsDiv').hide()
  $('#custom-html-1').removeClass('vc_col-lg-5')
  $('#custom-html-1').addClass('vc_col-lg-9')
  $('#header-msg').text("Payment Details")
  $('#info-msg').text('Review and complete your payment.')
  d = JSON.parse(sessionStorage.getItem('trx'))
  $('#detailsDiv').css('display', 'none')

  let user = JSON.parse(sessionStorage.getItem('currentUser'))
  if (couponApplied) {
    setPaymentDetails(`<span style ="font-size:17px" >Coupon Applied: <span style="font-family:sans-serif"> &#8377;</span>
    -<span style="color:#8B0000">${discount_amount}</span>/- </span> <br/> `, discount_amount)
  } else {
    setPaymentDetails()
  }

  // console.log(user)
  let _exam = user.exam ? user.exam.name.split('-') : ['', '']

  $('#userDetails').html(`
    <span>Name: ${user.name}</span> <br/>
    <span>Email: ${user.email}</span> <br/>
    <span>Mobile: ${user.mobile}</span> <br/>
    <span><b>Date: ${_exam[0]}</span></b> <br/>
    <span><b>Venue: ${_exam[1]}</span></b>`)

  $('#payTypeDiv').fadeIn()
}

function findUserByEmail(email) {
  let user = JSON.parse(sessionStorage.getItem('currentUser'))

  let url = `${base_url}/participantsFind`
  $.ajax({
    type: "POST",
    url: url,
    headers: {
      Authorization: `Bearer ${sessionStorage.getItem("_tkn")}`
    },
    data: { email },
    success: function (data) {
      $('#email-Id').next().fadeIn()
      allow = false
      isSameEmailId = true;
    },
    error: function (err) {
      allow = true
      isSameEmailId = false;
      $('#email-Id').next().css('display', 'none')
    }
  });

}


function sendSms(mobile, tkn, msg) {

  // sms msg
  $.ajax({
    type: "POST",
    url: `${base_url}/sendSms`,
    data: { mobile: '91' + mobile, msg },
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
    },
    error: function (err) {

    }
  });
}

function sendMails(to, tkn, msg, sub) {

  // TODO:
  let mailMessage = msg
  $.ajax({
    type: "POST",
    url: `${base_url}/email`,
    data: {
      to: to,
      from: mailId,
      subject: sub,
      html: mailMessage
    },
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
    },
    error: function (err) {

    }
  });

}


function titleCase(str) {
  var splitStr = str.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(' ');
}

function getFirstName(str) {
  var name = str.split(" ");
  return name[0]
}


$('#couponCodeApply').click(function () {
  let tkn = sessionStorage.getItem('_tkn')

  $.ajax({
    type: "POST",
    url: `${base_url}/findByCouponcodes`,
    data: {
      unique_code: $('#couponCode').val(),
    },
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {

      referredUser = data[0]
      $('#couponError').remove()
      if (data.length > 0) {
        $('#couponError').remove()
        CCode = $('#couponCode').val()
        $('.amountSpan').remove()
        couponApplied = true

        discount_amount = data[0].amout

        // if (CCode == disCoupn) {
        //   v = disAmt
        // }

        setPaymentDetails(`<span style="font-size:17px">Coupon Applied: <span style="font-family:sans-serif"> &#8377;</span>
    -<span style="color:#8B0000">${discount_amount}</span>/- </span> <br/> `, discount_amount)
        $('#couponDiv').hide()
      } else {
        // if ($('#couponCode').val() == disCoupn) {
        //   $('#couponDiv').append(`< p id = "couponError" > Coupon Limit Exhausted for ${ $('#couponCode').val() }.</p > `)
        // } else {
        $('#couponDiv').append(`<p id="couponError">${$('#couponCode').val()} coupon not found.</p>`)
        // }

      }
    },
    error: function (err) {
      $('#couponError').remove()

      // if ($('#couponCode').val() == disCoupn) {
      //   $('#couponDiv').append(`<p id="couponError">Coupon Limit Exhausted for ${ $('#couponCode').val() }.</p> `)
      // } else {
      $('#couponDiv').append(`<p id="couponError">${$('#couponCode').val()} coupon not found.</p>`)
      // }

    }
  });
})


function setPaymentDetails(txt, discount_amount) {
  d = JSON.parse(sessionStorage.getItem('trx'))
  let user = JSON.parse(sessionStorage.getItem('currentUser'))
  let amt2 = txt ? amt - discount_amount : amt
  if (d) {
    $('#payable-amount').html(
      `<span class="amountSpan" style="font-size:17px">Total: <span style="font-family:sans-serif">&#8377;</span> ${amt}/- </span><br/> 
                  <span class="amountSpan" style="font-size:17px"> 
                      Paid: <span style="font-family:sans-serif"> &#8377;</span>
                      -<span style="color:#8B0000"> ${d.Amount.initial}</span>/- </span> <br/> 
                  ${txt ? txt : ''}
                  <span class="amountSpan" style="font-size:17px"> Payable Amount: 
                      <span style="font-family:sans-serif">&#8377;</span> <span style="color:#006621">
                      ${amt2 - d.Amount.initial}</span>/- </span>`
    )

    if (d.status == 'Confirmed') {
      $('#cash').css('display', 'none')
    } else {
      if (user.colleges == '5c45d75e65e5b23c42853484') {
        $('#cash').hide()
      } else {
        $('#cash').fadeIn()
      }
    }
  } else {
    $('#payable-amount').html(
      `<span class="amountSpan" style = "font-size:17px">Total:
  <span style="font-family:sans-serif">&#8377;</span> ${amt} </span>
    <br/>
  ${txt ? txt : ''}
  <span class="amountSpan" style="font-size:17px">
    Payable Amount: <span style="font-family:sans-serif">&#8377;</span><span style="color:#006621">
      ${amt2}</span> </span>`
    )
    if (user.colleges == '5c45d75e65e5b23c42853484') {
      $('#cash').hide()
    } else {
      $('#cash').fadeIn()
    }
  }
}


function makeid() {
  var text = "TZ";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


function createCouponCode(owner, name, email, mobile) {

  let tkn = sessionStorage.getItem('_tkn')
  $.ajax({
    type: "POST",
    url: `${base_url}/couponcodes`,
    data: {
      unique_code: makeid(),
      owner
    },
    headers: {
      Authorization: `Bearer ${tkn}`
    },
    success: function (data) {
      referredTemplate = referralCodeOfOwnerTemplate(data, name)

      // TODO: Uncomment to activate mails and sms
      sendMails(email, tkn, referredTemplate, 'Your referral code details.')
      sendSms(mobile, tkn,
        `Refer your friends using code: ${data.unique_code} and get Rs.100 cash reward on each successful enrollment. Please check mail for more details.`)
    },
    error: function (err) {
    }
  });
}

$('#venue').change(function () {
  let val = $(this).val()
  if (val == '') {
    $('#_hiddenForm').fadeOut()
  } else {
    $('#_hiddenForm').fadeIn()
  }
})

$('#degree').change(function () {
  let val = $(this).val()
  if (val == 'BCA' || val == 'MCA') {
    $('#Courses').removeAttr('required');
    $('#Courses').hide()
    $('#Courses').prev().hide()
  } else {
    $('#Courses').attr("required", true)
    generateBranchList(val)
    $('#Courses').show()
    $('#Courses').prev().show()
  }
})


function generateBranchList(val) {
  $('.branchClass').remove()
  let op = ``
  if (val == 'BE' || val == 'ME' || val == 'BTech' || val == 'MTech') {

    be_me_btech_mtech.forEach(function (item) {
      op += `<option class="branchClass" value=${item}>${item}</option>`
    })
  } else if (val == 'BSc' || val == 'MSc') {

    bsc_msc.forEach(function (item) {
      op += `<option class="branchClass" value=${item}>${item}</option>`
    })
  }
  $('#Courses').append(op)
}


$('#referredYes').click(function () {
  $(this).hide()
  $('#couponCodeDiv').show()
})


//  -------------------------- template code --------------------------------------------------
function generateRegMsg(data) {
  return `<!DOCTYPE html>
                        <html lang="en">

                        <head>
                            <meta charset="UTF-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <meta http-equiv="X-UA-Compatible" content="ie=edge">
                            <title></title>
                        </head>

                        <body>
                            <p>Name: <b>${data.name}</b></p>
                            <p>TzId: <b>${data.uid}</b></p>
                            <p>DOB: <b>${data.dob}</b></p>
                            <p>Gender: <b>${data.gender}</b></p>
                            <p>Mobile: <b>${data.mobile}</b></p>
                            <p>Email: <b>${data.email}</b></p>
                            <p>College: <b>${data.colleges.name}</b></p>
                            <p>State: <b>${data.state}</b></p>
                            <p>City: <b>${data.city}</b></p>
                            <p>Picode: <b>${data.pincode}</b></p>
                            <p>Created at: <b>${data.createdAt}</b></p>
                        </body>

                        </html>`
}


function generatePaymentMsg(data, razor_pay_id, amount) {

  let code_owner = couponApplied ? `<p>Referrered By: <b>${referredUser.owner.name}</b></p>` : '<p></p>'
  try {
    return `<!DOCTYPE html>
                        <html lang="en">

                        <head>
                            <meta charset="UTF-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <meta http-equiv="X-UA-Compatible" content="ie=edge">
                            <title></title>
                        </head>

                        <body>
                            ${code_owner}
                            <p>Razor Pay: <b>${razor_pay_id}</b>, Amount: <b>${amount}</b></p>
                            <p>Name: <b>${data.name}</b></p>
                            <p>TzId: <b>${data.uid}</b></p>
                            <p>DOB: <b>${data.dob}</b></p>
                            <p>Gender: <b>${data.gender}</b></p>
                            <p>Mobile: <b>${data.mobile}</b></p>
                            <p>Email: <b>${data.email}</b></p>
                            <p>College: <b>${data.tempClg.name}</b></p>
                            <p>State: <b>${data.state}</b></p>
                            <p>City: <b>${data.city}</b></p>
                            <p>Picode: <b>${data.pincode}</b></p>
                            <p>Created at: <b>${data.createdAt}</b></p>
                            

                        </body>

                        </html>`
  } catch (error) {
    return `Something went wrong 
                <br/> 
                ${JSON.stringify(data)} | ${razor_pay_id} | ${amount}`
  }

}


function referralCodeOfOwnerTemplate(data, name) {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">

                <head>
                  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                  <meta name="viewport" content="width=device-width, initial-scale=1" />
                  <title>Neopolitan Confirm Email</title>
                  <!-- Designed by https://github.com/kaytcat -->
                  <!-- Robot header image designed by Freepik.com -->

                  <style type="text/css">
                    @import url(http://fonts.googleapis.com/css?family=Droid+Sans);

                    /* Take care of image borders and formatting */

                    p {
                      font-size: 18px !important
                    }

                    img {
                      max-width: 600px;
                      outline: none;
                      text-decoration: none;
                      -ms-interpolation-mode: bicubic;
                    }

                    a {
                      text-decoration: none;
                      border: 0;
                      outline: none;
                      color: #bbbbbb;
                    }

                    a img {
                      border: none;
                    }

                    /* General styling */

                    td,
                    h1,
                    h2,
                    h3 {
                      font-family: Helvetica, Arial, sans-serif;
                      font-weight: 400;
                    }

                    td {
                      text-align: center;
                    }

                    body {
                      -webkit-font-smoothing: antialiased;
                      -webkit-text-size-adjust: none;
                      width: 100%;
                      height: 100%;
                      color: #37302d;
                      background: #ffffff;
                      font-size: 16px;
                    }

                    table {
                      border-collapse: collapse !important;
                    }

                    .headline {
                      color: #ffffff;
                      font-size: 36px;
                    }

                    .force-full-width {
                      width: 100% !important;
                    }

                    .force-width-80 {
                      width: 80% !important;
                    }
                  </style>

                  <style type="text/css" media="screen">
                    @media screen {

                      /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
                      td,
                      h1,
                      h2,
                      h3 {
                        font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                      }
                    }
                  </style>

                  <style type="text/css" media="only screen and (max-width: 480px)">
                    /* Mobile styles */
                    @media only screen and (max-width: 480px) {

                      table[class="w320"] {
                        width: 320px !important;
                      }

                      td[class="mobile-block"] {
                        width: 100% !important;
                        display: block !important;
                      }


                    }
                  </style>
                </head>

                <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
                  bgcolor="#ffffff">
                  <table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
                    <tr>
                      <td align="center" valign="top" bgcolor="#ffffff" width="100%">
                        <center>
                          <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
                            <tr>
                              <td align="center" valign="top">

                                <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
                                  style="margin:0 auto;">
                                  <tr>
                                    <td style="font-size: 15px; text-align:center;">
                                      <br>
                                      <img src="https://techzillaindia.com/upskill/wp-content/uploads/2019/01/WHITE.png" height="50"
                                        width="160" alt="">
                                      <br>
                                      <br>
                                    </td>
                                  </tr>
                                </table>

                                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#4dbfbf">
                                  <tr>
                                    <td>
                                      <br>
                                      <img src="https://techzillaindia.com/template_images/robowave_03.gif" width="224" height="240" alt="robot picture">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="headline">
                                      Congratulations!
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>

                                      <center>
                                        <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="60%">
                                          <tr>
                                            <td style="color:#083d3d;text-align: left;">
                                              <br>

                                              <p>Hey <span style="color:black"><b>${name}</b></span>,</p>

                                          <p>   Greetings from Techzilla India!
                                              You can earn Rs. 100 cash reward on each successful enrollment of your friend and your friend gets 100 instant discount too.
                                              <br/>
                                              <br/>
                                              Share the Referral code: <b>${data.unique_code}</b>,  
                                              with your friends and ask them to register for NUET: <a style="color:blue" href="https://techzillaindia.com/upskill/registration">https://techzillaindia.com/upskill/registration</a>
                                              <br/>
                                              <br/>
                                              On every successful registration of your friend you will get an email & sms of his enrollment. 
                                              <br/>
                                              <br/>
                                              Once you refer 3 friends, you can request a cashout via WhatsApp message on this 7304322623. 
                                              <br/>
                                              <br/>
                                              We shall verify & issue your cashout in 2 working days. 
                                              <br/>
                                              <br/>
                                              Minimum 3 referral required for cashout.
                                              <br/>
                                              <br/>
                                              Payment method- Bank Transfer only.
                                              <br/>
                                              Details needed for cashout
                                              <br/>
                                              Payee Name
                                              <br/>
                                              Account Number
                                              <br/>
                                              IFSC CODE
                                              <br/>
                                              Branch Name
                                              </p>
                                              
                                              <br>

                                              <h3>You can use this message to forward in your groups.</h3>
                                              
                                              <p>
                                              *Dream job* with a guaranteed package of around *7LPA* but don't have enough skills ?
                                                We have got your back 📇
                                                <br>

                                                *Techzilla India* & *E Cell, IIT Bombay* present <br>
                                                *National Upskill Entrance Test* <br>
                                                A search for India's next gen technology leaders <br>
                                                <br>
                                                What is *National Upskill Programme?*
                                                <br>
                                                💻100 Days no cost Intensive training programme.
                                                <br>
                                                💻Assured placement of INR 7LPA Avg salary. 
                                                <br>
                                                💻 Hands on experience on technologies used by Big giants like Facebook, Microsoft,Flipkart,Airbnb etc.
                                                <br>
                                                💻Training by industry experts & Live projects and certification at no cost.
                                                <br>
                                                *No CGPA Criteria* : Students studying in Final year or graduates from science stream(BTech,BE,ME,MCA,BCA,Bsc,MSc,diploma )
                                                <br>
                                                <br>
                                                *Our process*
                                                1.NUET(Entrance Test) <br>
                                                2.Screening<br>
                                                3.Training<br>
                                                4.Get Placed. <br>
                                                <br>
                                                For more details on the training module & about the programme visit <span style="color:blue">https://techzillaindia.com/upskill</span>
                                                <br>
                                                
                                                Limited seats and first come first serve basis.<br>

                                                Register now at <span style="color:blue">https://techzillaindia.com/upskill/registration</span> <br>
                                                Use my code ${data.unique_code} to get INR 100/- instant discount on the application fee.<br>
                                                <br>

                                                </p>
                                            </td>
                                          </tr>
                                        </table>
                                      </center>

                                    </td>
                                  </tr>
                                </table>


                                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141"
                                  style="margin: 0 auto">
                                  <tr>
                                    <td style="color:#bbbbbb; font-size:15px">
                                      <br>
                                      <span style="text-align: center;">
                                      For any further assistance email us at <b style="color:black">upskill@techzillaindia.com</b>.
                                      </span>

                                    </td>
                                  </tr>

                                  <tr>

                                    <td style="color:#bbbbbb; font-size:12px;">
                                      <br>
                                      <br>
                                      <span>National UpSkill programme by Techzilla India Infotech</span>

                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="color:#bbbbbb; font-size:12px;">
                                      <br>
                                      © 2019 All Rights Reserved
                                      <br>
                                      <br>
                                      <br>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </center>
                      </td>
                    </tr>
                  </table>
                </body>

                </html>`
}


function registrationSuccessfulTemplate(full_name, amt) {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">

                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <title></title>
                    <style type="text/css">
                        @import url(http://fonts.googleapis.com/css?family=Droid+Sans);

                        /* Take care of image borders and formatting */

                        p{
                            font-size:15px !important
                        }

                        img {
                            max-width: 600px;
                            outline: none;
                            text-decoration: none;
                            -ms-interpolation-mode: bicubic;
                        }

                        a {
                            text-decoration: none;
                            border: 0;
                            outline: none;
                            color: black;
                        }

                        a img {
                            border: none;
                        }

                        /* General styling */

                        td,
                        h1,
                        h2,
                        h3 {
                            font-family: Helvetica, Arial, sans-serif;
                            font-weight: 400;
                        }

                        td {
                            text-align: center;
                        }

                        .buttomCls {
                            background-color: #178f8f;
                            border-radius: 4px;
                            color: #ffffff;
                            display: inline-block;
                            font-family: Helvetica, Arial, sans-serif;
                            font-size: 16px;
                            font-weight: bold;
                            line-height: 40px;
                            text-align: center;
                            text-decoration: none;
                            width: 150px;
                            -webkit-text-size-adjust: none;
                            margin-top: 15px;
                        }

                        body {
                            -webkit-font-smoothing: antialiased;
                            -webkit-text-size-adjust: none;
                            width: 100%;
                            height: 100%;
                            color: #37302d;
                            background: #ffffff;
                            font-size: 16px;
                        }

                        table {
                            border-collapse: collapse !important;
                        }

                        .headline {
                            color: #ffffff;
                            font-size: 28px;
                        }

                        .force-full-width {
                            width: 100% !important;
                        }

                        .step-width {
                            width: 90px;
                            height: 91px;
                        }
                    </style>

                    <style type="text/css" media="screen">
                        @media screen {

                            /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
                            td,
                            h1,
                            h2,
                            h3 {
                                font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                            }
                        }
                    </style>

                    <style type="text/css" media="only screen and (max-width: 480px)">
                        /* Mobile styles */
                        @media only screen and (max-width: 480px) {

                            table[class="w320"] {
                                width: 320px !important;
                            }

                            img[class="step-width"] {
                                width: 80px !important;
                                height: 81px !important;
                            }


                        }
                    </style>
                </head>

                <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
                    bgcolor="#ffffff">
                    <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
                        <tr>
                            <td align="center" valign="top" bgcolor="#ffffff" width="100%">
                                <center>
                                    <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
                                        <tr>
                                            <td align="center" valign="top">

                                                <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
                                                    style="margin:0 auto;">
                                                    <tr>
                                                        <td style="font-size: 15px; text-align:center;">
                                                            <br>
                                                            <img src="https://techzillaindia.com/template_images/upskill.png"
                                                                height="50" width="160" alt="">
                                                            <br>
                                                            <br>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="100%" bgcolor="#4dbfbf">
                                                    <tr>
                                                        <td class="headline">
                                                            <br>
                                                            Registration Successful!
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <br>
                                                            <center>
                                                                <table style="margin:0 auto;width=40%" cellspacing="0" cellpadding="0" class="force-width-80">
                                                                    <tr>

                                                                        <td>
                                                                  <!-- ======== STEP ONE ========= -->
                                                                  <!-- ==== Please use this url: https://techzillaindia.com/template_images/1_dark.png in the src below in order to set the progress to one.
                                                        
                                                                                    Then replace step two with this url: https://techzillaindia.com/template_images/2_light.png
                                                                                    Then replace step three with this url: https://techzillaindia.com/template_images/3_light.gif ==== -->
                                    
                                                                  <img class="step-width" src="https://techzillaindia.com/template_images/1_dark.png" alt="step one">
                                                                </td>


                                                                <td>
                                                                <!-- ======== STEP TWO ========= -->
                                                                <!-- ==== Please use this url: https://techzillaindia.com/template_images/2_dark.gif in the src below in order to set the progress to two.
                                                        
                                                                                    Then replace step three with this url: https://techzillaindia.com/template_images/3_light.gif
                                                        
                                                                                    Then replace step one with this url: https://techzillaindia.com/template_images/1_light.png ==== -->
                                                              
                                                                  <img class="step-width" src="https://techzillaindia.com/template_images/2_light.png" alt="step two">
                                                                </td>


                                                                <td>
                                                                <!-- ======== STEP THREE ========= -->
                                                                <!-- ==== Please use this url: https://techzillaindia.com/template_images/3_dark.gif in the src below in order to set the progress to three.
                                                        
                                                                                    Then replace step one with this url: https://techzillaindia.com/template_images/1_light.png
                                                        
                                                                                    Then replace step two with this url: https://techzillaindia.com/template_images/2_light.png ==== -->
                                                              
                                                                  <img class="step-width" src="https://techzillaindia.com/template_images/3_light.gif" alt="step three">
                                                                </td>
                                                                    </tr>


                                                                    <tr>
                                                                        <td style="vertical-align:top; color:#187272; font-weight:bold;">
                                                                            Initiated
                                                                        </td>
                                                                        <td style="vertical-align:top; color:#187272; font-weight:bold;">
                                                                            Confirmed
                                                                        </td>
                                                                        <td style="vertical-align:top; color:#187272; font-weight:bold;">
                                                                            Done!
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </center>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>

                                                            <center>
                                                                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="70%">
                                                                    <tr>
                                                                        <td style="color:#083d3d;text-align: left;padding:5px;">
                                                                            <br>
                                                                            <br>

                                                                            <p>Hey <span style="color:black"><b>${getFirstName(full_name)}</b></span>,</p>
                                                                            <p>You have successfully completed the
                                                                                registration process for NUET.</p>
                                                                            <p>Kindly pay
                                                                                ${amt}/- to the Upskill representative within 2
                                                                                working
                                                                                days or pay online
                                                                                <a href="http://bit.ly/NUETTZ" style="color: black;text-decoration: underline">here</a>
                                                                                to avoid
                                                                                cancellation of your
                                                                                seat.</p>

                                                                            <br>
                                                                            <br>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </center>

                                                        </td>
                                                    </tr>

                                                </table>

                                                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width"
                                                    bgcolor="#414141" style="margin: 0 auto">
                                                    <tr>
                                                        <td style="color:#bbbbbb; font-size:15px">
                                                            <br>
                                                            <span style="text-align: center;">
                                                            For any further assistance email us at <b style="color:black">upskill@techzillaindia.com</b>.
                                                            </span>
                                                        </td>
                                                    </tr>

                                                    <tr>

                                                        <td style="color:#bbbbbb; font-size:12px;">
                                                            <br/>
                                                            
                                                            <span>National UpSkill program by Techzilla India Infotech</span>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="color:#bbbbbb; font-size:12px;">
                                                            <br/>
                                                            © 2019 All Rights Reserved
                                                            <br>
                                                            <br>
                                                            <br>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </td>
                        </tr>
                    </table>
                </body>

                </html>`
}

function enrolledSuccessfulTemplate(full_name, _currentUser) {
  // console.log('enrolledSuccessfulTemplate =>', full_name, _currentUser)

  let venue_date = _currentUser.examvenue ? _currentUser.examvenue.name.split('-') : ["", ""]

  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                  <html xmlns="http://www.w3.org/1999/xhtml">

                  <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <title>Neopolitan Confirm Email</title>
                    <!-- Designed by https://github.com/kaytcat -->
                    <!-- Robot header image designed by Freepik.com -->

                    <style type="text/css">
                      @import url(http://fonts.googleapis.com/css?family=Droid+Sans);

                      /* Take care of image borders and formatting */

                      p{
                              font-size:15px !important
                          }

                      img {
                        max-width: 600px;
                        outline: none;
                        text-decoration: none;
                        -ms-interpolation-mode: bicubic;
                      }

                      a {
                        text-decoration: none;
                        border: 0;
                        outline: none;
                        color: #bbbbbb;
                      }

                      a img {
                        border: none;
                      }

                      /* General styling */

                      td,
                      h1,
                      h2,
                      h3 {
                        font-family: Helvetica, Arial, sans-serif;
                        font-weight: 400;
                      }

                      td {
                        text-align: center;
                      }

                      body {
                        -webkit-font-smoothing: antialiased;
                        -webkit-text-size-adjust: none;
                        width: 100%;
                        height: 100%;
                        color: #37302d;
                        background: #ffffff;
                        font-size: 16px;
                      }

                      table {
                        border-collapse: collapse !important;
                      }

                      .headline {
                        color: #ffffff;
                        font-size: 36px;
                      }

                      .force-full-width {
                        width: 100% !important;
                      }

                      .force-width-80 {
                        width: 80% !important;
                      }
                    </style>

                    <style type="text/css" media="screen">
                      @media screen {

                        /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
                        td,
                        h1,
                        h2,
                        h3 {
                          font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                        }
                      }
                    </style>

                    <style type="text/css" media="only screen and (max-width: 480px)">
                      /* Mobile styles */
                      @media only screen and (max-width: 480px) {

                        table[class="w320"] {
                          width: 320px !important;
                        }

                        td[class="mobile-block"] {
                          width: 100% !important;
                          display: block !important;
                        }


                      }
                    </style>
                  </head>

                  <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
                    bgcolor="#ffffff">
                    <table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
                      <tr>
                        <td align="center" valign="top" bgcolor="#ffffff" width="100%">
                          <center>
                            <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
                              <tr>
                                <td align="center" valign="top">

                                  <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
                                    style="margin:0 auto;">
                                    <tr>
                                      <td style="font-size: 15px; text-align:center;">
                                        <br>
                                        <img src="https://techzillaindia.com/template_images/upskill.png" height="50"
                                          width="160" alt="">
                                        <br>
                                        <br>
                                      </td>
                                    </tr>
                                  </table>

                                  <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#4dbfbf">
                                    <tr>
                                      <td>
                                        <br>
                                        <img src="https://techzillaindia.com/template_images/robomail.gif" width="224" height="240" alt="robot picture">
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="headline">
                                        Successfully Enrolled
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>

                                        <center>
                                          <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="70%">
                                            <tr>
                                              <td style="color:#083d3d;text-align: left;padding:5px;">
                                                <br>

                                                <p>Hey <span style="color:black"><b>${getFirstName(full_name)}</b></span>,</p>

                                                <p>Congratulations !!!</p>
                                               
                                                Your seat for the National Upskill Entrance Test (NUET) is confirmed with 
                                                seat no <span style="color:black"><b>${_currentUser.uid}</b></span>,
                                                which will be held on <span style="color:black"><b>${venue_date[1]}</b></span> 
                                                at <span style="color:black"><b>${venue_date[0]}</b></span>.
                                                
                                                <p>Gear up for NUET !</p>
                                                <p>For any further information
                                                  <a href="https://www.techzillaindia.com/upskill" style="color: black;text-decoration: underline">Click
                                                    here</a></p>
                                               
                                                
                                                <p>
                                                    You can generate the Admit card from 
                                                    <a style="color: black;text-decoration: underline" href="https://techzillaindia.com/upskill/hallticket/">
                                                      here
                                                    </a>
                                                    Download detailed Syllabus  
                                                    <a style="color:#53f6c6;" href="http://bit.ly/nuetsyb">
                                                    here
                                                    </a>
                                                  </p>
                                              </td>
                                            </tr>
                                          </table>
                                        </center>

                                      </td>
                                    </tr>
                                  </table>


                                  <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141"
                                    style="margin: 0 auto">
                                    <tr>
                                      <td style="color:#bbbbbb; font-size:15px">
                                        <br>
                                        <span style="text-align: center;">
                                        For any further assistance email us at <b style="color:black">upskill@techzillaindia.com</b>.
                                        </span>
                                      </td>
                                    </tr>

                                    <tr>

                                      <td style="color:#bbbbbb; font-size:12px;">
                                        <br/>
                                        <span>National UpSkill program by Techzilla India Infotech</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style="color:#bbbbbb; font-size:12px;">
                                          <br/>
                                        © 2019 All Rights Reserved
                                        <br>
                                        <br>
                                        <br>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </center>
                        </td>
                      </tr>
                    </table>
                  </body>
                  </html>`
}


function seatConfirmedTemplate(full_name, inputData) {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
              <html xmlns="http://www.w3.org/1999/xhtml">

              <head>
                  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                  <meta name="viewport" content="width=device-width, initial-scale=1" />
                  <title></title>
                  <style type="text/css">
                      @import url(http://fonts.googleapis.com/css?family=Droid+Sans);

                      /* Take care of image borders and formatting */

                      p{
                          font-size:15px !important
                      }

                      img {
                          max-width: 600px;
                          outline: none;
                          text-decoration: none;
                          -ms-interpolation-mode: bicubic;
                      }

                      a {
                          text-decoration: none;
                          border: 0;
                          outline: none;
                          color: black;
                      }

                      a img {
                          border: none;
                      }

                      /* General styling */

                      td,
                      h1,
                      h2,
                      h3 {
                          font-family: Helvetica, Arial, sans-serif;
                          font-weight: 400;
                      }

                      td {
                          text-align: center;
                      }

                      .buttomCls {
                          background-color: #178f8f;
                          border-radius: 4px;
                          color: #ffffff;
                          display: inline-block;
                          font-family: Helvetica, Arial, sans-serif;
                          font-size: 16px;
                          font-weight: bold;
                          line-height: 40px;
                          text-align: center;
                          text-decoration: none;
                          width: 150px;
                          -webkit-text-size-adjust: none;
                          margin-top: 15px;
                      }

                      body {
                          -webkit-font-smoothing: antialiased;
                          -webkit-text-size-adjust: none;
                          width: 100%;
                          height: 100%;
                          color: #37302d;
                          background: #ffffff;
                          font-size: 16px;
                      }

                      table {
                          border-collapse: collapse !important;
                      }

                      .headline {
                          color: #ffffff;
                          font-size: 28px;
                      }

                      .force-full-width {
                          width: 100% !important;
                      }

                      .step-width {
                          width: 90px;
                          height: 91px;
                      }
                  </style>

                  <style type="text/css" media="screen">
                      @media screen {

                          /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
                          td,
                          h1,
                          h2,
                          h3 {
                              font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                          }
                      }
                  </style>

                  <style type="text/css" media="only screen and (max-width: 480px)">
                      /* Mobile styles */
                      @media only screen and (max-width: 480px) {

                          table[class="w320"] {
                              width: 320px !important;
                          }

                          img[class="step-width"] {
                              width: 80px !important;
                              height: 81px !important;
                          }


                      }
                  </style>
              </head>

              <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
                  bgcolor="#ffffff">
                  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
                      <tr>
                          <td align="center" valign="top" bgcolor="#ffffff" width="100%">
                              <center>
                                  <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
                                      <tr>
                                          <td align="center" valign="top">

                                              <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
                                                  style="margin:0 auto;">
                                                  <tr>
                                                      <td style="font-size: 15px; text-align:center;">
                                                          <br>
                                                          <img src="https://techzillaindia.com/template_images/upskill.png"
                                                              height="50" width="160" alt="">
                                                          <br>
                                                          <br>
                                                      </td>
                                                  </tr>
                                              </table>

                                              <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="100%" bgcolor="#4dbfbf">
                                                  <tr>
                                                      <td class="headline">
                                                          <br>
                                                          Your seat is confirmed
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>
                                                          <br>
                                                          <center>
                                                              <table style="margin:0 auto;width=40%" cellspacing="0" cellpadding="0" class="force-width-80">
                                                                  <tr>



                                                                      <td>
                                                                          <!-- ======== STEP ONE ========= -->
                                                                          <!-- ==== Please use this url: https://www.filepicker.io/api/file/cdDaXwrhTd6EpvjpwqP1 in the src below in order to set the progress to one.
                                  
                                                              Then replace step two with this url: https://www.filepicker.io/api/file/MD29ZQs3RdK7mSu0VqxZ
                                                              Then replace step three with this url: https://www.filepicker.io/api/file/qnkuUNPS6TptLRIjWERA ==== -->

                                                                          <img class="step-width" src="https://techzillaindia.com/template_images/1_light.png"
                                                                              alt="step one">
                                                                      </td>



                                                                      <!-- ======== STEP TWO ========= -->
                                                                      <!-- ==== Please use this url: https://www.filepicker.io/api/file/QKOMsiThQcePodddaOHk in the src below in order to set the progress to two.
                                  
                                                              Then replace step three with this url: https://www.filepicker.io/api/file/qnkuUNPS6TptLRIjWERA
                                  
                                                              Then replace step one with this url: https://www.filepicker.io/api/file/MMVdxAuqQuy7nqVEjmPV ==== -->
                                                                      <td>
                                                                          <img class="step-width" src="https://techzillaindia.com/template_images/2_dark.gif"
                                                                              alt="step two">
                                                                      </td>


                                                                      <!-- ======== STEP THREE ========= -->
                                                                      <!-- ==== Please use this url: https://www.filepicker.io/api/file/mepNOdHRTCMs1Jrcy2fU in the src below in order to set the progress to three.
                                  
                                                              Then replace step one with this url: https://www.filepicker.io/api/file/MMVdxAuqQuy7nqVEjmPV
                                  
                                                              Then replace step two with this url: https://www.filepicker.io/api/file/MD29ZQs3RdK7mSu0VqxZ ==== -->
                                                                      <td>
                                                                          <img class="step-width" src="https://techzillaindia.com/template_images/3_light.gif"
                                                                              alt="step three">
                                                                      </td>
                                                                  </tr>


                                                                  <tr>
                                                                      <td style="vertical-align:top; color:#187272; font-weight:bold;">
                                                                          Initiated
                                                                      </td>
                                                                      <td style="vertical-align:top; color:#187272; font-weight:bold;">
                                                                          Confirmed
                                                                      </td>
                                                                      <td style="vertical-align:top; color:#187272; font-weight:bold;">
                                                                          Done!
                                                                      </td>
                                                                  </tr>
                                                              </table>
                                                          </center>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>

                                                          <center>
                                                              <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="70%">
                                                                  <tr>
                                                                      <td style="color:#083d3d;text-align: left;padding:5px;">
                                                                          <br>
                                                                          <br>

                                                                          <p>Hey <span style="color:black"><b>${getFirstName(full_name)}</b></span>,</p>
                                                                          <p>You have successfully completed the
                                                                              registration process for NUET.</p>
                                                                          <p>Kindly pay the remaining
                                                                              ${inputData.trx.Amount.rem}/- to the Upskill representative within 2
                                                                              working
                                                                              days or pay online
                                                                              <a href="http://bit.ly/NUETTZ" style="color: black;text-decoration: underline">here</a>
                                                                              to avoid
                                                                              cancellation of your
                                                                              seat.</p>
                                                                          <br>
                                                                          <br>
                                                                      </td>
                                                                  </tr>
                                                              </table>
                                                          </center>

                                                      </td>
                                                  </tr>

                                              </table>

                                              <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width"
                                                  bgcolor="#414141" style="margin: 0 auto">
                                                 
                                                  <tr>

                                                      <td style="color:#bbbbbb; font-size:12px;">
                                                          <br/>
                                                          <span>National UpSkill program by Techzilla India Infotech</span>

                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td style="color:#bbbbbb; font-size:12px;">
                                                          <br/>
                                                          © 2019 All Rights Reserved
                                                          <br>
                                                          <br>
                                                          <br>
                                                      </td>
                                                  </tr>
                                              </table>

                                          </td>
                                      </tr>
                                  </table>
                              </center>
                          </td>
                      </tr>
                  </table>
              </body>

              </html>`
}


function enrolledSuccessfulTemplate2(full_name, inputData) {
  // console.log('enrolledSuccessfulTemplate2 =>', full_name, inputData)
  let venue_date = inputData.examvenue ? inputData.examvenue.name.split('-') : ["", ""]
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">

                <head>
                  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                  <meta name="viewport" content="width=device-width, initial-scale=1" />
                  <title>Neopolitan Confirm Email</title>
                  <!-- Designed by https://github.com/kaytcat -->
                  <!-- Robot header image designed by Freepik.com -->

                  <style type="text/css">
                    @import url(http://fonts.googleapis.com/css?family=Droid+Sans);

                    /* Take care of image borders and formatting */

                    p{
                            font-size:15px !important
                        }

                    img {
                      max-width: 600px;
                      outline: none;
                      text-decoration: none;
                      -ms-interpolation-mode: bicubic;
                    }

                    a {
                      text-decoration: none;
                      border: 0;
                      outline: none;
                      color: #bbbbbb;
                    }

                    a img {
                      border: none;
                    }

                    /* General styling */

                    td,
                    h1,
                    h2,
                    h3 {
                      font-family: Helvetica, Arial, sans-serif;
                      font-weight: 400;
                    }

                    td {
                      text-align: center;
                    }

                    body {
                      -webkit-font-smoothing: antialiased;
                      -webkit-text-size-adjust: none;
                      width: 100%;
                      height: 100%;
                      color: #37302d;
                      background: #ffffff;
                      font-size: 16px;
                    }

                    table {
                      border-collapse: collapse !important;
                    }

                    .headline {
                      color: #ffffff;
                      font-size: 36px;
                    }

                    .force-full-width {
                      width: 100% !important;
                    }

                    .force-width-80 {
                      width: 80% !important;
                    }
                  </style>

                  <style type="text/css" media="screen">
                    @media screen {

                      /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
                      td,
                      h1,
                      h2,
                      h3 {
                        font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                      }
                    }
                  </style>

                  <style type="text/css" media="only screen and (max-width: 480px)">
                    /* Mobile styles */
                    @media only screen and (max-width: 480px) {

                      table[class="w320"] {
                        width: 320px !important;
                      }

                      td[class="mobile-block"] {
                        width: 100% !important;
                        display: block !important;
                      }


                    }
                  </style>
                </head>

                <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
                  bgcolor="#ffffff">
                  <table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
                    <tr>
                      <td align="center" valign="top" bgcolor="#ffffff" width="100%">
                        <center>
                          <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
                            <tr>
                              <td align="center" valign="top">

                                <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
                                  style="margin:0 auto;">
                                  <tr>
                                    <td style="font-size: 15px; text-align:center;">
                                      <br>
                                      <img src="https://techzillaindia.com/template_images/upskill.png" height="50"
                                        width="160" alt="">
                                      <br>
                                      <br>
                                    </td>
                                  </tr>
                                </table>

                                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#4dbfbf">
                                  <tr>
                                    <td>
                                      <br>
                                      <img src="https://techzillaindia.com/template_images/robomail.gif" width="224" height="240" alt="robot picture">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="headline">
                                        Congratulations!
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>

                                      <center>
                                        <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="70%">
                                          <tr>
                                            <td style="color:#083d3d;text-align: left;padding:5px;">
                                              <br>

                                              <p>Hey <span style="color:black"><b>${getFirstName(full_name)}</b></span>,</p>

                                              Your seat for the National Upskill Entrance Test (NUET) is confirmed with 
                                              seat no <span style="color:black"><b>${inputData.par.uid}</b></span>,
                                              which will be held on <span style="color:black"><b>${venue_date[1]}</b></span> 
                                                at <span style="color:black"><b>${venue_date[0]}</b></span>.

                                              <p>For any further information
                                                <a href="https://www.techzillaindia.com/upskill" style="color: black;text-decoration: underline">Click
                                                  here</a></p>
                                            

                                              <p>
                                                Admit card details will be sent 15 days prior to the test date.
                                                Download detailed Syllabus  
                                                <a style="color:#53f6c6;" href="http://bit.ly/nuetsyb">
                                                here
                                                </a>
                                              </p>
                                            </td>
                                          </tr>
                                        </table>
                                      </center>

                                    </td>
                                  </tr>
                                </table>


                                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141"
                                  style="margin: 0 auto">
                                  <tr>
                                    <td style="color:#bbbbbb; font-size:15px">
                                        <br/>
                                      <span style="text-align: center;">
                                      For any further assistance email us at <b style="color:black">upskill@techzillaindia.com</b>.
                                      </span>
                                      
                                    </td>
                                  </tr>

                                  <tr>

                                    <td style="color:#bbbbbb; font-size:12px;">
                                    <br/>
                                      <span>National UpSkill program by Techzilla India Infotech</span>

                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="color:#bbbbbb; font-size:12px;">
                                        <br/>
                                      © 2019 All Rights Reserved
                                      <br>
                                      <br>
                                      <br>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </center>
                      </td>
                    </tr>
                  </table>
                </body>
                </html>`
}

function friendReferredTemplate(referredUser, user_for_referral) {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">

                <head>
                  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                  <meta name="viewport" content="width=device-width, initial-scale=1" />
                  <title>Neopolitan Confirm Email</title>
                  <!-- Designed by https://github.com/kaytcat -->
                  <!-- Robot header image designed by Freepik.com -->

                  <style type="text/css">
                    @import url(http://fonts.googleapis.com/css?family=Droid+Sans);

                    /* Take care of image borders and formatting */

                    p {
                      font-size: 18px !important
                    }

                    img {
                      max-width: 600px;
                      outline: none;
                      text-decoration: none;
                      -ms-interpolation-mode: bicubic;
                    }

                    a {
                      text-decoration: none;
                      border: 0;
                      outline: none;
                      color: #bbbbbb;
                    }

                    a img {
                      border: none;
                    }

                    /* General styling */

                    td,
                    h1,
                    h2,
                    h3 {
                      font-family: Helvetica, Arial, sans-serif;
                      font-weight: 400;
                    }

                    td {
                      text-align: center;
                    }

                    body {
                      -webkit-font-smoothing: antialiased;
                      -webkit-text-size-adjust: none;
                      width: 100%;
                      height: 100%;
                      color: #37302d;
                      background: #ffffff;
                      font-size: 16px;
                    }

                    table {
                      border-collapse: collapse !important;
                    }

                    .headline {
                      color: #ffffff;
                      font-size: 36px;
                    }

                    .force-full-width {
                      width: 100% !important;
                    }

                    .force-width-80 {
                      width: 80% !important;
                    }
                  </style>

                  <style type="text/css" media="screen">
                    @media screen {

                      /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
                      td,
                      h1,
                      h2,
                      h3 {
                        font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                      }
                    }
                  </style>

                  <style type="text/css" media="only screen and (max-width: 480px)">
                    /* Mobile styles */
                    @media only screen and (max-width: 480px) {

                      table[class="w320"] {
                        width: 320px !important;
                      }

                      td[class="mobile-block"] {
                        width: 100% !important;
                        display: block !important;
                      }


                    }
                  </style>
                </head>

                <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
                  bgcolor="#ffffff">
                  <table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
                    <tr>
                      <td align="center" valign="top" bgcolor="#ffffff" width="100%">
                        <center>
                          <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
                            <tr>
                              <td align="center" valign="top">

                                <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
                                  style="margin:0 auto;">
                                  <tr>
                                    <td style="font-size: 15px; text-align:center;">
                                      <br>
                                      <img src="https://techzillaindia.com/upskill/wp-content/uploads/2019/01/WHITE.png" height="50"
                                        width="160" alt="">
                                      <br>
                                      <br>
                                    </td>
                                  </tr>
                                </table>

                                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#4dbfbf">
                                  <tr>
                                    <td>
                                      <br>
                                      <img src="https://techzillaindia.com/template_images/robowave_03.gif" width="224" height="240" alt="robot picture">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="headline">
                                      Congratulations!
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>

                                      <center>
                                        <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="60%">
                                          <tr>
                                            <td style="color:#083d3d;text-align: left;">
                                              <br>

                                              <p>Hey <span style="color:black"><b>${titleCase(getFirstName(referredUser.owner.name))}</b></span>,</p>

                                              Your friend <b>${user_for_referral.name}</b> has sucessfully enrolled with us for NUET.

                                              <p>Once you refer 3 friends, you can request a cashout via WhatsApp message on this 7304322623.</p>

                                              <br/>
                                              We shall verify & issue your cashout in 2 working days. 
                                              <br/>
                                              Payment method- Bank Transfer only.
                                              <br/>
                                              Details needed for cashout
                                              <br/>
                                              Payee Name
                                              <br/>
                                              Account Number
                                              <br/>
                                              IFSC -CODE
                                              <br/>
                                              Branch Name
                                              <br/><br/>
                                            
                                              
                                              <p>Keep sharing National Upskill programme by referring more friends & earn cash rewards.</p>
                                              <br>
                                              <br>
                                            </td>
                                          </tr>
                                        </table>
                                      </center>

                                    </td>
                                  </tr>
                                </table>


                                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141"
                                  style="margin: 0 auto">
                                  <tr>
                                    <td style="color:#bbbbbb; font-size:15px">
                                      <br>
                                      <span style="text-align: center;">
                                      For any further assistance email us at <b style="color:black">upskill@techzillaindia.com</b>.
                                      </span>

                                    </td>
                                  </tr>

                                  <tr>

                                    <td style="color:#bbbbbb; font-size:12px;">
                                      <br>
                                      <br>
                                      <span>National UpSkill program by Techzilla India Infotech</span>

                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="color:#bbbbbb; font-size:12px;">
                                      <br>
                                      © 2019 All Rights Reserved
                                      <br>
                                      <br>
                                      <br>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </center>
                      </td>
                    </tr>
                  </table>
                </body>

                </html>`
}