$('.cls-logout').on('click', function () {
    Cookies.remove('_t', { path: '/' });
    Cookies.remove('_email', { path: '/' });
    Cookies.remove('_uName', { path: '/' });
    Cookies.remove('_role', { path: '/' });
    Cookies.remove('_id', { path: '/' });
    Cookies.remove('_clg', { path: '/' });
})