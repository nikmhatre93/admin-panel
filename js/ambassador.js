
let trxList = []
let collegeList = {}
let colleges = []
let ambData = {}
// const base_url = 'https://upskilladmin.techzillaindia.com'
// const base_url = 'http://localhost:1337'
$.ajax({
    type: "GET",
    url: `${base_url}/transactions?_limit=100000000`,
    headers: {
        Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (data) {
        trxList = data
    },
    error: function (err) {
        console.log(err)
    }
});

// $('#select-role').on('change', function () {

//     if ($(this).val() == 'amb') {
//         $(`.tempTrstd, .footable-row-detail`).remove()
//         fillTable('${base_url}/users', 'amb')
//     } else if ($(this).val() == 'std') {
//         $(`.tempTramb, .footable-row-detail`).remove()
//         if ('authenticated' == auth_role) {
//             fillTable(`https//upskilladmin.techzillaindia.com/participants`, 'std')
//         } else {
//             fillTable(`https//upskilladmin.techzillaindia.com/participantsAll?owner=${owner}`, 'std')
//         }
//     } else {
//         gernerateTable()
//     }

// })

let tbl = `<table id="datatable" class="footable table" data-page-size="100" data-filter=#filter>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>College</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Role</th>
                            <th>Collected</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="trans-table">

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                <ul class="pagination float-right"></ul>
                            </td>
                        </tr>
                    </tfoot>
                </table>`


// get college list
$.ajax({
    type: "GET",
    url: `${base_url}/colleges?_limit=10000000`,
    headers: {
        Authorization: `Bearer ${Cookies.get('_t')}`
    },
    success: function (data) {
        colleges = data
    },
    error: function (err) {
        console.log(err)
    }
});

// 

function fillTable(uri, type) {
    $.ajax({
        type: "GET",
        url: uri,
        headers: {
            Authorization: `Bearer ${Cookies.get('_t')}`
        },
        success: function (data) {

            colleges.forEach(element => {
                collegeList[element._id] = element.name
            });

            data = data.slice(3)

            $(`.tempTr${type}, .footable-row-detail`).remove()
            let tempTr = appendTable(data, type)
            $("#trans-table").append(tempTr)
            $('.footable').footable();

            $('.removebtn').on('click', removeAmb)

        },
        error: function (err) {
            console.log(err)
        }
    });
}

function appendTable(data, t) {
    let tr = ``
    data.forEach(item => {
        if (item.role.name == "Public") {
            ambData[item.id] = item
            tr += generateRow(item, t)
        }
    })

    return tr
}

let currentAmb = ''
function removeAmb(e) {
    currentAmb = e.target.id
    $('#remove-name').text(`Remove ${ambData[currentAmb].name}`)
    $('#myModal').modal('show');

}

$('#myModal').on('hidden.bs.modal', function () {
    currentId = ''
    $('#remove-name').text('')
})


function removeClick() {
    $.ajax({
        type: "DELETE",
        url: `${base_url}/users/${currentAmb}`,
        headers: {
            Authorization: `Bearer ${Cookies.get('_t')}`
        },
        success: function (data) {
            $('#myModal').modal('hide');
            toastr.success('Successful', { timeOut: 2500 })
            fillTable(`${base_url}/users?_limit=100000000`, 'amb')

        },
        error: function (err) {
            console.log(err)
        }
    });
}

function generateRow(i, type) {

    let initial = 0
    let rem = 0

    trxList.forEach(trx => {
        if (trx.participants.owner == i._id) {
            initial += parseInt(trx.Amount.initial)
            rem += parseInt(trx.Amount.rem)
        }
    })

    return `<tr class="tempTr${type}">
                        <td>${i.name}</td>
                        <td>${collegeList[i.colleges._id]}</td>
                        <td>${i.email}</td>
                        <td>${i.mobile}</td>
                        <td>Ambassador</td>
                        <td>Initial ${initial} <br/> Rem ${rem}</td>
                        <td>
                            
                            <a target="_blank" href="studentlist.html?sid=${i.id}" class="btn btn-default" id="${i.id}">Student List</a>
                            <spna style="padding:0px 10px;"></span>
                            <button class="btn btn-danger removebtn" id="${i.id}">Remove</button>
                        </td>
                    </tr>`


}





function gernerateTable() {
    $('.footable').remove()
    $('#table-res').append(tbl)

    fillTable(`${base_url}/users?_limit=10000000`, 'amb')

}

gernerateTable()



