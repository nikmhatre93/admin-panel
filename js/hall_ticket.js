
// user who verified with phone number
var currentUserForHT = JSON.parse(sessionStorage.getItem("currentUserForHT"))
let timeSlots = JSON.parse(sessionStorage.getItem("time_dynamic_slots"));
let thresholdGlobObj = JSON.parse(sessionStorage.getItem("threshold_values"));
let fullTimeSlotHtml = `<br/><span style="font-size:12px;color:#f00;text-transform: none; line-height: 1.9em;">This time slot is full</span>`;
var _step = sessionStorage.getItem('_step_ht')
var retryCallCount = 0
// var base_url = 'http://localhost:1337'
const base_url = `https://upskilladmin.techzillaindia.com`
var setIntervalId;
var timer = $('#timer')
var resendOtp = $('#resendOtp')
var file;

$('.mt').css('margin-top', '3%');
FilePond.registerPlugin(FilePondPluginFileValidateType);


$('#chooseImage').filepond({
    labelIdle: '<p style="font-size:14px" >Choose your passport size photo <span style="text-decoration:underline;font-weight:600">Browse</span><br><span style="font-size: 10px;">Formats: PNG, JPEG</span></p>',
    allowMultiple: false,
    maxFiles: 1,
    allowDrop: false,
    acceptedFileTypes: ['image/png', 'image/jpeg'],
    required: true
});



// Check current user from session storage
checkCurrentState()


// disabled scroll on number field
$('form').on('focus', 'input[type=number]', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
        e.preventDefault()
    })
})


// enabled scroll on number field
$('form').on('blur', 'input[type=number]', function (e) {
    $(this).off('mousewheel.disableScroll')
})


// generate opt
$('#getOpt').on('click', function (e) {
    if ($("#getOtpForm").valid()) {
        e.preventDefault()

        // get token call
        getTokenFromDb()


        // afterOptSent()
    }
})

function getOtpRewrittenFunction() {

    setIntervalId = setInterval(function () {
        timer.text(`( ${remainingTime()} )`)
    }, 1000)

    // TODO:
    // get otp call

    getOtp($('#phoneNumber').val())

}

// verify otp
$('#verifyOtp').on('click', function (e) {
    if ($("#verifyOtpForm").valid()) {
        e.preventDefault()

        // Verify call
        verifyOtp($('#phoneNumber').val(), $('#otp').val())
        // getDetailsIfExists()
        // afterOtpVerified()
    }
})


// resend call for otp
$('#resendOtp').on('click', function (e) {
    e.preventDefault()
    retryCallCount += 1
    $('#info-msg').html(`you'll get a call on <span style="font-weight:600;">+91 ${$('#phoneNumber').val()}</span>`)

    // retry call
    resendOtpCall($('#phoneNumber').val())

    if (retryCallCount == 2) {
        $(this).css("pointer-events", 'none');
        $(this).css("color", 'grey');
    }

})

// Select time slot
$('.time_slots').click(function () {
    $('.time_slots').removeClass('time_slots_active')
    getCurrentThreshold("Click")
    $(this).addClass('time_slots_active')
})


// filepond event
$('#chooseImage').on('FilePond:addfile', function (e) {
    // console.log('file added event', e.detail.file.file)
    file = e.detail.file.file
});


// submit
$('#uploadImage').click(function (e) {

    if ($('#uploadForm').valid()) {
        e.preventDefault()
        if ($('.time_slots_active').text() != '') {
            currentUserForHT.name = $('#fullName').val();
            sessionStorage.setItem("currentUserForHT", JSON.stringify(currentUserForHT));

            var selected_slot = $('.time_slots_active').data('id')
            // var selected_dynamic_slot = $('.time_slots_active').html()
            // console.log(selected_dynamic_slot)
            var select_time = $('.time_slots_active').text()
            var reportingTime;
            let firstTimeGiven = select_time.split("-")[0];
            // console.log(firstTimeGiven)

            var d = new Date(`July 21, 1983 ${firstTimeGiven}`);

            let minutes = d.getMinutes();
            let hours = (d.getHours() - 1)

            let amPmCheck = '';

            if (hours < 10) {
                hours = "0" + hours
            }
            if (hours < 12) {
                amPmCheck = "AM"
            }
            else {
                amPmCheck = "PM"
            }

            if (minutes < 10) {
                minutes = "0" + minutes;
            }

            // for 12 hrs format
            hours = hours % 12;
            hours = hours ? hours : 12;

            reportingTime = hours + ":" + minutes + ` ${amPmCheck}`;

            // if (selected_slot == '1st') {
            //     reportingTime = '10 AM'
            // } else {
            //     reportingTime = '12.30 PM'
            // }

            if (confirm(`You have selected ${select_time}. Your reporting time will be ${reportingTime}.`)) {
                // e.preventDefault()
                getCurrentThreshold("Submit")

            } else {
                // e.preventDefault()
                console.log('cancel')
            }
        } else {
            alert('Please select valid time slot.')
        }
    } else {
        // console.log(($('#fullName').val()).trim())
        if (($('#fullName').val()).trim() == '') {
            alert("You can't leave Full Name Field blank");
            $('#fullName').focus();
        }
        else {
            currentUserForHT.name = $('#fullName').val();
            sessionStorage.setItem("currentUserForHT", JSON.stringify(currentUserForHT));
            alert('Please upload a photo.')
        }

    }
})

var remainingTime = (function () {
    var time = 30
    return function () {
        time -= 1
        if (time <= 0) {
            clearInterval(setIntervalId)
            return ''
        }
        return time
    }
})()


function beforeOtpSent() {
    $('#getOtpDiv').show()
    $('#verifyOtpDiv').hide()
    $('#user_name').hide()
    // $('#fileUploadDivHeader').hide()
    $('#fileUploadDiv').hide()
    $('#generateHtDiv').hide()
}


function afterOptSent() {
    currentUserForHT = JSON.parse(sessionStorage.getItem("currentUserForHT"))
    $('#afterOtpMsg').html(`OTP has been sent to <span style="font-weight:600;">+91 ${$('#phoneNumber').val()}</span> & <span style="font-weight:600;">${currentUserForHT.email}</span>`)
    sessionStorage.setItem('_step_ht', 'otp_sent');
    $('#getOtpDiv').hide()
    $('#verifyOtpDiv').show()
    $('#resendOtp').show()
    $('#fileUploadDiv').hide()
    // $('#fileUploadDivHeader').hide()
    $('#generateHtDiv').hide()
    $('#user_name').hide()
    setTimeout(function () {
        timer.text('')
        resendOtp.css("pointer-events", 'auto');
        resendOtp.css("color", '#1d7d5d');
        clearInterval(setIntervalId)
    }, 30000)
}


function afterOtpVerified() {

    setuserDetails('afterOtpVerified')
    sessionStorage.setItem('_step_ht', 'otp_verified');
    $('#getOtpDiv').hide()
    $('#verifyOtpDiv').hide()
    $('#fileUploadDiv').show()
    $('#fileUploadDivHeader').show()
    $('#generateHtDiv').hide()


}


function afterPhotoUpload() {

    generateHallTicket()
    setuserDetails('afterPhotoUpload')
    $('#getOtpDiv').hide()
    $('#verifyOtpDiv').hide()
    $('#fileUploadDiv').hide()
    // $('#fileUploadDivHeader').hide()
    $('#generateHtDiv').show()
}


// persistant with refresh
function checkCurrentState() {
    // console.log(currentUserForHT)
    let venueMsg;
    if (currentUserForHT != undefined) {

        if (currentUserForHT.pic == "") {
            // afterOtpVerified()
            _step = sessionStorage.getItem('_step_ht')
            if (_step == undefined) {
                beforeOtpSent()
            } else if (_step == 'otp_sent') {
                afterOptSent()
            } else {

                $('#fullName').val(currentUserForHT.name);

                if (timeSlots.slot2 == "") {
                    $('.examTwoSlotSelectOption').hide()
                    $('.examOnlyOneSlot').show()
                    $('#1st_slot_1slotExam').html(timeSlots.slot1);
                    $('#1st_slot_1slotExam').addClass('time_slots_active');
                    venueMsg = `We only have 1 time slot for <b>${timeSlots.venue}</b> Venue`
                    $('.showExamVenue').html(venueMsg)
                } else {
                    getCurrentThreshold("Click");
                    $('.examOnlyOneSlot').hide()
                    $('.examTwoSlotSelectOption').show()
                    let prevVenueText = $('.showExamVenue').text();
                    venueMsg = prevVenueText + ` for <b>${timeSlots.venue}</b> Venue`;
                    $('.showExamVenue').html(venueMsg);

                }
                afterOtpVerified()

            }
        } else {
            afterPhotoUpload()
        }
    } else {
        _step = sessionStorage.getItem('_step_ht')
        if (_step == undefined) {
            beforeOtpSent()
        } else if (_step == 'otp_sent') {
            afterOptSent()
        } else {
            afterOtpVerified()
        }
    }
}


/* ------------------------------------ api calls ------------------------------------ */


/* get access token */
// get auth token from db new method
function getTokenFromDb() {
    $.ajax({
        type: "GET",
        url: `${base_url}/tokens`,
        success: function (data) {
            // console.log(data)

            if (data.length == 0) {

                initializeTokenData();
            }
            else {
                let AuthToken = data[0].AuthToken;
                sessionStorage.setItem('token_id', data[0]._id);

                if (AuthToken) { // if token present check if valid or not

                    sessionStorage.setItem('_tkn', AuthToken);
                    sessionStorage.setItem('_usr', data[0].userDetails);

                    let decodedToken = parseJwt(AuthToken);
                    var current_time = new Date().getTime() / 1000;

                    if (current_time > decodedToken.exp) { // token expired get a new one
                        getToken();
                    }
                    else { // token valid
                        getOtpRewrittenFunction();
                    }

                }
                else {  // token not present in db so request

                    getToken();

                }
            }
        }
    });
}

function initializeTokenData() {
    let tokenInitialData = {
        "AuthToken": "",
        "userDetails": {}
    }
    $.ajax({
        type: "POST",
        url: `${base_url}/tokens`,
        data: tokenInitialData,
        success: function (data) {
            sessionStorage.setItem('token_id', data._id);
            getToken();
        }
    });
}

/* get access token */
function getToken() {
    $.ajax({
        type: "POST",
        url: `${base_url}/auth/local`,
        data: { identifier: "public", password: "tech@123" },

        success: function (data) {
            let userDetails = JSON.stringify({ name: data.user.name, id: data.user._id });
            sessionStorage.setItem('_usr', JSON.stringify({ name: data.user.name, id: data.user._id }))
            sessionStorage.setItem('_tkn', data.jwt);
            saveTokenToDb(data.jwt, userDetails);
            // getColleges(data.jwt)
            // venue(data.jwt)
        },
        error: function (err) { }
    })
}

function saveTokenToDb(tokenReceived, userDetails) {

    let tkn = sessionStorage.getItem('_tkn') // might use this
    let tokenId = sessionStorage.getItem('token_id')
    let inputData = {
        "AuthToken": tokenReceived,
        "userDetails": userDetails
    }

    $.ajax({
        type: "PUT",
        url: `${base_url}/tokens/${tokenId}`,
        data: inputData,
        headers: {
            Authorization: `Bearer ${tkn}`
        },
        success: function (data) {
            // console.log(data);
            getOtpRewrittenFunction();

        }
    });

}

function parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};


// get otp - api
function getOtp(number) {

    getDetailsIfExists(function (user) {
        // afterOtpVerified(); // uncomment to skip otp part & comment all below ajax
        let email = user == undefined ? undefined : user.participants.email
        let tkn = sessionStorage.getItem('_tkn')
        // debugger

        $.ajax({
            type: "POST",
            url: `${base_url}/participantsOtp`,
            data: {
                number,
                email
            },
            headers: {
                Authorization: `Bearer ${tkn}`
            },
            "async": true,
            "crossDomain": true,

            success: function (data) {

                afterOptSent()
            },
            error: function (err) {
                console.log(err)
            }
        });
    })
}


function setuserDetails(val) {

    currentUserForHT = JSON.parse(sessionStorage.getItem("currentUserForHT"))
    try {
        if (val == 'afterPhotoUpload') {
            $('#user_name').html(``)
        } else {
            $('#user_name').html(`Hi ${titleCase(getFirstName(currentUserForHT.name))}, Please <span style="font-weight:600">upload your photo</span> & <span  style="font-weight:600">choose a time slot</span>.`)
        }

        $('#user_name').show()
    } catch (error) {

    }
}

// get transaction info for user - api
// #transNoIssue
function getDetailsIfExists(callback) {
    let tkn = sessionStorage.getItem("_tkn")
    $.ajax({
        type: "POST",
        url: `${base_url}/getTransactionFromMobile`,
        data: { "mobile": $('#phoneNumber').val() },
        headers: {
            Authorization: `Bearer ${tkn}`
        },

        success: function (data) {
            // console.log(data.participants)
            // time_slots
            sessionStorage.setItem("currentUserForHT", JSON.stringify(data.participants));
            $('#fullName').val(data.participants.name);

            getVenueNames(data.participants.examvenue, data.participants.exam)
            callback(data)
        },
        error: function (err) {
            console.log(err)
            // callback(undefined)
            alert(`User with ${$('#phoneNumber').val()} phone number is not found.`)
        }
    });
}

function getVenueNames(examvenue_id, exam_id) {

    let tkn = sessionStorage.getItem("_tkn")
    let venueMsg;
    $.ajax({
        type: "GET",
        url: `${base_url}/examvenues/${examvenue_id}`,
        headers: {
            Authorization: `Bearer ${tkn}`
        },
        success: function (data) {
            let examvenue = (data.name).split("-")[0];
            let examDate = (data.name).split("-")[1];
            let completeVenue = examDate + " - " + examvenue;
            getTimeSlots(exam_id, completeVenue)
        }
    });

}

// $('#downloadHallTicket').click(function () {
//     console.log("Clicked")
//     window.open("https://shaping3d.sgp1.digitaloceanspaces.com/upskill/5cfe27a278c97840bfa35081.pdf", "_blank");
// })

function getTimeSlots(exam_id, examvenue) {
    let tkn = sessionStorage.getItem("_tkn")
    let examsVenueSlotObj = {}
    let examsThresholdObj = {}
    let venueMsg;

    $.ajax({
        type: "GET",
        url: `${base_url}/exams/${exam_id}`,
        headers: {
            Authorization: `Bearer ${tkn}`
        },
        success: function (data) {

            examsVenueSlotObj = {
                "slot1": data.time_slot_1,
                "slot2": data.time_slot_2,
                "venue": examvenue,
                "exams_id": data._id,
                "threshold1": data.Threshold_slot_1,
                "threshold2": data.Threshold_slot_2
            }
            examsThresholdObj = {
                "exams_id": data._id,
                "Threshold_slot_1": data.Threshold_slot_1,
                "Threshold_slot_2": data.Threshold_slot_2
            }
            sessionStorage.setItem("threshold_values", JSON.stringify(examsThresholdObj));
            sessionStorage.setItem("time_dynamic_slots", JSON.stringify(examsVenueSlotObj));

            if (data.time_slot_2 == "") {
                $('.examTwoSlotSelectOption').hide()
                $('.examOnlyOneSlot').show()
                $('#1st_slot_1slotExam').html(data.time_slot_1);
                $('#1st_slot_1slotExam').addClass('time_slots_active');
                venueMsg = `We only have 1 time slot for <b>${examvenue}</b> Venue`
                $('.showExamVenue').html(venueMsg)
            } else {
                if (data.Threshold_slot_1 > 0 || data.Threshold_slot_2 > 0) {
                    if (data.Threshold_slot_1 <= 0) {
                        $('#1st_slot_2slotExam').addClass('inactive_time_slot');
                        $('#2nd_slot_2slotExam').removeClass('inactive_time_slot');
                        $('#1st_slot_2slotExam').html(data.time_slot_1 + fullTimeSlotHtml);
                        $('#2nd_slot_2slotExam').html(data.time_slot_2);
                    }
                    if (data.Threshold_slot_2 <= 0) {
                        $('#2nd_slot_2slotExam').addClass('inactive_time_slot');
                        $('#1st_slot_2slotExam').removeClass('inactive_time_slot');
                        $('#1st_slot_2slotExam').html(data.time_slot_1);
                        $('#2nd_slot_2slotExam').html(data.time_slot_2 + fullTimeSlotHtml);
                    }
                    if (data.Threshold_slot_1 > 0 && data.Threshold_slot_2 > 0) {
                        $('#1st_slot_2slotExam').html(data.time_slot_1);
                        $('#2nd_slot_2slotExam').html(data.time_slot_2);
                        $('#1st_slot_2slotExam').removeClass('inactive_time_slot');
                        $('#2nd_slot_2slotExam').removeClass('inactive_time_slot');
                    }
                }
                else {
                    $('#1st_slot_2slotExam').removeClass('time_slots_active');
                    $('#2nd_slot_2slotExam').removeClass('time_slots_active');

                    $('#1st_slot_2slotExam').addClass('inactive_time_slot');
                    $('#2nd_slot_2slotExam').addClass('inactive_time_slot');

                    $('#1st_slot_2slotExam').html(data.time_slot_1 + fullTimeSlotHtml);
                    $('#2nd_slot_2slotExam').html(data.time_slot_2 + fullTimeSlotHtml);
                }
                $('.examOnlyOneSlot').hide()
                $('.examTwoSlotSelectOption').show()


                let prevVenueText = $('.showExamVenue').text();
                venueMsg = prevVenueText + ` for <b>${examvenue}</b> Venue`;
                $('.showExamVenue').html(venueMsg);
            }


        }
    })
}



// verify otp - api
function verifyOtp(number, otp) {
    let tkn = sessionStorage.getItem('_tkn')
    let url = `${base_url}/participantsVerifyOtp`
    $.ajax({
        type: "POST",
        url: url,
        async: true,
        crossDomain: true,
        data: { number, otp },
        headers: {
            Authorization: `Bearer ${tkn}`
        },

        success: function (data) {

            currentUserForHT = JSON.parse(sessionStorage.getItem("currentUserForHT"))
            if (data.type != 'error') {
                if (currentUserForHT.pic == '') {
                    afterOtpVerified()
                } else {
                    afterPhotoUpload()
                }

            }
            else {
                $("#otp").next().fadeIn()
            }
        },
        error: function (err) {
            console.log(err)
        }
    });
}


// resendOtp on call - api
function resendOtpCall(number) {
    let tkn = sessionStorage.getItem('_tkn')
    $.ajax({
        type: "POST",
        url: `${base_url}/participantsGetCall`,
        async: true,
        crossDomain: true,
        data: { number },
        headers: {
            Authorization: `Bearer ${tkn}`
        },
        success: function (data) { },
        error: function (err) { }
    });
}


// compress and upload an image
function uploadImage(file) {
    let tkn = sessionStorage.getItem('_tkn')
    var formData = new FormData();

    var width = 350;
    var height = 385;
    var fileName = file.name;

    var reader = new FileReader();
    var file1;
    reader.readAsDataURL(file);

    reader.onload = function (event) {

        var img = new Image();
        img.src = event.target.result;
        img.onload = function () {

            var elem = document.createElement('canvas');
            elem.width = width;
            elem.height = height;
            var ctx = elem.getContext('2d');

            ctx.drawImage(img, 0, 0, width, height);
            ctx.canvas.toBlob(function (blob) {

                file1 = new File([blob], fileName, {
                    type: 'image/jpeg',
                    lastModified: Date.now()
                });

                formData.append('files', file1)

                $.ajax({
                    type: "POST",
                    processData: false,
                    contentType: false,
                    url: `${base_url}/upload`,
                    data: formData,
                    headers: {
                        Authorization: `Bearer ${tkn}`
                    },
                    success: function (data) {
                        var usr = JSON.parse(sessionStorage.getItem("currentUserForHT"))
                        usr['pic'] = data[0].url
                        usr['time_slots'] = $('.time_slots_active').text()
                        usr['name'] = $('#fullName').val()

                        updateParticipant(usr)
                    },
                    error: function (err) {
                        console.log(err)
                    }
                });

            }, 'image/jpeg', 1);

        },
            reader.onerror = error => console.log(error);
    };
}

function updateSlotDisplayOnClick(timeSlots) {
    let thresholdObj = JSON.parse(sessionStorage.getItem("threshold_values"));
    if (thresholdObj.Threshold_slot_1 > 0 || thresholdObj.Threshold_slot_2 > 0) {
        if (thresholdObj.Threshold_slot_1 <= 0) {
            $('#1st_slot_2slotExam').removeClass('time_slots_active');
            $('#1st_slot_2slotExam').addClass('inactive_time_slot');
            $('#2nd_slot_2slotExam').removeClass('inactive_time_slot');
            $('#1st_slot_2slotExam').html(timeSlots.slot1 + fullTimeSlotHtml);
            $('#2nd_slot_2slotExam').html(timeSlots.slot2);

        }
        if (thresholdObj.Threshold_slot_2 <= 0) {
            $('#2nd_slot_2slotExam').removeClass('time_slots_active');
            $('#2nd_slot_2slotExam').addClass('inactive_time_slot');
            $('#1st_slot_2slotExam').removeClass('inactive_time_slot');
            $('#1st_slot_2slotExam').html(timeSlots.slot1);
            $('#2nd_slot_2slotExam').html(timeSlots.slot2 + fullTimeSlotHtml);

        }
        if (thresholdObj.Threshold_slot_1 > 0 && thresholdObj.Threshold_slot_2 > 0) {
            $('#1st_slot_2slotExam').html(timeSlots.slot1);
            $('#2nd_slot_2slotExam').html(timeSlots.slot2);
            $('#1st_slot_2slotExam').removeClass('inactive_time_slot');
            $('#2nd_slot_2slotExam').removeClass('inactive_time_slot');
            // uploadImage(file)
        }
    }
    else {
        $('#1st_slot_2slotExam').removeClass('time_slots_active');
        $('#2nd_slot_2slotExam').removeClass('time_slots_active');

        $('#1st_slot_2slotExam').addClass('inactive_time_slot');
        $('#2nd_slot_2slotExam').addClass('inactive_time_slot');

        $('#1st_slot_2slotExam').html(timeSlots.slot1 + fullTimeSlotHtml);
        $('#2nd_slot_2slotExam').html(timeSlots.slot2 + fullTimeSlotHtml);
        sendMailOnBothFullSlots();
    }

}

function getCurrentThreshold(commandStr) {
    // exams_id
    let timeSlots = JSON.parse(sessionStorage.getItem("time_dynamic_slots"));
    let current_exam_id = timeSlots.exams_id
    let examsThresholdObj = {};
    let tkn = sessionStorage.getItem('_tkn')
    $.ajax({
        type: "GET",
        url: `${base_url}/exams/${current_exam_id}`,
        headers: {
            Authorization: `Bearer ${tkn}`
        },
        success: function (data) {

            examsThresholdObj = {
                "exams_id": data._id,
                "Threshold_slot_1": data.Threshold_slot_1,
                "Threshold_slot_2": data.Threshold_slot_2
            }
            sessionStorage.setItem("threshold_values", JSON.stringify(examsThresholdObj));
            if (commandStr == "Submit" && data.time_slot_2 != "") {
                checkThresholdSubmit(examsThresholdObj, timeSlots)
            }
            if (commandStr == "Submit" && data.time_slot_2 == "") { // 1 slot exam case
                uploadImage(file)
            }
            if (commandStr == "Click") {
                updateSlotDisplayOnClick(timeSlots);
            }
        }
    })


}

function checkThresholdSubmit(thresholdObj, timeSlots) {
    let currentlySelectedTimeSlot = $('.time_slots_active').data('id').substring(0, 1);
    if (thresholdObj.Threshold_slot_1 > 0 || thresholdObj.Threshold_slot_2 > 0) {

        if (currentlySelectedTimeSlot == 1) {
            if (thresholdObj.Threshold_slot_1 <= 0) {
                alert('Slot 1 just got full !');
                $('#1st_slot_2slotExam').removeClass('time_slots_active');
                $('#1st_slot_2slotExam').addClass('inactive_time_slot');
                $('#2nd_slot_2slotExam').removeClass('inactive_time_slot');
                $('#1st_slot_2slotExam').html(timeSlots.slot1 + fullTimeSlotHtml);
                $('#2nd_slot_2slotExam').html(timeSlots.slot2);
            }
            else {
                updateThresholdCount()
                uploadImage(file)
            }
        }
        if (currentlySelectedTimeSlot == 2) {
            if (thresholdObj.Threshold_slot_2 <= 0) {
                alert('Slot 2 just got full !');
                $('#2nd_slot_2slotExam').removeClass('time_slots_active');
                $('#2nd_slot_2slotExam').addClass('inactive_time_slot');
                $('#1st_slot_2slotExam').removeClass('inactive_time_slot');
                $('#1st_slot_2slotExam').html(timeSlots.slot1);
                $('#2nd_slot_2slotExam').html(timeSlots.slot2 + fullTimeSlotHtml);
            }
            else {
                updateThresholdCount()
                uploadImage(file)
            }
        }

    }
    else {

        $('#1st_slot_2slotExam').removeClass('time_slots_active');
        $('#2nd_slot_2slotExam').removeClass('time_slots_active');

        $('#1st_slot_2slotExam').addClass('inactive_time_slot');
        $('#2nd_slot_2slotExam').addClass('inactive_time_slot');

        $('#1st_slot_2slotExam').html(timeSlots.slot1 + fullTimeSlotHtml);
        $('#2nd_slot_2slotExam').html(timeSlots.slot2 + fullTimeSlotHtml);
        sendMailOnBothFullSlots();

    }

}

function sendMailOnBothFullSlots() {
    let tkn = sessionStorage.getItem('_tkn')
    let timeSlots = JSON.parse(sessionStorage.getItem("time_dynamic_slots"));
    let mailMessage = generateFullSlotMsg(timeSlots)
    let sub = `Both Full slots hall ticket | ${timeSlots.venue}`
    $.ajax({
        type: "POST",
        url: `${base_url}/email`,
        data: {
            to: "neel@techzillaindia.com",
            from: "upskill@techzillaindia.com",
            subject: sub,
            html: mailMessage
        },
        headers: {
            Authorization: `Bearer ${tkn}`
        },
        success: function (data) {
            alert('We have recieved the issue of both time slots full, we will fix it soon. Please try again after some time.')
        },
        error: function (err) {

        }
    });
}


function updateThresholdCount() {
    let currentlySelectedTimeSlot = $('.time_slots_active').data('id').substring(0, 1);
    let thresholdObj = JSON.parse(sessionStorage.getItem("threshold_values"));
    let thresholdValue;
    let tkn = sessionStorage.getItem('_tkn')
    // console.log(currentlySelectedTimeSlot);
    let selectedThresholdSlot;
    if (currentlySelectedTimeSlot == 1) {
        selectedThresholdSlot = "Threshold_slot_1";
        thresholdValue = thresholdObj.Threshold_slot_1;
    }
    else {
        selectedThresholdSlot = "Threshold_slot_2";
        thresholdValue = thresholdObj.Threshold_slot_2;
    }
    thresholdValue = thresholdValue - 1

    let thresholdData = {
        [selectedThresholdSlot]: thresholdValue
    }

    $.ajax({
        type: "PUT",
        url: `${base_url}/exams/${thresholdObj.exams_id}`,
        data: thresholdData,
        headers: {
            Authorization: `Bearer ${tkn}`
        },
        success: function (data) {
            // console.log(data);
        }
    })

}


// update participant with uploaded picture and time_slot
function updateParticipant(usr) {
    let tkn = sessionStorage.getItem('_tkn')
    // debugger
    delete usr.transaction;
    $.ajax({
        type: "PUT",
        url: `${base_url}/participants/${usr._id}`,
        data: usr,
        headers: {
            Authorization: `Bearer ${tkn}`
        },
        success: function (data) {
            sessionStorage.setItem("currentUserForHT", JSON.stringify(data))
            // console.log(data)
            afterPhotoUpload()

        },
        error: function (err) {
            alert('Something went wrong, Please try again later.')
        }
    })
}


// generate hall ticket pdf in backend
// #transNoIssue 

function generateHallTicket() {
    let tkn = sessionStorage.getItem('_tkn')
    currentUserForHT = JSON.parse(sessionStorage.getItem("currentUserForHT"))
    $.ajax({
        type: "POST",
        url: `${base_url}/getHallTicket`,
        data: { mobile: currentUserForHT.mobile },
        headers: {
            Authorization: `Bearer ${tkn}`
        },
        success: function (data) {
            if (data.error == false) {
                currentUserForHT = JSON.parse(sessionStorage.getItem("currentUserForHT"))
                $('#hiddenField').val(currentUserForHT._id)
                setTimeout(function () {
                    $('#user_name').html(`Your Admit Card is ready.`)
                    $('#mailedYou').html(`Please check your email ${currentUserForHT.email} for <b>important instructions & Admit card</b>.`)
                    $('#waitId').hide()
                    $('#download_pdf').attr('href', `https://shaping3d.sgp1.digitaloceanspaces.com/upskill/${data.filename}`)
                    $('#download_pdf').fadeIn(500)
                }, 1750)
            }
        },
        error: function (err) { }
    })
}



function titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
}

function getFirstName(str) {

    var name = str.split(" ");
    return name[0]
}


$(document).on('click', '.inactive_time_slot', function () {
    alert("This time slot is full !")
    // console.log(this)
    let currentId = $(this).data('id').substring(0, 1)
    // console.log(currentId);
    $(this).removeClass('time_slots_active')
    // if (currentId == 1) {
    //     $('#2nd_slot_2slotExam').addClass('time_slots_active');
    // }
    // else if (currentId == 2) {

    //     $('#1st_slot_2slotExam').addClass('time_slots_active');
    // }
})


// email template 

function generateFullSlotMsg(timeSlots) {
    return `<!DOCTYPE html>
                            <html lang="en">
    
                            <head>
                                <meta charset="UTF-8">
                                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                <title></title>
                            </head>
    
                            <body>
                                <p>Please Increase the threshold of time slots ${timeSlots.venue} venue from Upskill Admin panel</p>
                                
                            </body>
    
                            </html>`
}









// $('.inactive_time_slot').click(function () {

// })

// $(document).ready(function () {

// })

// function getCount() {
//     let tkn = sessionStorage.getItem('_tkn')
//     $.ajax({
//         type: "GET",
//         url: `${base_url}/getTimeSlotCount`,

//         headers: {
//             Authorization: `Bearer ${tkn}`
//         },
//         success: function (data) {
//             // console.log('data', data)
//             if (data['1st_available'] == data['1st']) {
//                 var first = $('#1st_slot')
//                 first.unbind("click")
//                 first.css('cursor', 'default')
//                 first.closest('.wpb_column').append(`<p>This time slot is full</p>`)
//             }
//             if (data['2nd_available'] == data['2nd']) {
//                 var second = $('#2nd_slot')
//                 second.unbind("click")
//                 second.css('cursor', 'default')
//                 second.closest('.wpb_column').append(`<p>This time slot is full</p>`)

//             }

//         },
//         error: function (err) { }
//     })
// }